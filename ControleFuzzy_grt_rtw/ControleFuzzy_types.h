/*
 * ControleFuzzy_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControleFuzzy".
 *
 * Model version              : 2.9
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Thu Oct  7 00:59:58 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ControleFuzzy_types_h_
#define RTW_HEADER_ControleFuzzy_types_h_
#include "rtwtypes.h"

/* Model Code Variants */
#ifndef DEFINED_TYPEDEF_FOR_struct_N4VyYUXPEmFZMBAFMyypUB_
#define DEFINED_TYPEDEF_FOR_struct_N4VyYUXPEmFZMBAFMyypUB_

typedef struct {
  uint8_T SimulinkDiagnostic;
  uint8_T Model[13];
  uint8_T Block[25];
  uint8_T OutOfRangeInputValue;
  uint8_T NoRuleFired;
  uint8_T EmptyOutputFuzzySet;
} struct_N4VyYUXPEmFZMBAFMyypUB;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_nDiNttezQ8pHMZv76leKsH_
#define DEFINED_TYPEDEF_FOR_struct_nDiNttezQ8pHMZv76leKsH_

typedef struct {
  uint8_T type[6];
  int32_T origTypeLength;
  real_T params[4];
  int32_T origParamLength;
} struct_nDiNttezQ8pHMZv76leKsH;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_Kpae7lrLZEHvT9g4PA3tsE_
#define DEFINED_TYPEDEF_FOR_struct_Kpae7lrLZEHvT9g4PA3tsE_

typedef struct {
  struct_nDiNttezQ8pHMZv76leKsH mf[4];
  int32_T origNumMF;
} struct_Kpae7lrLZEHvT9g4PA3tsE;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_LfLTyMaJ3OCgIW59m4OfO_
#define DEFINED_TYPEDEF_FOR_struct_LfLTyMaJ3OCgIW59m4OfO_

typedef struct {
  uint8_T type[7];
  int32_T origTypeLength;
  real_T params[4];
  int32_T origParamLength;
} struct_LfLTyMaJ3OCgIW59m4OfO;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_sLqy4a4hG5DcKWfE6mP5ZB_
#define DEFINED_TYPEDEF_FOR_struct_sLqy4a4hG5DcKWfE6mP5ZB_

typedef struct {
  struct_LfLTyMaJ3OCgIW59m4OfO mf[4];
  int32_T origNumMF;
} struct_sLqy4a4hG5DcKWfE6mP5ZB;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_oO1ZivHtuNyFPsCiTEfw0B_
#define DEFINED_TYPEDEF_FOR_struct_oO1ZivHtuNyFPsCiTEfw0B_

typedef struct {
  uint8_T type[7];
  uint8_T andMethod[3];
  uint8_T orMethod[3];
  uint8_T defuzzMethod[8];
  uint8_T impMethod[3];
  uint8_T aggMethod[3];
  real_T inputRange[8];
  real_T outputRange[2];
  struct_Kpae7lrLZEHvT9g4PA3tsE inputMF[4];
  struct_sLqy4a4hG5DcKWfE6mP5ZB outputMF;
  real_T antecedent[44];
  real_T consequent[11];
  real_T connection[11];
  real_T weight[11];
  int32_T numSamples;
  int32_T numInputs;
  int32_T numOutputs;
  int32_T numRules;
  int32_T numInputMFs[4];
  int32_T numCumInputMFs[4];
  int32_T numOutputMFs;
  int32_T numCumOutputMFs;
  real_T outputSamplePoints[101];
  int32_T orrSize[2];
  int32_T aggSize[2];
  int32_T irrSize[2];
  int32_T rfsSize[2];
  int32_T sumSize[2];
  int32_T inputFuzzySetType;
} struct_oO1ZivHtuNyFPsCiTEfw0B;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_ControleFuzzy_T RT_MODEL_ControleFuzzy_T;

#endif                                 /* RTW_HEADER_ControleFuzzy_types_h_ */
