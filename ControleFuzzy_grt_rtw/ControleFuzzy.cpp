/*
 * ControleFuzzy.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControleFuzzy".
 *
 * Model version              : 2.9
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Thu Oct  7 00:59:58 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "ControleFuzzy.h"
#include "ControleFuzzy_private.h"

/* Function for MATLAB Function: '<S1>/Evaluate Rule Antecedents' */
real_T ControleFuzzyModelClass::ControleFuzzy_trimf(real_T x, const real_T
  params[3])
{
  real_T y;
  y = 0.0;
  if ((params[0] != params[1]) && (params[0] < x) && (x < params[1])) {
    y = 1.0 / (params[1] - params[0]) * (x - params[0]);
  }

  if ((params[1] != params[2]) && (params[1] < x) && (x < params[2])) {
    y = 1.0 / (params[2] - params[1]) * (params[2] - x);
  }

  if (x == params[1]) {
    y = 1.0;
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Consequents' */
void ControleFuzzyModelClass::ControleFuzzy_trimf_o(const real_T x[101], const
  real_T params[3], real_T y[101])
{
  real_T a;
  real_T b;
  real_T c;
  real_T x_0;
  int32_T i;
  a = params[0];
  b = params[1];
  c = params[2];
  for (i = 0; i < 101; i++) {
    x_0 = x[i];
    y[i] = 0.0;
    if ((a < x_0) && (x_0 < b)) {
      y[i] = (x_0 - a) * 2.0;
    }

    if ((b < x_0) && (x_0 < c)) {
      y[i] = (c - x_0) * 2.0;
    }

    if (x_0 == b) {
      y[i] = 1.0;
    }
  }
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T tmp;
  real_T tmp_0;
  real_T y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = std::abs(u0);
    tmp_0 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = 1.0;
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = (rtNaN);
    } else {
      y = std::pow(u0, u1);
    }
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Consequents' */
void ControleFuzzyModelClass::Cont_createMamdaniOutputMFCache(const real_T
  outputSamplePoints[101], real_T outputMFCache[404])
{
  real_T tmp_1[101];
  real_T tmp_0[3];
  real_T outputSamplePoints_0;
  real_T tmp;
  int32_T i;
  int32_T outputMFCache_tmp;
  int8_T b_y1;
  int8_T y2;
  tmp_0[0] = 0.0;
  tmp_0[1] = 0.5;
  tmp_0[2] = 1.0;
  ControleFuzzy_trimf_o(outputSamplePoints, tmp_0, tmp_1);
  for (i = 0; i < 101; i++) {
    outputMFCache[i << 2] = tmp_1[i];
  }

  tmp_0[0] = 1.0;
  tmp_0[1] = 1.5;
  tmp_0[2] = 2.0;
  ControleFuzzy_trimf_o(outputSamplePoints, tmp_0, tmp_1);
  for (i = 0; i < 101; i++) {
    outputMFCache_tmp = i << 2;
    outputMFCache[outputMFCache_tmp + 1] = tmp_1[i];
    outputSamplePoints_0 = outputSamplePoints[i];
    tmp = (outputSamplePoints_0 - 2.37) * 2.05761316872428;
    tmp *= tmp;
    tmp = rt_powd_snf(tmp, 14.296118611365527);
    outputMFCache[outputMFCache_tmp + 2] = 1.0 / (tmp + 1.0);
    b_y1 = 0;
    y2 = 0;
    if (outputSamplePoints_0 >= 0.0) {
      b_y1 = 1;
    }

    if (outputSamplePoints_0 < 0.0) {
      b_y1 = 0;
    }

    if (outputSamplePoints_0 <= 0.05) {
      y2 = 1;
    }

    if (outputSamplePoints_0 > 0.05) {
      y2 = 0;
    }

    if (static_cast<real_T>(b_y1) < y2) {
      outputMFCache[(i << 2) + 3] = b_y1;
    } else {
      outputMFCache[outputMFCache_tmp + 3] = y2;
    }
  }
}

/* Model step function */
void ControleFuzzyModelClass::step()
{
  static const real_T b_0[3] = { -37.5, 0.0, 20.0 };

  static const real_T c[3] = { 45.0, 90.0, 128.0 };

  static const real_T d[3] = { -166.7, 0.0, 180.0 };

  static const real_T e[3] = { 220.0, 400.0, 566.7 };

  static const real_T f[3] = { 50.0, 120.0, 300.0 };

  static const real_T g[3] = { 46.67, 180.0, 313.3 };

  static const real_T h[3] = { 366.7, 500.0, 633.3 };

  static const int8_T i_0[44] = { 0, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1, 2, 3, 3, 3, 3, 2, 2, 3, 3, 4, 0, 0, 0, 0, 0, 0, 1, 2, 3,
    1, 0 };

  static const int8_T b[11] = { 1, 2, 3, 3, 2, 1, 2, 2, 3, 1, 4 };

  real_T outputMFCache[404];
  real_T rtb_aggregatedOutputs[101];
  real_T inputMFCache[13];
  real_T rtb_antecedentOutputs[11];
  real_T tmp[3];
  real_T b_y1;
  real_T rtb_aggregatedOutputs_e;
  real_T sumAntecedentOutputs;
  real_T y2;
  int32_T inputID;
  int32_T ruleID;
  int8_T i;

  /* Outputs for Atomic SubSystem: '<Root>/Controlador' */
  /* MATLAB Function: '<S1>/Evaluate Rule Antecedents' incorporates:
   *  Inport: '<Root>/Input'
   *  Inport: '<Root>/Input1'
   *  Inport: '<Root>/Input2'
   *  Inport: '<Root>/Input3'
   */
  sumAntecedentOutputs = 0.0;
  inputMFCache[0] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, b_0);
  tmp[0] = 15.0;
  tmp[1] = 37.5;
  tmp[2] = 60.0;
  inputMFCache[1] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, tmp);
  inputMFCache[2] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, c);
  inputMFCache[3] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_next, d);
  tmp[0] = 100.0;
  tmp[1] = 200.0;
  tmp[2] = 300.0;
  inputMFCache[4] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_next, tmp);
  inputMFCache[5] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_next, e);
  inputMFCache[6] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, f);
  tmp[0] = 180.0;
  tmp[1] = 340.0;
  tmp[2] = 500.0;
  inputMFCache[7] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, tmp);
  b_y1 = 0.0;
  y2 = 0.0;
  if (ControleFuzzy_U.distancia_tot >= 600.0) {
    b_y1 = 1.0;
  }

  if (ControleFuzzy_U.distancia_tot < 450.0) {
    b_y1 = 0.0;
  }

  if ((450.0 <= ControleFuzzy_U.distancia_tot) && (ControleFuzzy_U.distancia_tot
       < 600.0)) {
    b_y1 = (ControleFuzzy_U.distancia_tot - 450.0) * 0.0066666666666666671;
  }

  if (ControleFuzzy_U.distancia_tot <= 2000.0) {
    y2 = 1.0;
  }

  if (ControleFuzzy_U.distancia_tot > 2750.0) {
    y2 = 0.0;
  }

  if ((2000.0 < ControleFuzzy_U.distancia_tot) && (ControleFuzzy_U.distancia_tot
       <= 2750.0)) {
    y2 = (2750.0 - ControleFuzzy_U.distancia_tot) * 0.0013333333333333333;
  }

  if (b_y1 < y2) {
    inputMFCache[8] = b_y1;
  } else {
    inputMFCache[8] = y2;
  }

  tmp[0] = -100.0;
  tmp[1] = 0.0;
  tmp[2] = 100.0;
  inputMFCache[9] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, tmp);
  inputMFCache[10] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_opo, g);
  tmp[0] = 206.7;
  tmp[1] = 340.0;
  tmp[2] = 473.3;
  inputMFCache[11] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_opo, tmp);
  inputMFCache[12] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_opo, h);
  for (ruleID = 0; ruleID < 11; ruleID++) {
    b_y1 = 1.0;
    i = i_0[ruleID];
    if (i != 0) {
      y2 = inputMFCache[i - 1];
      if (1.0 > y2) {
        b_y1 = y2;
      }
    }

    i = i_0[ruleID + 11];
    if (i != 0) {
      y2 = inputMFCache[i + 2];
      if ((b_y1 > y2) || (rtIsNaN(b_y1) && (!rtIsNaN(y2)))) {
        b_y1 = y2;
      }
    }

    i = i_0[ruleID + 22];
    if (i != 0) {
      y2 = inputMFCache[i + 5];
      if ((b_y1 > y2) || (rtIsNaN(b_y1) && (!rtIsNaN(y2)))) {
        b_y1 = y2;
      }
    }

    i = i_0[ruleID + 33];
    if (i != 0) {
      y2 = inputMFCache[i + 9];
      if ((b_y1 > y2) || (rtIsNaN(b_y1) && (!rtIsNaN(y2)))) {
        b_y1 = y2;
      }
    }

    sumAntecedentOutputs += b_y1;
    rtb_antecedentOutputs[ruleID] = b_y1;
  }

  /* MATLAB Function: '<S1>/Evaluate Rule Consequents' incorporates:
   *  Constant: '<S1>/Output Sample Points'
   */
  std::memset(&rtb_aggregatedOutputs[0], 0, 101U * sizeof(real_T));
  Cont_createMamdaniOutputMFCache(ControleFuzzy_ConstP.OutputSamplePoints_Value,
    outputMFCache);
  for (ruleID = 0; ruleID < 11; ruleID++) {
    y2 = rtb_antecedentOutputs[ruleID];
    for (inputID = 0; inputID < 101; inputID++) {
      rtb_aggregatedOutputs_e = rtb_aggregatedOutputs[inputID];
      b_y1 = outputMFCache[((inputID << 2) + b[ruleID]) - 1];
      if ((b_y1 > y2) || (rtIsNaN(b_y1) && (!rtIsNaN(y2)))) {
        b_y1 = y2;
      }

      if ((rtb_aggregatedOutputs_e < b_y1) || (rtIsNaN(rtb_aggregatedOutputs_e) &&
           (!rtIsNaN(b_y1)))) {
        rtb_aggregatedOutputs_e = b_y1;
      }

      rtb_aggregatedOutputs[inputID] = rtb_aggregatedOutputs_e;
    }
  }

  /* End of MATLAB Function: '<S1>/Evaluate Rule Consequents' */

  /* MATLAB Function: '<S1>/Defuzzify Outputs' incorporates:
   *  Constant: '<S1>/Output Sample Points'
   *  MATLAB Function: '<S1>/Evaluate Rule Antecedents'
   */
  if (sumAntecedentOutputs == 0.0) {
    /* Outport: '<Root>/Output' */
    ControleFuzzy_Y.velocidade = 1.25;
  } else {
    sumAntecedentOutputs = 0.0;
    b_y1 = 0.0;
    for (ruleID = 0; ruleID < 101; ruleID++) {
      b_y1 += rtb_aggregatedOutputs[ruleID];
    }

    if (b_y1 == 0.0) {
      /* Outport: '<Root>/Output' */
      ControleFuzzy_Y.velocidade = 1.25;
    } else {
      for (ruleID = 0; ruleID < 101; ruleID++) {
        sumAntecedentOutputs +=
          ControleFuzzy_ConstP.OutputSamplePoints_Value[ruleID] *
          rtb_aggregatedOutputs[ruleID];
      }

      /* Outport: '<Root>/Output' incorporates:
       *  Constant: '<S1>/Output Sample Points'
       */
      ControleFuzzy_Y.velocidade = 1.0 / b_y1 * sumAntecedentOutputs;
    }
  }

  /* End of MATLAB Function: '<S1>/Defuzzify Outputs' */
  /* End of Outputs for SubSystem: '<Root>/Controlador' */
}

/* Model initialize function */
void ControleFuzzyModelClass::initialize()
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));
}

/* Model terminate function */
void ControleFuzzyModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControleFuzzyModelClass::ControleFuzzyModelClass() :
  ControleFuzzy_U(),
  ControleFuzzy_Y(),
  ControleFuzzy_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControleFuzzyModelClass::~ControleFuzzyModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControleFuzzy_T * ControleFuzzyModelClass::getRTM()
{
  return (&ControleFuzzy_M);
}
