/*
 * ControlePosicao.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.94
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Thu Dec 12 09:55:07 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#include "ControlePosicao.h"
#include "ControlePosicao_private.h"

/*
 * Output and update for action system:
 *    '<S3>/If Action Subsystem'
 *    '<S3>/If Action Subsystem1'
 *    '<S4>/If Action Subsystem'
 *    '<S4>/If Action Subsystem1'
 */
void ControlePosic_IfActionSubsystem(real_T rtu_In1, real_T *rty_Out1)
{
  /* Inport: '<S93>/In1' */
  *rty_Out1 = rtu_In1;
}

/* Model step function */
void ControlePosicaoModelClass::step()
{
  real_T rtb_Sum;
  real_T rtb_Merge;
  real_T rtb_FilterCoefficient;
  real_T rtb_Filter_e;
  real_T rtb_Integrator_p;
  boolean_T rtb_NotEqual;
  int8_T rtb_DataTypeConv2;
  real_T rtb_ErroY;
  int8_T rtb_DataTypeConv1;
  real_T rtb_PosRoboX;
  real_T rtb_IntegralGain;
  boolean_T rtb_NotEqual_o;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Inport: '<Root>/PosX'
   *  Inport: '<Root>/ReAlimX'
   */
  rtb_Sum = ControlePosicao_U.PosX - ControlePosicao_U.ReAlimX;

  /* If: '<S3>/If' incorporates:
   *  Constant: '<S3>/Constant'
   *  Constant: '<S3>/Constant1'
   */
  if (rtb_Sum < 0.005) {
    /* Outputs for IfAction SubSystem: '<S3>/If Action Subsystem' incorporates:
     *  ActionPort: '<S93>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S3>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S3>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S94>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S3>/If Action Subsystem1' */
  }

  /* End of If: '<S3>/If' */

  /* DiscreteIntegrator: '<S34>/Integrator' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState <= 0))
  {
    ControlePosicao_DW.Integrator_DSTATE = 0.0;
  }

  /* DiscreteIntegrator: '<S29>/Filter' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Filter_PrevResetState <= 0)) {
    ControlePosicao_DW.Filter_DSTATE = 0.0;
  }

  /* Gain: '<S37>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S29>/Filter'
   *  Gain: '<S28>/Derivative Gain'
   *  Sum: '<S29>/SumD'
   */
  rtb_FilterCoefficient = (-0.0145959513325677 * rtb_Sum -
    ControlePosicao_DW.Filter_DSTATE) * 66.1315693226058;

  /* Sum: '<S43>/Sum' incorporates:
   *  DiscreteIntegrator: '<S34>/Integrator'
   *  Gain: '<S39>/Proportional Gain'
   */
  rtb_Filter_e = (1.91527889417129 * rtb_Sum +
                  ControlePosicao_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

  /* DeadZone: '<S27>/DeadZone' */
  if (rtb_Filter_e > 1.75) {
    rtb_Integrator_p = rtb_Filter_e - 1.75;
  } else if (rtb_Filter_e >= -1.75) {
    rtb_Integrator_p = 0.0;
  } else {
    rtb_Integrator_p = rtb_Filter_e - -1.75;
  }

  /* End of DeadZone: '<S27>/DeadZone' */

  /* RelationalOperator: '<S27>/NotEqual' incorporates:
   *  Gain: '<S27>/ZeroGain'
   */
  rtb_NotEqual = (0.0 != rtb_Integrator_p);

  /* Signum: '<S27>/SignPreSat' */
  if (rtb_Integrator_p < 0.0) {
    rtb_Integrator_p = -1.0;
  } else {
    if (rtb_Integrator_p > 0.0) {
      rtb_Integrator_p = 1.0;
    }
  }

  /* End of Signum: '<S27>/SignPreSat' */

  /* DataTypeConversion: '<S27>/DataTypeConv1' */
  rtb_DataTypeConv2 = static_cast<int8_T>(rtb_Integrator_p);

  /* Gain: '<S31>/Integral Gain' */
  rtb_Integrator_p = 0.0192414465266312 * rtb_Sum;

  /* Saturate: '<S41>/Saturation' */
  if (rtb_Filter_e > 1.75) {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = 1.75;
  } else if (rtb_Filter_e < -1.75) {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = -1.75;
  } else {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = rtb_Filter_e;
  }

  /* End of Saturate: '<S41>/Saturation' */

  /* Outport: '<Root>/ErroX' */
  ControlePosicao_Y.ErroX = rtb_Sum;

  /* Outport: '<Root>/PosRoboX' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoX'
   */
  ControlePosicao_Y.PosRoboX = 0.00024266145754114541 *
    ControlePosicao_DW.TFPosicaoX_states[0] + 0.00023847996175603949 *
    ControlePosicao_DW.TFPosicaoX_states[1];

  /* Sum: '<Root>/Sum3' incorporates:
   *  Inport: '<Root>/PosY'
   *  Inport: '<Root>/ReAlimY'
   */
  rtb_ErroY = ControlePosicao_U.PosY - ControlePosicao_U.ReAlimY;

  /* If: '<S4>/If' incorporates:
   *  Constant: '<S4>/Constant'
   *  Constant: '<S4>/Constant1'
   */
  if (rtb_ErroY < 0.005) {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem' incorporates:
     *  ActionPort: '<S95>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Filter_e);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S96>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Filter_e);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem1' */
  }

  /* End of If: '<S4>/If' */

  /* DiscreteIntegrator: '<S78>/Integrator' */
  if ((rtb_Filter_e != 0.0) || (ControlePosicao_DW.Integrator_PrevResetState_p
       != 0)) {
    ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  }

  /* DiscreteIntegrator: '<S73>/Filter' */
  if ((rtb_Filter_e != 0.0) || (ControlePosicao_DW.Filter_PrevResetState_o != 0))
  {
    ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  }

  /* Gain: '<S81>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S73>/Filter'
   *  Gain: '<S72>/Derivative Gain'
   *  Sum: '<S73>/SumD'
   */
  rtb_PosRoboX = (-0.075497602894972 * rtb_ErroY -
                  ControlePosicao_DW.Filter_DSTATE_b) * 1.88433415137563;

  /* Sum: '<S87>/Sum' incorporates:
   *  DiscreteIntegrator: '<S78>/Integrator'
   *  Gain: '<S83>/Proportional Gain'
   */
  rtb_Sum = (2.50195051297098 * rtb_ErroY +
             ControlePosicao_DW.Integrator_DSTATE_i) + rtb_PosRoboX;

  /* DeadZone: '<S71>/DeadZone' */
  if (rtb_Sum > 1.75) {
    rtb_IntegralGain = rtb_Sum - 1.75;
  } else if (rtb_Sum >= -1.75) {
    rtb_IntegralGain = 0.0;
  } else {
    rtb_IntegralGain = rtb_Sum - -1.75;
  }

  /* End of DeadZone: '<S71>/DeadZone' */

  /* RelationalOperator: '<S71>/NotEqual' incorporates:
   *  Gain: '<S71>/ZeroGain'
   */
  rtb_NotEqual_o = (0.0 != rtb_IntegralGain);

  /* Signum: '<S71>/SignPreSat' */
  if (rtb_IntegralGain < 0.0) {
    rtb_IntegralGain = -1.0;
  } else {
    if (rtb_IntegralGain > 0.0) {
      rtb_IntegralGain = 1.0;
    }
  }

  /* End of Signum: '<S71>/SignPreSat' */

  /* DataTypeConversion: '<S71>/DataTypeConv1' */
  rtb_DataTypeConv1 = static_cast<int8_T>(rtb_IntegralGain);

  /* Gain: '<S75>/Integral Gain' */
  rtb_IntegralGain = 0.0351923201517644 * rtb_ErroY;

  /* Saturate: '<S85>/Saturation' */
  if (rtb_Sum > 1.75) {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = 1.75;
  } else if (rtb_Sum < -1.75) {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = -1.75;
  } else {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = rtb_Sum;
  }

  /* End of Saturate: '<S85>/Saturation' */

  /* Outport: '<Root>/ErroY' */
  ControlePosicao_Y.ErroY = rtb_ErroY;

  /* Outport: '<Root>/PosRoboY' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoY'
   */
  ControlePosicao_Y.PosRoboY = 0.00033791425239401582 *
    ControlePosicao_DW.TFPosicaoY_states[0] + 0.00033058944925994858 *
    ControlePosicao_DW.TFPosicaoY_states[1];

  /* Signum: '<S27>/SignPreIntegrator' */
  if (rtb_Integrator_p < 0.0) {
    rtb_Sum = -1.0;
  } else if (rtb_Integrator_p > 0.0) {
    rtb_Sum = 1.0;
  } else {
    rtb_Sum = rtb_Integrator_p;
  }

  /* End of Signum: '<S27>/SignPreIntegrator' */

  /* Switch: '<S27>/Switch' incorporates:
   *  Constant: '<S27>/Constant1'
   *  DataTypeConversion: '<S27>/DataTypeConv2'
   *  Logic: '<S27>/AND3'
   *  RelationalOperator: '<S27>/Equal1'
   */
  if (rtb_NotEqual && (rtb_DataTypeConv2 == static_cast<int8_T>(rtb_Sum))) {
    rtb_Integrator_p = 0.0;
  }

  /* End of Switch: '<S27>/Switch' */

  /* Update for DiscreteIntegrator: '<S34>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE += 0.015 * rtb_Integrator_p;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S34>/Integrator' */

  /* Update for DiscreteIntegrator: '<S29>/Filter' */
  ControlePosicao_DW.Filter_DSTATE += 0.015 * rtb_FilterCoefficient;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S29>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoX' incorporates:
   *  Outport: '<Root>/SinalControleX'
   */
  rtb_Sum = (ControlePosicao_Y.SinalControleX - -1.9491892662203381 *
             ControlePosicao_DW.TFPosicaoX_states[0]) - 0.949189266222495 *
    ControlePosicao_DW.TFPosicaoX_states[1];
  ControlePosicao_DW.TFPosicaoX_states[1] =
    ControlePosicao_DW.TFPosicaoX_states[0];
  ControlePosicao_DW.TFPosicaoX_states[0] = rtb_Sum;

  /* Signum: '<S71>/SignPreIntegrator' */
  if (rtb_IntegralGain < 0.0) {
    rtb_Merge = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    rtb_Merge = 1.0;
  } else {
    rtb_Merge = rtb_IntegralGain;
  }

  /* End of Signum: '<S71>/SignPreIntegrator' */

  /* Switch: '<S71>/Switch' incorporates:
   *  Constant: '<S71>/Constant1'
   *  DataTypeConversion: '<S71>/DataTypeConv2'
   *  Logic: '<S71>/AND3'
   *  RelationalOperator: '<S71>/Equal1'
   */
  if (rtb_NotEqual_o && (rtb_DataTypeConv1 == static_cast<int8_T>(rtb_Merge))) {
    rtb_IntegralGain = 0.0;
  }

  /* End of Switch: '<S71>/Switch' */

  /* Update for DiscreteIntegrator: '<S78>/Integrator' incorporates:
   *  DiscreteIntegrator: '<S73>/Filter'
   */
  ControlePosicao_DW.Integrator_DSTATE_i += 0.015 * rtb_IntegralGain;
  if (rtb_Filter_e > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 1;
    ControlePosicao_DW.Filter_PrevResetState_o = 1;
  } else {
    if (rtb_Filter_e < 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_p = -1;
    } else if (rtb_Filter_e == 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_p = 0;
    } else {
      ControlePosicao_DW.Integrator_PrevResetState_p = 2;
    }

    if (rtb_Filter_e < 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_o = -1;
    } else if (rtb_Filter_e == 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_o = 0;
    } else {
      ControlePosicao_DW.Filter_PrevResetState_o = 2;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S78>/Integrator' */

  /* Update for DiscreteIntegrator: '<S73>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b += 0.015 * rtb_PosRoboX;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoY' incorporates:
   *  Outport: '<Root>/SinalControleY'
   */
  rtb_Sum = (ControlePosicao_Y.SinalControleY - -1.9363679141114229 *
             ControlePosicao_DW.TFPosicaoY_states[0]) - 0.936367914533924 *
    ControlePosicao_DW.TFPosicaoY_states[1];
  ControlePosicao_DW.TFPosicaoY_states[1] =
    ControlePosicao_DW.TFPosicaoY_states[0];
  ControlePosicao_DW.TFPosicaoY_states[0] = rtb_Sum;
}

/* Model initialize function */
void ControlePosicaoModelClass::initialize()
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus((&ControlePosicao_M), (NULL));

  /* states (dwork) */
  (void) memset((void *)&ControlePosicao_DW, 0,
                sizeof(DW_ControlePosicao_T));

  /* external inputs */
  (void)memset(&ControlePosicao_U, 0, sizeof(ExtU_ControlePosicao_T));

  /* external outputs */
  (void) memset((void *)&ControlePosicao_Y, 0,
                sizeof(ExtY_ControlePosicao_T));

  /* InitializeConditions for DiscreteIntegrator: '<S34>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE = 0.0;
  ControlePosicao_DW.Integrator_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S29>/Filter' */
  ControlePosicao_DW.Filter_DSTATE = 0.0;
  ControlePosicao_DW.Filter_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S78>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  ControlePosicao_DW.Integrator_PrevResetState_p = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S73>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  ControlePosicao_DW.Filter_PrevResetState_o = 0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[1] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[1] = 0.0;
}

/* Model terminate function */
void ControlePosicaoModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControlePosicaoModelClass::ControlePosicaoModelClass()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControlePosicaoModelClass::~ControlePosicaoModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControlePosicao_T * ControlePosicaoModelClass::getRTM()
{
  return (&ControlePosicao_M);
}
