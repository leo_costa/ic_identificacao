/*
 * ControlePosicao.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.94
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Thu Dec 12 09:55:07 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_h_
#define RTW_HEADER_ControlePosicao_h_
#include <string.h>
#include <stddef.h>
#ifndef ControlePosicao_COMMON_INCLUDES_
# define ControlePosicao_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* ControlePosicao_COMMON_INCLUDES_ */

#include "ControlePosicao_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S34>/Integrator' */
  real_T Filter_DSTATE;                /* '<S29>/Filter' */
  real_T TFPosicaoX_states[2];         /* '<Root>/TFPosicaoX' */
  real_T Integrator_DSTATE_i;          /* '<S78>/Integrator' */
  real_T Filter_DSTATE_b;              /* '<S73>/Filter' */
  real_T TFPosicaoY_states[2];         /* '<Root>/TFPosicaoY' */
  int8_T Integrator_PrevResetState;    /* '<S34>/Integrator' */
  int8_T Filter_PrevResetState;        /* '<S29>/Filter' */
  int8_T Integrator_PrevResetState_p;  /* '<S78>/Integrator' */
  int8_T Filter_PrevResetState_o;      /* '<S73>/Filter' */
} DW_ControlePosicao_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T PosX;                         /* '<Root>/PosX' */
  real_T PosY;                         /* '<Root>/PosY' */
  real_T ReAlimX;                      /* '<Root>/ReAlimX' */
  real_T ReAlimY;                      /* '<Root>/ReAlimY' */
} ExtU_ControlePosicao_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T SinalControleX;               /* '<Root>/SinalControleX' */
  real_T SinalControleY;               /* '<Root>/SinalControleY' */
  real_T PosRoboX;                     /* '<Root>/PosRoboX' */
  real_T PosRoboY;                     /* '<Root>/PosRoboY' */
  real_T ErroX;                        /* '<Root>/ErroX' */
  real_T ErroY;                        /* '<Root>/ErroY' */
} ExtY_ControlePosicao_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControlePosicao_T {
  const char_T *errorStatus;
};

/* Class declaration for model ControlePosicao */
class ControlePosicaoModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_ControlePosicao_T ControlePosicao_U;

  /* External outputs */
  ExtY_ControlePosicao_T ControlePosicao_Y;

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  ControlePosicaoModelClass();

  /* Destructor */
  ~ControlePosicaoModelClass();

  /* Real-Time Model get method */
  RT_MODEL_ControlePosicao_T * getRTM();

  /* private data and function members */
 private:
  /* Block states */
  DW_ControlePosicao_T ControlePosicao_DW;

  /* Real-Time Model */
  RT_MODEL_ControlePosicao_T ControlePosicao_M;
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Scope1' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControlePosicao'
 * '<S1>'   : 'ControlePosicao/ControladorX'
 * '<S2>'   : 'ControlePosicao/ControladorY'
 * '<S3>'   : 'ControlePosicao/PID_Reset'
 * '<S4>'   : 'ControlePosicao/PID_Reset1'
 * '<S5>'   : 'ControlePosicao/ControladorX/Anti-windup'
 * '<S6>'   : 'ControlePosicao/ControladorX/D Gain'
 * '<S7>'   : 'ControlePosicao/ControladorX/Filter'
 * '<S8>'   : 'ControlePosicao/ControladorX/Filter ICs'
 * '<S9>'   : 'ControlePosicao/ControladorX/I Gain'
 * '<S10>'  : 'ControlePosicao/ControladorX/Ideal P Gain'
 * '<S11>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk'
 * '<S12>'  : 'ControlePosicao/ControladorX/Integrator'
 * '<S13>'  : 'ControlePosicao/ControladorX/Integrator ICs'
 * '<S14>'  : 'ControlePosicao/ControladorX/N Copy'
 * '<S15>'  : 'ControlePosicao/ControladorX/N Gain'
 * '<S16>'  : 'ControlePosicao/ControladorX/P Copy'
 * '<S17>'  : 'ControlePosicao/ControladorX/Parallel P Gain'
 * '<S18>'  : 'ControlePosicao/ControladorX/Reset Signal'
 * '<S19>'  : 'ControlePosicao/ControladorX/Saturation'
 * '<S20>'  : 'ControlePosicao/ControladorX/Saturation Fdbk'
 * '<S21>'  : 'ControlePosicao/ControladorX/Sum'
 * '<S22>'  : 'ControlePosicao/ControladorX/Sum Fdbk'
 * '<S23>'  : 'ControlePosicao/ControladorX/Tracking Mode'
 * '<S24>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum'
 * '<S25>'  : 'ControlePosicao/ControladorX/postSat Signal'
 * '<S26>'  : 'ControlePosicao/ControladorX/preSat Signal'
 * '<S27>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel'
 * '<S28>'  : 'ControlePosicao/ControladorX/D Gain/Internal Parameters'
 * '<S29>'  : 'ControlePosicao/ControladorX/Filter/Disc. Forward Euler Filter'
 * '<S30>'  : 'ControlePosicao/ControladorX/Filter ICs/Internal IC - Filter'
 * '<S31>'  : 'ControlePosicao/ControladorX/I Gain/Internal Parameters'
 * '<S32>'  : 'ControlePosicao/ControladorX/Ideal P Gain/Passthrough'
 * '<S33>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk/Disabled'
 * '<S34>'  : 'ControlePosicao/ControladorX/Integrator/Discrete'
 * '<S35>'  : 'ControlePosicao/ControladorX/Integrator ICs/Internal IC'
 * '<S36>'  : 'ControlePosicao/ControladorX/N Copy/Disabled'
 * '<S37>'  : 'ControlePosicao/ControladorX/N Gain/Internal Parameters'
 * '<S38>'  : 'ControlePosicao/ControladorX/P Copy/Disabled'
 * '<S39>'  : 'ControlePosicao/ControladorX/Parallel P Gain/Internal Parameters'
 * '<S40>'  : 'ControlePosicao/ControladorX/Reset Signal/External Reset'
 * '<S41>'  : 'ControlePosicao/ControladorX/Saturation/Enabled'
 * '<S42>'  : 'ControlePosicao/ControladorX/Saturation Fdbk/Disabled'
 * '<S43>'  : 'ControlePosicao/ControladorX/Sum/Sum_PID'
 * '<S44>'  : 'ControlePosicao/ControladorX/Sum Fdbk/Disabled'
 * '<S45>'  : 'ControlePosicao/ControladorX/Tracking Mode/Disabled'
 * '<S46>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum/Passthrough'
 * '<S47>'  : 'ControlePosicao/ControladorX/postSat Signal/Forward_Path'
 * '<S48>'  : 'ControlePosicao/ControladorX/preSat Signal/Forward_Path'
 * '<S49>'  : 'ControlePosicao/ControladorY/Anti-windup'
 * '<S50>'  : 'ControlePosicao/ControladorY/D Gain'
 * '<S51>'  : 'ControlePosicao/ControladorY/Filter'
 * '<S52>'  : 'ControlePosicao/ControladorY/Filter ICs'
 * '<S53>'  : 'ControlePosicao/ControladorY/I Gain'
 * '<S54>'  : 'ControlePosicao/ControladorY/Ideal P Gain'
 * '<S55>'  : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk'
 * '<S56>'  : 'ControlePosicao/ControladorY/Integrator'
 * '<S57>'  : 'ControlePosicao/ControladorY/Integrator ICs'
 * '<S58>'  : 'ControlePosicao/ControladorY/N Copy'
 * '<S59>'  : 'ControlePosicao/ControladorY/N Gain'
 * '<S60>'  : 'ControlePosicao/ControladorY/P Copy'
 * '<S61>'  : 'ControlePosicao/ControladorY/Parallel P Gain'
 * '<S62>'  : 'ControlePosicao/ControladorY/Reset Signal'
 * '<S63>'  : 'ControlePosicao/ControladorY/Saturation'
 * '<S64>'  : 'ControlePosicao/ControladorY/Saturation Fdbk'
 * '<S65>'  : 'ControlePosicao/ControladorY/Sum'
 * '<S66>'  : 'ControlePosicao/ControladorY/Sum Fdbk'
 * '<S67>'  : 'ControlePosicao/ControladorY/Tracking Mode'
 * '<S68>'  : 'ControlePosicao/ControladorY/Tracking Mode Sum'
 * '<S69>'  : 'ControlePosicao/ControladorY/postSat Signal'
 * '<S70>'  : 'ControlePosicao/ControladorY/preSat Signal'
 * '<S71>'  : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel'
 * '<S72>'  : 'ControlePosicao/ControladorY/D Gain/Internal Parameters'
 * '<S73>'  : 'ControlePosicao/ControladorY/Filter/Disc. Forward Euler Filter'
 * '<S74>'  : 'ControlePosicao/ControladorY/Filter ICs/Internal IC - Filter'
 * '<S75>'  : 'ControlePosicao/ControladorY/I Gain/Internal Parameters'
 * '<S76>'  : 'ControlePosicao/ControladorY/Ideal P Gain/Passthrough'
 * '<S77>'  : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk/Disabled'
 * '<S78>'  : 'ControlePosicao/ControladorY/Integrator/Discrete'
 * '<S79>'  : 'ControlePosicao/ControladorY/Integrator ICs/Internal IC'
 * '<S80>'  : 'ControlePosicao/ControladorY/N Copy/Disabled'
 * '<S81>'  : 'ControlePosicao/ControladorY/N Gain/Internal Parameters'
 * '<S82>'  : 'ControlePosicao/ControladorY/P Copy/Disabled'
 * '<S83>'  : 'ControlePosicao/ControladorY/Parallel P Gain/Internal Parameters'
 * '<S84>'  : 'ControlePosicao/ControladorY/Reset Signal/External Reset'
 * '<S85>'  : 'ControlePosicao/ControladorY/Saturation/Enabled'
 * '<S86>'  : 'ControlePosicao/ControladorY/Saturation Fdbk/Disabled'
 * '<S87>'  : 'ControlePosicao/ControladorY/Sum/Sum_PID'
 * '<S88>'  : 'ControlePosicao/ControladorY/Sum Fdbk/Disabled'
 * '<S89>'  : 'ControlePosicao/ControladorY/Tracking Mode/Disabled'
 * '<S90>'  : 'ControlePosicao/ControladorY/Tracking Mode Sum/Passthrough'
 * '<S91>'  : 'ControlePosicao/ControladorY/postSat Signal/Forward_Path'
 * '<S92>'  : 'ControlePosicao/ControladorY/preSat Signal/Forward_Path'
 * '<S93>'  : 'ControlePosicao/PID_Reset/If Action Subsystem'
 * '<S94>'  : 'ControlePosicao/PID_Reset/If Action Subsystem1'
 * '<S95>'  : 'ControlePosicao/PID_Reset1/If Action Subsystem'
 * '<S96>'  : 'ControlePosicao/PID_Reset1/If Action Subsystem1'
 */
#endif                                 /* RTW_HEADER_ControlePosicao_h_ */
