/*
 * ControlePosicao_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.94
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Thu Dec 12 09:55:07 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_types_h_
#define RTW_HEADER_ControlePosicao_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_ControlePosicao_T RT_MODEL_ControlePosicao_T;

#endif                                 /* RTW_HEADER_ControlePosicao_types_h_ */
