function [riseTimeX,riseTimeY,riseT] = riseTime(X,t)
%riseTime Calcula o tempo de subida do sistema

finalValue = abs(X(end) - X(1));
value10Pct = sign(X(end))*finalValue*0.1;
value90Pct = sign(X(end))*finalValue*0.9;
t10Pct = 0;
t90Pct = 0;
% 
% X = unique(X);
% t = unique(t);
% 
% t10Pct = interp1(X,t,value10Pct);
% t90Pct = interp1(X,t,value90Pct);

if X(end) < X(1)
%     aux = value10Pct;
%     value10Pct = value90Pct;
%     value90Pct = aux;
    
    for n=1 : length(X)
        if X(n) <= value90Pct && t90Pct == 0
            t90Pct = t(n);
        elseif X(n+1) <= value10Pct
            t10Pct = t(n);
            break;
        end
    end

else
    
    if value10Pct > value90Pct
        aux = value10Pct;
        value10Pct = value90Pct;
        value90Pct = aux;
    end
    
    for n=1 : length(X)
        if X(n) >= value10Pct && t10Pct == 0
            t10Pct = t(n);
        elseif X(n) >= value90Pct
            t90Pct = t(n);
            break;
        end
    end
end


riseT = abs(t90Pct - t10Pct);
riseTimeX = [t10Pct t90Pct];
riseTimeY = [value10Pct value90Pct];
end

