Signal Type: PRBS
Linear Amplitude: 0.2 m/s
Angular Amplitude: 2 rad/s
Switching time: 250 ms
Linear Offset: -0.1 m/s
Angular Offset: -1 rad/s
