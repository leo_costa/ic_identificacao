%% Código que realiza a identificação dos modelos de Função de Transferência do SSL
% clear; close all; clc;
%% Adiciona as funções auxiliares
addpath('importFunctions/');
%% Carrega os dados do experimento
Endereco = uigetdir(pwd,"Select the folder containing the experiment data");
% [ Time, Veli, VeliMeasured, Posii, Posij]; i = {x,y}; j = {x,y}
[TempoX, VelX, VelXMed, PosXX, PosYX, AngleX] = importfileXYW(Endereco + "/Data_X.csv");
[TempoY, VelY, VelYMed, PosXY, PosYY, AngleY] = importfileXYW(Endereco + "/Data_Y.csv");
[TempoW, VelW, VelWMed, PosXW, PosYW, AngleW] = importfileXYW(Endereco + "/Data_W.csv");

PosYW = zeros(length(PosYW),1);
%% Junta todos os dados
NAmostras = min(min(length(TempoX), length(TempoY)), length(TempoW));
Tempo = [TempoX(1:NAmostras), TempoY(1:NAmostras), TempoW(1:NAmostras)];
PosX  = [PosXX(1:NAmostras),  PosXY(1:NAmostras),  PosXW(1:NAmostras)];
PosY  = [PosYX(1:NAmostras),  PosYY(1:NAmostras),  PosYW(1:NAmostras)];
Angle = [AngleX(1:NAmostras), AngleY(1:NAmostras), AngleW(1:NAmostras)];
Vel   = [VelX(1:NAmostras),   VelY(1:NAmostras),   VelW(1:NAmostras)];

%% Cria as estruturas de dados para usar na identificação dos modelos
PosicaoInicialX = PosX(1, :);
PosicaoInicialY = PosY(1, :);
PosicaoInicialW = Angle(1, :);

% Normalização da saída para partir do zero
PosX = PosX - PosicaoInicialX;
PosY = PosY - PosicaoInicialY;
Angle = Angle - PosicaoInicialW;

Tsample = 0.015;

%%
XRobo = iddata([PosX(:, 1), PosY(:, 1), Angle(:, 1)], Vel(:, 1), Tsample);
YRobo = iddata([PosX(:, 2), PosY(:, 2), Angle(:, 2)], Vel(:, 2), Tsample);
WRobo = iddata([PosX(:, 3), PosY(:, 3), Angle(:, 3)], Vel(:, 3), Tsample);

%% Mostra os gráficos dos dados carregados
subplot(2,3,1);
plot(XRobo);
grid on;
subplot(2,3,2);
plot(YRobo);
grid on;
subplot(2,3,3);
plot(WRobo);
grid on;

%% Cria os modelos
% Transfer function estimation options
Options = tfestOptions;
Options.Display = 'off';
Options.WeightingFilter = [];

 % 2 polos e 0 zeros
tfPosicaoXRobo = tfest(XRobo, 1, 0, Options, 'Ts', Tsample)
tfPosicaoYRobo = tfest(YRobo, 1, 0, Options, 'Ts', Tsample)
tfPosicaoWRobo = tfest(WRobo, 1, 0, Options, 'Ts', Tsample)

%% Mostra as FTs e troca a variável de z^-1 para z

% tfRobotX     = tf([tfPosicaoXRobo.Numerator 0 0], tfPosicaoXRobo.Denominator,...
%                   Tsample,'Variable','z')
% tfRobotY     = tf([tfPosicaoYRobo.Numerator 0 0], tfPosicaoYRobo.Denominator,...
%                   Tsample,'Variable','z')
% tfRobotTheta = tf([tfPosicaoWRobo.Numerator 0 0], tfPosicaoWRobo.Denominator,...
%                   Tsample,'Variable','z')

%% Compara os modelos
subplot(2,3,4);
compare(XRobo(1:NAmostras), tfPosicaoXRobo);
grid on;

subplot(2,3,5);
compare(YRobo(1:NAmostras), tfPosicaoYRobo);
grid on;

subplot(2,3,6);
compare(WRobo(1:NAmostras), tfPosicaoWRobo);
grid on;

%% Espaço de estados
%     | PxIx PyIx PwIx |
% H = | PxIy PyIy PwIy |
%     | PxIw PyIw PwIw |

H = [tf(tfPosicaoXRobo.Numerator(1), tfPosicaoXRobo.Denominator(1), Tsample), tf(tfPosicaoYRobo.Numerator(1), tfPosicaoYRobo.Denominator(1), Tsample), 0;
     tf(tfPosicaoXRobo.Numerator(2), tfPosicaoXRobo.Denominator(2), Tsample), tf(tfPosicaoYRobo.Numerator(2), tfPosicaoYRobo.Denominator(2), Tsample), 0;
     0                                                                      , tf(tfPosicaoYRobo.Numerator(3), tfPosicaoYRobo.Denominator(3), Tsample), tf(tfPosicaoWRobo.Numerator(3), tfPosicaoWRobo.Denominator(3), Tsample)];

RoboSS = ss(H);
RoboSS_Disc = RoboSS;

figure;
step(RoboSS, 5); grid on;

%% Realimentacao de estados
% P = [1 0.02064 0.558+0.3037i 0.558-0.3037i 0.0664+0.8289i 0.0664-0.8289i];
% K = place(RoboSS.A, RoboSS.B, P);
% eig(RoboSS.A - RoboSS.B*K)
% disp(K);
% RoboSS = d2c(RoboSS_Disc);

% Q = [1.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00;
%      0.00 1.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00;
%      0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00 0.00;
%      0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00 0.00;
%      0.00 0.00 0.00 0.00 1.00 0.00 0.00 0.00 0.00;
%      0.00 0.00 0.00 0.00 0.00 1.00 0.00 0.00 0.00;
%      0.00 0.00 0.00 0.00 0.00 0.00 1.00 0.00 0.00;
%      0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.00 0.00;
%      0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.00];
% 
% R = 1;
% 
% A = RoboSS.A;
% B = RoboSS.B;
% C = RoboSS.C;
% D = RoboSS.D;
% % calc_lqr_pid Calcula os ganhos Kp Ki e Kd do controlador PID via LQR
% A_b = [A zeros(6,3); C zeros(3,3)];
% B_b = [B; zeros(3,3)];
% k_b = lqr(A_b, B_b, Q, R); % k = [kp_b ki_b]
% 
% kp_b = k_b(1, 1:2);
% ki_b = k_b(1, 3);
% C_b = [C; C*A-C*B*kp_b];
% 
% kp_kd = kp_b/C_b;
% 
% kp = kp_kd(1);
% kd = kp_kd(2);
% ki = (eye(1) + kd*C*B)*ki_b;
