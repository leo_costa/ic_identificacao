function index = returnIndex(bestAbsoluteIndex, nTests)
%returnIndex Retorna os indices dos melhores modelos baseado nos indices 
% absolutos dos melhores
index = zeros(2, length(bestAbsoluteIndex));
bestAbsoluteIndex = bestAbsoluteIndex -1;

for i=1 :1: length(bestAbsoluteIndex)
    index(1,i) = int16(idivide(bestAbsoluteIndex(i), nTests)+1);%Ordem - c
    index(2,i) = int16(mod(bestAbsoluteIndex(i), nTests)+1);%Experimento - r
end

