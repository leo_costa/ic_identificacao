function [IDData,nDados] = getIDData(Endereco)
%getIDData Retorna o objeto de dados para a identificação a partir do endereço da pasta de dados e a quantidade de dados

%% Carrega os dados do experimento
[  ~   , VelX, VelXMed, PosXX,   ~  ] = importfileXY(Endereco+"Data_X.csv", [2, Inf]);
[  ~   , VelY, VelYMed, ~    , PosYY] = importfileXY(Endereco+"Data_Y.csv", [2, Inf]);
[Tempo , VelW, VelWMed, Angle]        =  importfileW(Endereco+"Data_W.csv", [2, Inf]);

%% Cria as estruturas de dados para usar na identificação dos modelos
sampleTime = round((Tempo(end)-Tempo(1))/length(Tempo), 3);
fprintf('Sample time = %f s\n', sampleTime);

IDData = iddata([PosXX,PosYY,Angle], [VelX,VelY,VelW], sampleTime);

IDData.InputName  = {'VX','VY','VW'};
IDData.OutputName = {'Posição X', 'Posição Y', 'Angulo'};
IDData.InputUnit  = {'m/s','m/s','m/s'};
IDData.OutputUnit = {'m','m','rad'};

nDados = length(Tempo);
end