function middle = findMiddle(length)
%findMiddle Retorna a metade inteira para o tamanho fornecido

middle = length;

if mod(middle,2) ~= 0
    middle = middle -1;
    middle = middle/2;
else
    middle = middle/2;
end

end

