function testNames = getTestFolderNames(rootFolder)
%getTestCount Retorna o nome dos testes que existem no diretório
%fornecido (rootFolder)

% Get a list of all files and folders in this folder.
files = dir(rootFolder);
% Get a logical vector that tells which is a directory.
dirFlags = [files.isdir] & ~strcmp({files.name},'.') &...
    ~strcmp({files.name},'..');
% Extract only those that are directories.
subFolders = files(dirFlags);
% Print folder names to command window.
Names = "";

for k = 1 : length(subFolders)
  Names = [Names subFolders(k).name];
end

testNames = Names(1, 2:1+length(subFolders));
end

