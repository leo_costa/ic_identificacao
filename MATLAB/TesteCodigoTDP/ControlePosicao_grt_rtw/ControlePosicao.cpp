/*
 * ControlePosicao.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.103
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Tue Jan  7 15:16:02 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#include "ControlePosicao.h"
#include "ControlePosicao_private.h"

/*
 * Output and update for action system:
 *    '<S4>/If Action Subsystem'
 *    '<S4>/If Action Subsystem1'
 *    '<S5>/If Action Subsystem'
 *    '<S5>/If Action Subsystem1'
 *    '<S6>/If Action Subsystem'
 *    '<S6>/If Action Subsystem1'
 */
void ControlePosic_IfActionSubsystem(real_T rtu_In1, real_T *rty_Out1)
{
  /* Inport: '<S139>/In1' */
  *rty_Out1 = rtu_In1;
}

/* Model step function */
void ControlePosicaoModelClass::step()
{
  real_T rtb_Sum;
  real_T rtb_Merge;
  real_T rtb_FilterCoefficient;
  real_T rtb_Filter_e;
  real_T rtb_Integrator_p;
  boolean_T rtb_NotEqual;
  int8_T rtb_DataTypeConv2;
  real_T rtb_Filter_m;
  int8_T rtb_DataTypeConv1;
  real_T rtb_PosRoboX;
  real_T rtb_Integrator_d;
  boolean_T rtb_NotEqual_o;
  real_T rtb_ErroW;
  boolean_T rtb_Equal1_lz;
  real_T rtb_PosRoboY;
  real_T rtb_IntegralGain;
  boolean_T rtb_NotEqual_p2;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Inport: '<Root>/PosX'
   *  Inport: '<Root>/ReAlimX'
   */
  rtb_Sum = ControlePosicao_U.PosX - ControlePosicao_U.ReAlimX;

  /* If: '<S4>/If' incorporates:
   *  Constant: '<S4>/Constant'
   *  Constant: '<S4>/Constant1'
   */
  if (rtb_Sum < 0.005) {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem' incorporates:
     *  ActionPort: '<S139>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S140>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem1' */
  }

  /* End of If: '<S4>/If' */

  /* DiscreteIntegrator: '<S80>/Integrator' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState <= 0))
  {
    ControlePosicao_DW.Integrator_DSTATE = 0.0;
  }

  /* DiscreteIntegrator: '<S75>/Filter' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Filter_PrevResetState <= 0)) {
    ControlePosicao_DW.Filter_DSTATE = 0.0;
  }

  /* Gain: '<S83>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S75>/Filter'
   *  Gain: '<S74>/Derivative Gain'
   *  Sum: '<S75>/SumD'
   */
  rtb_FilterCoefficient = (0.102966199285408 * rtb_Sum -
    ControlePosicao_DW.Filter_DSTATE) * 1.22722477558936;

  /* Sum: '<S89>/Sum' incorporates:
   *  DiscreteIntegrator: '<S80>/Integrator'
   *  Gain: '<S85>/Proportional Gain'
   */
  rtb_Filter_e = (1.80266580450494 * rtb_Sum +
                  ControlePosicao_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

  /* DeadZone: '<S73>/DeadZone' */
  if (rtb_Filter_e > 1.75) {
    rtb_Integrator_p = rtb_Filter_e - 1.75;
  } else if (rtb_Filter_e >= -1.75) {
    rtb_Integrator_p = 0.0;
  } else {
    rtb_Integrator_p = rtb_Filter_e - -1.75;
  }

  /* End of DeadZone: '<S73>/DeadZone' */

  /* RelationalOperator: '<S73>/NotEqual' incorporates:
   *  Gain: '<S73>/ZeroGain'
   */
  rtb_NotEqual = (0.0 != rtb_Integrator_p);

  /* Signum: '<S73>/SignPreSat' */
  if (rtb_Integrator_p < 0.0) {
    rtb_Integrator_p = -1.0;
  } else {
    if (rtb_Integrator_p > 0.0) {
      rtb_Integrator_p = 1.0;
    }
  }

  /* End of Signum: '<S73>/SignPreSat' */

  /* DataTypeConversion: '<S73>/DataTypeConv1' */
  rtb_DataTypeConv2 = static_cast<int8_T>(rtb_Integrator_p);

  /* Gain: '<S77>/Integral Gain' */
  rtb_Integrator_p = 0.0195239502119297 * rtb_Sum;

  /* Saturate: '<S87>/Saturation' */
  if (rtb_Filter_e > 1.75) {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = 1.75;
  } else if (rtb_Filter_e < -1.75) {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = -1.75;
  } else {
    /* Outport: '<Root>/SinalControleX' */
    ControlePosicao_Y.SinalControleX = rtb_Filter_e;
  }

  /* End of Saturate: '<S87>/Saturation' */

  /* Outport: '<Root>/ErroX' */
  ControlePosicao_Y.ErroX = rtb_Sum;

  /* Outport: '<Root>/PosRoboX' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoX'
   */
  ControlePosicao_Y.PosRoboX = 0.000349407810544228 *
    ControlePosicao_DW.TFPosicaoX_states[1];

  /* Sum: '<Root>/Sum3' incorporates:
   *  Inport: '<Root>/PosY'
   *  Inport: '<Root>/ReAlimY'
   */
  rtb_Filter_m = ControlePosicao_U.PosY - ControlePosicao_U.ReAlimY;

  /* If: '<S5>/If' incorporates:
   *  Constant: '<S5>/Constant'
   *  Constant: '<S5>/Constant1'
   */
  if (rtb_Filter_m < 0.005) {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem' incorporates:
     *  ActionPort: '<S141>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Filter_e);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S142>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Filter_e);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem1' */
  }

  /* End of If: '<S5>/If' */

  /* DiscreteIntegrator: '<S124>/Integrator' */
  if ((rtb_Filter_e != 0.0) || (ControlePosicao_DW.Integrator_PrevResetState_p
       != 0)) {
    ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  }

  /* DiscreteIntegrator: '<S119>/Filter' */
  if ((rtb_Filter_e != 0.0) || (ControlePosicao_DW.Filter_PrevResetState_o != 0))
  {
    ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  }

  /* Gain: '<S127>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S119>/Filter'
   *  Gain: '<S118>/Derivative Gain'
   *  Sum: '<S119>/SumD'
   */
  rtb_PosRoboX = (0.0252510897395415 * rtb_Filter_m -
                  ControlePosicao_DW.Filter_DSTATE_b) * 1.70599272259623;

  /* Sum: '<S133>/Sum' incorporates:
   *  DiscreteIntegrator: '<S124>/Integrator'
   *  Gain: '<S129>/Proportional Gain'
   */
  rtb_Sum = (2.62167273219043 * rtb_Filter_m +
             ControlePosicao_DW.Integrator_DSTATE_i) + rtb_PosRoboX;

  /* DeadZone: '<S117>/DeadZone' */
  if (rtb_Sum > 1.75) {
    rtb_Integrator_d = rtb_Sum - 1.75;
  } else if (rtb_Sum >= -1.75) {
    rtb_Integrator_d = 0.0;
  } else {
    rtb_Integrator_d = rtb_Sum - -1.75;
  }

  /* End of DeadZone: '<S117>/DeadZone' */

  /* RelationalOperator: '<S117>/NotEqual' incorporates:
   *  Gain: '<S117>/ZeroGain'
   */
  rtb_NotEqual_o = (0.0 != rtb_Integrator_d);

  /* Signum: '<S117>/SignPreSat' */
  if (rtb_Integrator_d < 0.0) {
    rtb_Integrator_d = -1.0;
  } else {
    if (rtb_Integrator_d > 0.0) {
      rtb_Integrator_d = 1.0;
    }
  }

  /* End of Signum: '<S117>/SignPreSat' */

  /* DataTypeConversion: '<S117>/DataTypeConv1' */
  rtb_DataTypeConv1 = static_cast<int8_T>(rtb_Integrator_d);

  /* Gain: '<S121>/Integral Gain' */
  rtb_Integrator_d = 0.0396327631747005 * rtb_Filter_m;

  /* Signum: '<S117>/SignPreIntegrator' */
  if (rtb_Integrator_d < 0.0) {
    rtb_PosRoboY = -1.0;
  } else if (rtb_Integrator_d > 0.0) {
    rtb_PosRoboY = 1.0;
  } else {
    rtb_PosRoboY = rtb_Integrator_d;
  }

  /* End of Signum: '<S117>/SignPreIntegrator' */

  /* RelationalOperator: '<S117>/Equal1' incorporates:
   *  DataTypeConversion: '<S117>/DataTypeConv2'
   */
  rtb_Equal1_lz = (rtb_DataTypeConv1 == static_cast<int8_T>(rtb_PosRoboY));

  /* Saturate: '<S131>/Saturation' */
  if (rtb_Sum > 1.75) {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = 1.75;
  } else if (rtb_Sum < -1.75) {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = -1.75;
  } else {
    /* Outport: '<Root>/SinalControleY' */
    ControlePosicao_Y.SinalControleY = rtb_Sum;
  }

  /* End of Saturate: '<S131>/Saturation' */

  /* Outport: '<Root>/ErroY' */
  ControlePosicao_Y.ErroY = rtb_Filter_m;

  /* DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  rtb_PosRoboY = 0.000637817571138752 * ControlePosicao_DW.TFPosicaoY_states[1];

  /* Outport: '<Root>/PosRoboY' */
  ControlePosicao_Y.PosRoboY = rtb_PosRoboY;

  /* Sum: '<Root>/Sum1' incorporates:
   *  Inport: '<Root>/PosAngular'
   *  Inport: '<Root>/ReAlimAngular'
   */
  rtb_ErroW = ControlePosicao_U.PosAngular - ControlePosicao_U.ReAlimAngular;

  /* If: '<S6>/If' incorporates:
   *  Constant: '<S6>/Constant'
   *  Constant: '<S6>/Constant1'
   */
  if (rtb_ErroW < 0.005) {
    /* Outputs for IfAction SubSystem: '<S6>/If Action Subsystem' incorporates:
     *  ActionPort: '<S143>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_PosRoboY);

    /* End of Outputs for SubSystem: '<S6>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S6>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S144>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_PosRoboY);

    /* End of Outputs for SubSystem: '<S6>/If Action Subsystem1' */
  }

  /* End of If: '<S6>/If' */

  /* DiscreteIntegrator: '<S36>/Integrator' */
  if ((rtb_PosRoboY != 0.0) || (ControlePosicao_DW.Integrator_PrevResetState_m
       != 0)) {
    ControlePosicao_DW.Integrator_DSTATE_b = 0.0;
  }

  /* DiscreteIntegrator: '<S31>/Filter' */
  if ((rtb_PosRoboY != 0.0) || (ControlePosicao_DW.Filter_PrevResetState_p != 0))
  {
    ControlePosicao_DW.Filter_DSTATE_d = 0.0;
  }

  /* Gain: '<S39>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S31>/Filter'
   *  Gain: '<S30>/Derivative Gain'
   *  Sum: '<S31>/SumD'
   */
  rtb_Filter_m = (0.648198669917731 * rtb_ErroW -
                  ControlePosicao_DW.Filter_DSTATE_d) * 13.765961843175;

  /* Sum: '<S45>/Sum' incorporates:
   *  DiscreteIntegrator: '<S36>/Integrator'
   *  Gain: '<S41>/Proportional Gain'
   */
  rtb_Sum = (6.94202553376377 * rtb_ErroW +
             ControlePosicao_DW.Integrator_DSTATE_b) + rtb_Filter_m;

  /* DeadZone: '<S29>/DeadZone' */
  if (rtb_Sum > 6.25) {
    rtb_IntegralGain = rtb_Sum - 6.25;
  } else if (rtb_Sum >= -6.25) {
    rtb_IntegralGain = 0.0;
  } else {
    rtb_IntegralGain = rtb_Sum - -6.25;
  }

  /* End of DeadZone: '<S29>/DeadZone' */

  /* RelationalOperator: '<S29>/NotEqual' incorporates:
   *  Gain: '<S29>/ZeroGain'
   */
  rtb_NotEqual_p2 = (0.0 != rtb_IntegralGain);

  /* Signum: '<S29>/SignPreSat' */
  if (rtb_IntegralGain < 0.0) {
    rtb_IntegralGain = -1.0;
  } else {
    if (rtb_IntegralGain > 0.0) {
      rtb_IntegralGain = 1.0;
    }
  }

  /* End of Signum: '<S29>/SignPreSat' */

  /* DataTypeConversion: '<S29>/DataTypeConv1' */
  rtb_DataTypeConv1 = static_cast<int8_T>(rtb_IntegralGain);

  /* Gain: '<S33>/Integral Gain' */
  rtb_IntegralGain = 0.436430943774027 * rtb_ErroW;

  /* Saturate: '<S43>/Saturation' */
  if (rtb_Sum > 6.25) {
    /* Outport: '<Root>/SinalControleW' */
    ControlePosicao_Y.SinalControleW = 6.25;
  } else if (rtb_Sum < -6.25) {
    /* Outport: '<Root>/SinalControleW' */
    ControlePosicao_Y.SinalControleW = -6.25;
  } else {
    /* Outport: '<Root>/SinalControleW' */
    ControlePosicao_Y.SinalControleW = rtb_Sum;
  }

  /* End of Saturate: '<S43>/Saturation' */

  /* Outport: '<Root>/ErroW' */
  ControlePosicao_Y.ErroW = rtb_ErroW;

  /* Outport: '<Root>/PosRoboAngular' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoAngular'
   */
  ControlePosicao_Y.PosRoboAngular = 0.00162307861372471 *
    ControlePosicao_DW.TFPosicaoAngular_states[1];

  /* Signum: '<S73>/SignPreIntegrator' */
  if (rtb_Integrator_p < 0.0) {
    rtb_Sum = -1.0;
  } else if (rtb_Integrator_p > 0.0) {
    rtb_Sum = 1.0;
  } else {
    rtb_Sum = rtb_Integrator_p;
  }

  /* End of Signum: '<S73>/SignPreIntegrator' */

  /* Switch: '<S73>/Switch' incorporates:
   *  Constant: '<S73>/Constant1'
   *  DataTypeConversion: '<S73>/DataTypeConv2'
   *  Logic: '<S73>/AND3'
   *  RelationalOperator: '<S73>/Equal1'
   */
  if (rtb_NotEqual && (rtb_DataTypeConv2 == static_cast<int8_T>(rtb_Sum))) {
    rtb_Integrator_p = 0.0;
  }

  /* End of Switch: '<S73>/Switch' */

  /* Update for DiscreteIntegrator: '<S80>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE += 0.015 * rtb_Integrator_p;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S80>/Integrator' */

  /* Update for DiscreteIntegrator: '<S75>/Filter' */
  ControlePosicao_DW.Filter_DSTATE += 0.015 * rtb_FilterCoefficient;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S75>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoX' incorporates:
   *  Outport: '<Root>/SinalControleX'
   */
  rtb_Sum = (ControlePosicao_Y.SinalControleX - -1.97003408267869 *
             ControlePosicao_DW.TFPosicaoX_states[0]) - 0.970034082246672 *
    ControlePosicao_DW.TFPosicaoX_states[1];
  ControlePosicao_DW.TFPosicaoX_states[1] =
    ControlePosicao_DW.TFPosicaoX_states[0];
  ControlePosicao_DW.TFPosicaoX_states[0] = rtb_Sum;

  /* Switch: '<S117>/Switch' incorporates:
   *  Constant: '<S117>/Constant1'
   *  Logic: '<S117>/AND3'
   */
  if (rtb_NotEqual_o && rtb_Equal1_lz) {
    rtb_Integrator_d = 0.0;
  }

  /* End of Switch: '<S117>/Switch' */

  /* Update for DiscreteIntegrator: '<S124>/Integrator' incorporates:
   *  DiscreteIntegrator: '<S119>/Filter'
   */
  ControlePosicao_DW.Integrator_DSTATE_i += 0.015 * rtb_Integrator_d;
  if (rtb_Filter_e > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 1;
    ControlePosicao_DW.Filter_PrevResetState_o = 1;
  } else {
    if (rtb_Filter_e < 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_p = -1;
    } else if (rtb_Filter_e == 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_p = 0;
    } else {
      ControlePosicao_DW.Integrator_PrevResetState_p = 2;
    }

    if (rtb_Filter_e < 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_o = -1;
    } else if (rtb_Filter_e == 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_o = 0;
    } else {
      ControlePosicao_DW.Filter_PrevResetState_o = 2;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S124>/Integrator' */

  /* Update for DiscreteIntegrator: '<S119>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b += 0.015 * rtb_PosRoboX;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoY' incorporates:
   *  Outport: '<Root>/SinalControleY'
   */
  rtb_Sum = (ControlePosicao_Y.SinalControleY - -1.93989434819277 *
             ControlePosicao_DW.TFPosicaoY_states[0]) - 0.939900946935122 *
    ControlePosicao_DW.TFPosicaoY_states[1];
  ControlePosicao_DW.TFPosicaoY_states[1] =
    ControlePosicao_DW.TFPosicaoY_states[0];
  ControlePosicao_DW.TFPosicaoY_states[0] = rtb_Sum;

  /* Signum: '<S29>/SignPreIntegrator' */
  if (rtb_IntegralGain < 0.0) {
    rtb_Merge = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    rtb_Merge = 1.0;
  } else {
    rtb_Merge = rtb_IntegralGain;
  }

  /* End of Signum: '<S29>/SignPreIntegrator' */

  /* Switch: '<S29>/Switch' incorporates:
   *  Constant: '<S29>/Constant1'
   *  DataTypeConversion: '<S29>/DataTypeConv2'
   *  Logic: '<S29>/AND3'
   *  RelationalOperator: '<S29>/Equal1'
   */
  if (rtb_NotEqual_p2 && (rtb_DataTypeConv1 == static_cast<int8_T>(rtb_Merge)))
  {
    rtb_IntegralGain = 0.0;
  }

  /* End of Switch: '<S29>/Switch' */

  /* Update for DiscreteIntegrator: '<S36>/Integrator' incorporates:
   *  DiscreteIntegrator: '<S31>/Filter'
   */
  ControlePosicao_DW.Integrator_DSTATE_b += 0.015 * rtb_IntegralGain;
  if (rtb_PosRoboY > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_m = 1;
    ControlePosicao_DW.Filter_PrevResetState_p = 1;
  } else {
    if (rtb_PosRoboY < 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_m = -1;
    } else if (rtb_PosRoboY == 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_m = 0;
    } else {
      ControlePosicao_DW.Integrator_PrevResetState_m = 2;
    }

    if (rtb_PosRoboY < 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_p = -1;
    } else if (rtb_PosRoboY == 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_p = 0;
    } else {
      ControlePosicao_DW.Filter_PrevResetState_p = 2;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S36>/Integrator' */

  /* Update for DiscreteIntegrator: '<S31>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_d += 0.015 * rtb_Filter_m;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' incorporates:
   *  Outport: '<Root>/SinalControleW'
   */
  rtb_Sum = (ControlePosicao_Y.SinalControleW - -1.90090134081304 *
             ControlePosicao_DW.TFPosicaoAngular_states[0]) - 0.900906322193617 *
    ControlePosicao_DW.TFPosicaoAngular_states[1];
  ControlePosicao_DW.TFPosicaoAngular_states[1] =
    ControlePosicao_DW.TFPosicaoAngular_states[0];
  ControlePosicao_DW.TFPosicaoAngular_states[0] = rtb_Sum;
}

/* Model initialize function */
void ControlePosicaoModelClass::initialize()
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus((&ControlePosicao_M), (NULL));

  /* states (dwork) */
  (void) memset((void *)&ControlePosicao_DW, 0,
                sizeof(DW_ControlePosicao_T));

  /* external inputs */
  (void)memset(&ControlePosicao_U, 0, sizeof(ExtU_ControlePosicao_T));

  /* external outputs */
  (void) memset((void *)&ControlePosicao_Y, 0,
                sizeof(ExtY_ControlePosicao_T));

  /* InitializeConditions for DiscreteIntegrator: '<S80>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE = 0.0;
  ControlePosicao_DW.Integrator_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S75>/Filter' */
  ControlePosicao_DW.Filter_DSTATE = 0.0;
  ControlePosicao_DW.Filter_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S124>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  ControlePosicao_DW.Integrator_PrevResetState_p = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S119>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  ControlePosicao_DW.Filter_PrevResetState_o = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S36>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_b = 0.0;
  ControlePosicao_DW.Integrator_PrevResetState_m = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S31>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_d = 0.0;
  ControlePosicao_DW.Filter_PrevResetState_p = 0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  ControlePosicao_DW.TFPosicaoAngular_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[1] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[1] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  ControlePosicao_DW.TFPosicaoAngular_states[1] = 0.0;
}

/* Model terminate function */
void ControlePosicaoModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControlePosicaoModelClass::ControlePosicaoModelClass()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControlePosicaoModelClass::~ControlePosicaoModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControlePosicao_T * ControlePosicaoModelClass::getRTM()
{
  return (&ControlePosicao_M);
}
