/*
 * ControlePosicao.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.103
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Tue Jan  7 15:16:02 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_h_
#define RTW_HEADER_ControlePosicao_h_
#include <string.h>
#include <stddef.h>
#ifndef ControlePosicao_COMMON_INCLUDES_
# define ControlePosicao_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* ControlePosicao_COMMON_INCLUDES_ */

#include "ControlePosicao_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S80>/Integrator' */
  real_T Filter_DSTATE;                /* '<S75>/Filter' */
  real_T TFPosicaoX_states[2];         /* '<Root>/TFPosicaoX' */
  real_T Integrator_DSTATE_i;          /* '<S124>/Integrator' */
  real_T Filter_DSTATE_b;              /* '<S119>/Filter' */
  real_T TFPosicaoY_states[2];         /* '<Root>/TFPosicaoY' */
  real_T Integrator_DSTATE_b;          /* '<S36>/Integrator' */
  real_T Filter_DSTATE_d;              /* '<S31>/Filter' */
  real_T TFPosicaoAngular_states[2];   /* '<Root>/TFPosicaoAngular' */
  int8_T Integrator_PrevResetState;    /* '<S80>/Integrator' */
  int8_T Filter_PrevResetState;        /* '<S75>/Filter' */
  int8_T Integrator_PrevResetState_p;  /* '<S124>/Integrator' */
  int8_T Filter_PrevResetState_o;      /* '<S119>/Filter' */
  int8_T Integrator_PrevResetState_m;  /* '<S36>/Integrator' */
  int8_T Filter_PrevResetState_p;      /* '<S31>/Filter' */
} DW_ControlePosicao_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T PosX;                         /* '<Root>/PosX' */
  real_T PosY;                         /* '<Root>/PosY' */
  real_T ReAlimX;                      /* '<Root>/ReAlimX' */
  real_T ReAlimY;                      /* '<Root>/ReAlimY' */
  real_T PosAngular;                   /* '<Root>/PosAngular' */
  real_T ReAlimAngular;                /* '<Root>/ReAlimAngular' */
} ExtU_ControlePosicao_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T SinalControleX;               /* '<Root>/SinalControleX' */
  real_T SinalControleY;               /* '<Root>/SinalControleY' */
  real_T PosRoboX;                     /* '<Root>/PosRoboX' */
  real_T PosRoboY;                     /* '<Root>/PosRoboY' */
  real_T ErroX;                        /* '<Root>/ErroX' */
  real_T ErroY;                        /* '<Root>/ErroY' */
  real_T SinalControleW;               /* '<Root>/SinalControleW' */
  real_T PosRoboAngular;               /* '<Root>/PosRoboAngular' */
  real_T ErroW;                        /* '<Root>/ErroW' */
} ExtY_ControlePosicao_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControlePosicao_T {
  const char_T *errorStatus;
};

/* Class declaration for model ControlePosicao */
class ControlePosicaoModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_ControlePosicao_T ControlePosicao_U;

  /* External outputs */
  ExtY_ControlePosicao_T ControlePosicao_Y;

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  ControlePosicaoModelClass();

  /* Destructor */
  ~ControlePosicaoModelClass();

  /* Real-Time Model get method */
  RT_MODEL_ControlePosicao_T * getRTM();

  /* private data and function members */
 private:
  /* Block states */
  DW_ControlePosicao_T ControlePosicao_DW;

  /* Real-Time Model */
  RT_MODEL_ControlePosicao_T ControlePosicao_M;
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Scope1' : Unused code path elimination
 * Block '<Root>/Scope2' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControlePosicao'
 * '<S1>'   : 'ControlePosicao/ControladorAngular'
 * '<S2>'   : 'ControlePosicao/ControladorX'
 * '<S3>'   : 'ControlePosicao/ControladorY'
 * '<S4>'   : 'ControlePosicao/PID_Reset'
 * '<S5>'   : 'ControlePosicao/PID_Reset1'
 * '<S6>'   : 'ControlePosicao/PID_Reset2'
 * '<S7>'   : 'ControlePosicao/ControladorAngular/Anti-windup'
 * '<S8>'   : 'ControlePosicao/ControladorAngular/D Gain'
 * '<S9>'   : 'ControlePosicao/ControladorAngular/Filter'
 * '<S10>'  : 'ControlePosicao/ControladorAngular/Filter ICs'
 * '<S11>'  : 'ControlePosicao/ControladorAngular/I Gain'
 * '<S12>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain'
 * '<S13>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk'
 * '<S14>'  : 'ControlePosicao/ControladorAngular/Integrator'
 * '<S15>'  : 'ControlePosicao/ControladorAngular/Integrator ICs'
 * '<S16>'  : 'ControlePosicao/ControladorAngular/N Copy'
 * '<S17>'  : 'ControlePosicao/ControladorAngular/N Gain'
 * '<S18>'  : 'ControlePosicao/ControladorAngular/P Copy'
 * '<S19>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain'
 * '<S20>'  : 'ControlePosicao/ControladorAngular/Reset Signal'
 * '<S21>'  : 'ControlePosicao/ControladorAngular/Saturation'
 * '<S22>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk'
 * '<S23>'  : 'ControlePosicao/ControladorAngular/Sum'
 * '<S24>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk'
 * '<S25>'  : 'ControlePosicao/ControladorAngular/Tracking Mode'
 * '<S26>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum'
 * '<S27>'  : 'ControlePosicao/ControladorAngular/postSat Signal'
 * '<S28>'  : 'ControlePosicao/ControladorAngular/preSat Signal'
 * '<S29>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping Parallel'
 * '<S30>'  : 'ControlePosicao/ControladorAngular/D Gain/Internal Parameters'
 * '<S31>'  : 'ControlePosicao/ControladorAngular/Filter/Disc. Forward Euler Filter'
 * '<S32>'  : 'ControlePosicao/ControladorAngular/Filter ICs/Internal IC - Filter'
 * '<S33>'  : 'ControlePosicao/ControladorAngular/I Gain/Internal Parameters'
 * '<S34>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain/Passthrough'
 * '<S35>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk/Disabled'
 * '<S36>'  : 'ControlePosicao/ControladorAngular/Integrator/Discrete'
 * '<S37>'  : 'ControlePosicao/ControladorAngular/Integrator ICs/Internal IC'
 * '<S38>'  : 'ControlePosicao/ControladorAngular/N Copy/Disabled'
 * '<S39>'  : 'ControlePosicao/ControladorAngular/N Gain/Internal Parameters'
 * '<S40>'  : 'ControlePosicao/ControladorAngular/P Copy/Disabled'
 * '<S41>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain/Internal Parameters'
 * '<S42>'  : 'ControlePosicao/ControladorAngular/Reset Signal/External Reset'
 * '<S43>'  : 'ControlePosicao/ControladorAngular/Saturation/Enabled'
 * '<S44>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk/Disabled'
 * '<S45>'  : 'ControlePosicao/ControladorAngular/Sum/Sum_PID'
 * '<S46>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk/Disabled'
 * '<S47>'  : 'ControlePosicao/ControladorAngular/Tracking Mode/Disabled'
 * '<S48>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum/Passthrough'
 * '<S49>'  : 'ControlePosicao/ControladorAngular/postSat Signal/Forward_Path'
 * '<S50>'  : 'ControlePosicao/ControladorAngular/preSat Signal/Forward_Path'
 * '<S51>'  : 'ControlePosicao/ControladorX/Anti-windup'
 * '<S52>'  : 'ControlePosicao/ControladorX/D Gain'
 * '<S53>'  : 'ControlePosicao/ControladorX/Filter'
 * '<S54>'  : 'ControlePosicao/ControladorX/Filter ICs'
 * '<S55>'  : 'ControlePosicao/ControladorX/I Gain'
 * '<S56>'  : 'ControlePosicao/ControladorX/Ideal P Gain'
 * '<S57>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk'
 * '<S58>'  : 'ControlePosicao/ControladorX/Integrator'
 * '<S59>'  : 'ControlePosicao/ControladorX/Integrator ICs'
 * '<S60>'  : 'ControlePosicao/ControladorX/N Copy'
 * '<S61>'  : 'ControlePosicao/ControladorX/N Gain'
 * '<S62>'  : 'ControlePosicao/ControladorX/P Copy'
 * '<S63>'  : 'ControlePosicao/ControladorX/Parallel P Gain'
 * '<S64>'  : 'ControlePosicao/ControladorX/Reset Signal'
 * '<S65>'  : 'ControlePosicao/ControladorX/Saturation'
 * '<S66>'  : 'ControlePosicao/ControladorX/Saturation Fdbk'
 * '<S67>'  : 'ControlePosicao/ControladorX/Sum'
 * '<S68>'  : 'ControlePosicao/ControladorX/Sum Fdbk'
 * '<S69>'  : 'ControlePosicao/ControladorX/Tracking Mode'
 * '<S70>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum'
 * '<S71>'  : 'ControlePosicao/ControladorX/postSat Signal'
 * '<S72>'  : 'ControlePosicao/ControladorX/preSat Signal'
 * '<S73>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel'
 * '<S74>'  : 'ControlePosicao/ControladorX/D Gain/Internal Parameters'
 * '<S75>'  : 'ControlePosicao/ControladorX/Filter/Disc. Forward Euler Filter'
 * '<S76>'  : 'ControlePosicao/ControladorX/Filter ICs/Internal IC - Filter'
 * '<S77>'  : 'ControlePosicao/ControladorX/I Gain/Internal Parameters'
 * '<S78>'  : 'ControlePosicao/ControladorX/Ideal P Gain/Passthrough'
 * '<S79>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk/Disabled'
 * '<S80>'  : 'ControlePosicao/ControladorX/Integrator/Discrete'
 * '<S81>'  : 'ControlePosicao/ControladorX/Integrator ICs/Internal IC'
 * '<S82>'  : 'ControlePosicao/ControladorX/N Copy/Disabled'
 * '<S83>'  : 'ControlePosicao/ControladorX/N Gain/Internal Parameters'
 * '<S84>'  : 'ControlePosicao/ControladorX/P Copy/Disabled'
 * '<S85>'  : 'ControlePosicao/ControladorX/Parallel P Gain/Internal Parameters'
 * '<S86>'  : 'ControlePosicao/ControladorX/Reset Signal/External Reset'
 * '<S87>'  : 'ControlePosicao/ControladorX/Saturation/Enabled'
 * '<S88>'  : 'ControlePosicao/ControladorX/Saturation Fdbk/Disabled'
 * '<S89>'  : 'ControlePosicao/ControladorX/Sum/Sum_PID'
 * '<S90>'  : 'ControlePosicao/ControladorX/Sum Fdbk/Disabled'
 * '<S91>'  : 'ControlePosicao/ControladorX/Tracking Mode/Disabled'
 * '<S92>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum/Passthrough'
 * '<S93>'  : 'ControlePosicao/ControladorX/postSat Signal/Forward_Path'
 * '<S94>'  : 'ControlePosicao/ControladorX/preSat Signal/Forward_Path'
 * '<S95>'  : 'ControlePosicao/ControladorY/Anti-windup'
 * '<S96>'  : 'ControlePosicao/ControladorY/D Gain'
 * '<S97>'  : 'ControlePosicao/ControladorY/Filter'
 * '<S98>'  : 'ControlePosicao/ControladorY/Filter ICs'
 * '<S99>'  : 'ControlePosicao/ControladorY/I Gain'
 * '<S100>' : 'ControlePosicao/ControladorY/Ideal P Gain'
 * '<S101>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk'
 * '<S102>' : 'ControlePosicao/ControladorY/Integrator'
 * '<S103>' : 'ControlePosicao/ControladorY/Integrator ICs'
 * '<S104>' : 'ControlePosicao/ControladorY/N Copy'
 * '<S105>' : 'ControlePosicao/ControladorY/N Gain'
 * '<S106>' : 'ControlePosicao/ControladorY/P Copy'
 * '<S107>' : 'ControlePosicao/ControladorY/Parallel P Gain'
 * '<S108>' : 'ControlePosicao/ControladorY/Reset Signal'
 * '<S109>' : 'ControlePosicao/ControladorY/Saturation'
 * '<S110>' : 'ControlePosicao/ControladorY/Saturation Fdbk'
 * '<S111>' : 'ControlePosicao/ControladorY/Sum'
 * '<S112>' : 'ControlePosicao/ControladorY/Sum Fdbk'
 * '<S113>' : 'ControlePosicao/ControladorY/Tracking Mode'
 * '<S114>' : 'ControlePosicao/ControladorY/Tracking Mode Sum'
 * '<S115>' : 'ControlePosicao/ControladorY/postSat Signal'
 * '<S116>' : 'ControlePosicao/ControladorY/preSat Signal'
 * '<S117>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel'
 * '<S118>' : 'ControlePosicao/ControladorY/D Gain/Internal Parameters'
 * '<S119>' : 'ControlePosicao/ControladorY/Filter/Disc. Forward Euler Filter'
 * '<S120>' : 'ControlePosicao/ControladorY/Filter ICs/Internal IC - Filter'
 * '<S121>' : 'ControlePosicao/ControladorY/I Gain/Internal Parameters'
 * '<S122>' : 'ControlePosicao/ControladorY/Ideal P Gain/Passthrough'
 * '<S123>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk/Disabled'
 * '<S124>' : 'ControlePosicao/ControladorY/Integrator/Discrete'
 * '<S125>' : 'ControlePosicao/ControladorY/Integrator ICs/Internal IC'
 * '<S126>' : 'ControlePosicao/ControladorY/N Copy/Disabled'
 * '<S127>' : 'ControlePosicao/ControladorY/N Gain/Internal Parameters'
 * '<S128>' : 'ControlePosicao/ControladorY/P Copy/Disabled'
 * '<S129>' : 'ControlePosicao/ControladorY/Parallel P Gain/Internal Parameters'
 * '<S130>' : 'ControlePosicao/ControladorY/Reset Signal/External Reset'
 * '<S131>' : 'ControlePosicao/ControladorY/Saturation/Enabled'
 * '<S132>' : 'ControlePosicao/ControladorY/Saturation Fdbk/Disabled'
 * '<S133>' : 'ControlePosicao/ControladorY/Sum/Sum_PID'
 * '<S134>' : 'ControlePosicao/ControladorY/Sum Fdbk/Disabled'
 * '<S135>' : 'ControlePosicao/ControladorY/Tracking Mode/Disabled'
 * '<S136>' : 'ControlePosicao/ControladorY/Tracking Mode Sum/Passthrough'
 * '<S137>' : 'ControlePosicao/ControladorY/postSat Signal/Forward_Path'
 * '<S138>' : 'ControlePosicao/ControladorY/preSat Signal/Forward_Path'
 * '<S139>' : 'ControlePosicao/PID_Reset/If Action Subsystem'
 * '<S140>' : 'ControlePosicao/PID_Reset/If Action Subsystem1'
 * '<S141>' : 'ControlePosicao/PID_Reset1/If Action Subsystem'
 * '<S142>' : 'ControlePosicao/PID_Reset1/If Action Subsystem1'
 * '<S143>' : 'ControlePosicao/PID_Reset2/If Action Subsystem'
 * '<S144>' : 'ControlePosicao/PID_Reset2/If Action Subsystem1'
 */
#endif                                 /* RTW_HEADER_ControlePosicao_h_ */
