%% Gera e analisa modelos de todas as pastas de teste
clear all; close all; clc;
%% Adiciona as funções auxiliares
addpath('importFunctions/');
addpath('evaluationFunctions/');
addpath('plotFunctions/');
addpath('modelGenFunctions/');
addpath('auxFunctions/');
%% Definição da pasta de teste
% Indica se o teste foi feito em campo real ou no grsim
% Robo = "RoboReal";
%Robo = "gRSim";

% Indica o endereço base da pasta de testes
EnderecoBase = "/home/leonardo/Repositorios/ic_identificacao/Dados/RelatorioParcial/";

%% Nome das pastas de testes feitos para cada tipo de sinal
testesPRBS = getTestFolderNames(EnderecoBase + "PRBS"); 
testesGBN  = getTestFolderNames(EnderecoBase + "GBN");

%% Ordem dos modelos
% Para o modelo ARX
nOrdemInicialARX = 2;
nOrdemFinalARX = 5;

% Para o modelo ARMAX
nOrdemInicialARMAX = 2;
nOrdemFinalARMAX = 5;

%% Geração dos modelos
% Armazena os modelos gerados em uma matriz
% do formato {Experimento,Ordem do modelo}

[arxModelPRBS, armaxModelPRBS, ...
    dadosRoboPRBS, nDadosPRBS, parametrosEntradaPRBS] = ...
                            createArxArmaxModels(testesPRBS, 'PRBS' ,...
                            nOrdemInicialARX  , nOrdemFinalARX      ,...
                            nOrdemInicialARMAX, nOrdemFinalARMAX    ,...
                            EnderecoBase);

[arxModelGBN, armaxModelGBN, ...
    dadosRoboGBN, nDadosGBN, parametrosEntradaGBN] = ...
                            createArxArmaxModels(testesGBN, 'GBN',...
                            nOrdemInicialARX  , nOrdemFinalARX   ,...
                            nOrdemInicialARMAX, nOrdemFinalARMAX ,...
                            EnderecoBase);

%% Analisa os modelos gerados para determinar os melhores
melhoresARXPRBS   = zeros(length(testesPRBS), ...
    nOrdemFinalARX-nOrdemInicialARX    );
melhoresARMAXPRBS = zeros(length(testesPRBS), ...
    nOrdemFinalARMAX-nOrdemInicialARMAX);
melhoresARXGBN    = zeros(length(testesGBN) , ...
    nOrdemFinalARX-nOrdemInicialARX    );
melhoresARMAXGBN  = zeros(length(testesGBN) , ...
    nOrdemFinalARMAX-nOrdemInicialARMAX);
nPredAnalise = 10;
menorFit = 80; 
% PRBS
for n=1 :1: length(testesPRBS)
    for ordem = nOrdemInicialARX :1: nOrdemFinalARX
        
        [~, fit, ~] = compare(dadosRoboPRBS{n}(findMiddle(nDadosPRBS{n}):nDadosPRBS{n}), ...
                     arxModelPRBS{n,ordem - nOrdemInicialARX+1},...
                     nPredAnalise);
                 
        if mean(fit) >= menorFit
            melhoresARXPRBS(n, ordem - nOrdemInicialARX + 1) = 1;
        end
    end
    
    [~, fit, ~] = compare(dadosRoboPRBS{n}(findMiddle(nDadosPRBS{n}):nDadosPRBS{n}), ...
                     armaxModelPRBS{n,ordem - nOrdemInicialARMAX+1},...
                     nPredAnalise);
                 
    for ordem = nOrdemInicialARMAX :1: nOrdemFinalARMAX
        if mean(fit) >= menorFit
            melhoresARMAXPRBS(n, ordem - nOrdemInicialARMAX + 1) = 1;
        end
    end
end

% GBN
for n=1 :1: length(testesGBN)
    for ordem = nOrdemInicialARX :1: nOrdemFinalARX
        
        [~, fit, ~] = compare(dadosRoboGBN{n}(findMiddle(nDadosGBN{n}):nDadosGBN{n}), ...
                     arxModelGBN{n,ordem - nOrdemInicialARX+1},...
                     nPredAnalise);
                 
        if mean(fit) >= menorFit
            melhoresARXGBN(n, ordem - nOrdemInicialARX + 1) = 1;
        end
    end
    
    [~, fit, ~] = compare(dadosRoboGBN{n}(findMiddle(nDadosGBN{n}):nDadosGBN{n}), ...
                     armaxModelGBN{n,ordem - nOrdemInicialARX+1},...
                     nPredAnalise);
                 
    for ordem = nOrdemInicialARMAX :1: nOrdemFinalARMAX
        if mean(fit) >= menorFit
            melhoresARMAXGBN(n, ordem - nOrdemInicialARMAX + 1) = 1;
        end
    end
end

%% Gera gráficos dos melhores modelos
nPredicoes = 10; % Número de incremento das predições
nMultMax = 5; % Multiplicador do número máximo de predições

melhoresARXPRBS   = returnIndex(find(melhoresARXPRBS)    ,...
                                int16(length(testesPRBS)));
melhoresARMAXPRBS = returnIndex(find(melhoresARMAXPRBS)  ,...
                                int16(length(testesPRBS)));
melhoresARXGBN    = returnIndex(find(melhoresARXGBN)     ,... 
                                int16(length(testesGBN)));
melhoresARMAXGBN  = returnIndex(find(melhoresARMAXGBN)   ,... 
                                int16(length(testesGBN)));

EnderecoTeste = EnderecoBase + "PRBS/ARX_PRBS";
createGraph(arxModelPRBS , melhoresARXPRBS, nPredicoes   , nMultMax, ...
            dadosRoboPRBS, nDadosPRBS     , EnderecoTeste, ....
            nOrdemInicialARX, 'ARX PRBS', parametrosEntradaPRBS);
        
EnderecoTeste = EnderecoBase + "PRBS/ARMAX_PRBS";
createGraph(armaxModelPRBS, melhoresARMAXPRBS, nPredicoes   , nMultMax, ...
            dadosRoboPRBS , nDadosPRBS       , EnderecoTeste, ...
            nOrdemInicialARMAX, 'ARMAX PRBS', parametrosEntradaPRBS);
        
EnderecoTeste = EnderecoBase + "GBN/ARX_GBN";
createGraph(arxModelGBN , melhoresARXGBN, nPredicoes   , nMultMax, ...
            dadosRoboGBN, nDadosGBN     , EnderecoTeste, ...
            nOrdemInicialARX, 'ARX GBN', parametrosEntradaGBN);
        
EnderecoTeste = EnderecoBase + "GBN/ARMAX_GBN";
createGraph(armaxModelGBN, melhoresARMAXGBN, nPredicoes   , nMultMax, ...
            dadosRoboGBN , nDadosGBN       , EnderecoTeste, ...
            nOrdemInicialARMAX, 'ARMAX GBN', parametrosEntradaGBN);

%% Gera gráficos de performance dos modelos
createFitBarGraph(arxModelPRBS, armaxModelPRBS, arxModelGBN, ...
    armaxModelGBN, EnderecoBase);