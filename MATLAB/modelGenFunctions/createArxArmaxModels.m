function [arxModels,armaxModels,robotData,nData,inputParam] = ...
    createArxArmaxModels(test, testName, initOrderArx, maxOrderArx, ...
    initOrderArmax, maxOrderArmax, dataBaseAddress)
%createArxArmaxModels Cria os modelos ARX e ARMAX a partir dos dados fornecidos
%
% Esta função é utilizada para identificar uma grande quantidade de modelos
% de uma vez só.
%
% [ARXModels, ARMAXModels, ROBOTS, N, INPUTP] = createArxArmaxModels(test, 
%                                      testName, initOrderArx, maxOrderArx, 
%                                      initOrderArmax, maxOrderArmax, 
%                                      dataBaseAddress)
%
% ARXModels Modelos ARX estimados a partir dos dados fornecidos.
%
% ARMAXModels Modelos ARMAX estimados a partir dos dados fornecidos.
%
% ROBOTS iddata contendo os dados dos experimentos.
% 
% N Comprimento dos dados dos experimentos.
%
% INPUTP Parâmetros do sinal de teste utilizado.
% 
% test São os nomes das pastas com os dados que serão utilizados na 
% identificação.Este parâmetro é fornecido pela função auxiliar 
% getTestFolderNames.
%
% testName Nome do teste, e.g. 'PRBS' ou 'GBN'.
%
% initOrderArx Valor inicial das ordens do modelo ARX.
%
% maxOrderArx Valor final das ordens do modelo ARX.
%
% initOrderArmax Valor inicial das ordens do modelo ARMAX.
%
% maxOrderArmax Valor final das ordens do modelo ARMAA.
%
% dataBaseAddress Endereço base dos testes, neste endereço devem estar
% contidas as pastas com os testes definidos pelo parâmetro 'test'.

arxModels   = cell(length(test), maxOrderArx   - initOrderArx  +1); 
armaxModels = cell(length(test), maxOrderArmax - initOrderArmax+1);

robotData  = cell(length(test),1);
nData      = cell(length(test),1);
inputParam = cell(length(test),1);

EnderecoTeste = dataBaseAddress + testName +"/"; %pasta + PRBS/GBN

NK = [1 1 1;...
      1 1 1;...
      1 1 1];
  
OptArx = arxOptions;
OptArx.Focus = 'prediction';
OptArmax = armaxOptions;
OptArmax.Focus = 'prediction';


for n=1 :1: length(test)
    [robotData{n},nData{n}]= getIDData(EnderecoTeste + test(n) + "/"); %pasta + PRBS/GBN + PRBS_R0_n/
    inputParam{n} = getInputParam(EnderecoTeste + test(n) + "/");
    for ordem = initOrderArx :1: maxOrderArx
        NA = [ordem ordem ordem;...
              ordem ordem ordem;...
              ordem ordem ordem];
          
        NB = [ordem ordem ordem;...
              ordem ordem ordem;...
              ordem ordem ordem];
                  
        arxModels{n,ordem - initOrderArx+1} = arx(robotData{n}(1:nData{n}/2),[NA, NB, NK], OptArx);
        arxModels{n,ordem - initOrderArx+1}.Name = "ARX_"+testName+"_" + n;
        arxModels{n,ordem - initOrderArx+1}.Notes = inputParam{n}{3,1};
    end
    
    for ordem = initOrderArmax :1: maxOrderArmax
        NA = [ordem ordem ordem;...
              ordem ordem ordem;...
              ordem ordem ordem];
          
        NB = [ordem ordem ordem;...
              ordem ordem ordem;...
              ordem ordem ordem];
          
        NC = [ordem ;...
              ordem ;...
              ordem ];
                  
        armaxModels{n,ordem - initOrderArmax+1} = armax(robotData{n}(1:nData{n}/2),[NA NB NC NK], OptArmax);
        armaxModels{n,ordem - initOrderArmax+1}.Name = "ARMAX_"+testName+"_" + n;
        armaxModels{n,ordem - initOrderArx+1}.Notes = inputParam{n}{3,1};
    end
end


end

