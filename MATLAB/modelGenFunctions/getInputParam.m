function DataParameters1 = getInputParam(path)
% getInputParam Retorna os parâmetros do sinal de teste utilizado
%  DATAPARAMETERS1 = getInputParam(PATH)
%  PATH Caminho onde está o arquivo DataParameters.txt 

%% Input handling

% If dataLines is not specified, define defaults
dataLines = [2, Inf];

%% Setup the Import Options
opts = delimitedTextImportOptions("NumVariables", 1);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = "";

% Specify column names and types
opts.VariableNames = "SignalTypePRBS";
opts.VariableTypes = "string";
opts = setvaropts(opts, 1, "WhitespaceRule", "preserve");
opts = setvaropts(opts, 1, "EmptyFieldRule", "auto");
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.ConsecutiveDelimitersRule = "join";

% Import the data
DataParameters1 = readtable(path + "DataParameters.txt", opts);

%% Convert to output type
DataParameters1 = table2cell(DataParameters1);
numIdx = cellfun(@(x) ~isnan(str2double(x)), DataParameters1);
DataParameters1(numIdx) = cellfun(@(x) {str2double(x)}, DataParameters1(numIdx));
end