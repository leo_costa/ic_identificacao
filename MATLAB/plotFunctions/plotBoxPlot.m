function plotBoxPlot(fit, modelName)
figure;
boxplot(fit, 'Colors','mkg'); hold on; grid on;
% 
title(strcat('Box Plot para o modelo ', modelName), ...
    'Interpreter', 'Latex', 'FontSize', 18);

xlabel('Limite de predição', 'FontSize', 18);
ylabel('nRMSE [%]');
yticks(85 :1: 100);
xticks(1 :1: 12);
xticklabels({' ','1 Passo',' ',' ','5 Passos',' ',...
             ' ','10 Passos', ' ',' ','20 Passos', ' '});
a = get(gca,'XTickLabel');  
set(gca,'XTickLabel',a,'fontsize',14)

plot(NaN,1,'m',NaN,1,'k',NaN,1,'g',NaN,1,'r+'); %// dummy plot for legend

legend('X','Y','$\theta$','Fora do intervalo', 'Interpreter', 'Latex',...
       'Location','southwest', 'FontSize', 18);
end

