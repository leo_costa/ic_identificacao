function createBar(modelFit, xSize,graphTitle)
%createBar Cria um gráfico de barras para os dados fornecidos

bar(modelFit);
grid on; box on;
title(graphTitle);
xlabel('Modelos');
ylabel('NRMSE [%]');
legend('X','Y','$\theta$','Interpreter','LaTex');
ylim([85 100]);
xticks(1 :1: xSize);
yticks(85 :1: 100);

end

