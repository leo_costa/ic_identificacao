function plotDFT(t, vel, velMeas, filename, unit)
    fs = 1 / (t(end)/length(t));
    maxF = 5; % fs/2

    % FFT
    fftMeas = fft(velMeas);
    fftVel = fft(vel);

    % Frequência em bins
    fBin = 0:1:length(velMeas)-1;

    % Plot
    fh = figure();

    % Modulo
    subplot(211);

    plot((fs/length(t)).*fBin, abs(fftVel)/(length(fftVel)/2));
    hold on; grid on;
    plot((fs/length(t)).*fBin, abs(fftMeas)/(length(fftMeas)/2));
    xlim([0 maxF]);

    xlabel('$f(Hz)$','Interpreter','LaTex','FontSize', 14);
    ylabel('Mag','Interpreter','LaTex','FontSize', 14);
    legend("Input $[" + unit + "/s]$", "Output $[" + unit + "]$", 'Interpreter', 'LaTex');
    ax = gca;
    ax.FontSize = 14;

    % Fase
    subplot(212);
    plot((fs/length(t)).*fBin, angle(fftVel));
    grid on; hold on;
    plot((fs/length(t)).*fBin, angle(fftMeas));
    xlim([0 maxF]);

    xlabel('$f(Hz)$','Interpreter','LaTex','FontSize', 14);
    ylabel('Phase','Interpreter','LaTex','FontSize', 14);
    legend('Input $[rad]$', 'Output $[rad]$', 'Interpreter', 'LaTex');
    ax = gca;
    ax.FontSize = 14;

    % Save
%     fh.WindowState = 'maximized';
    set(gcf, 'Position',  [100, 100, 900, 400]);
    pause(1);
    exportgraphics(gcf, filename);
end
