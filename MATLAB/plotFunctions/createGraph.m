function createGraph(models, bestModels, nPred, nMultMax, robotData, ...
    nData, baseAddress, initialOrder, modelName, inputParam)
%createGraph Cria os graficos para o modelo fornecido e teste fornecido

for p=1 :nPred: nPred*nMultMax
    
    grafico = figure;
    grafico.WindowState = 'maximized';
    pause(1);
        
    for n=1 :1: length(bestModels)
        exp = int16(bestModels(2,n));

        [auxComp, fit, ~] = compare(robotData{exp}(int16(findMiddle(nData{exp})):int16(nData{exp})), ...
                            models{exp, bestModels(1,n)},...
                            p);
                        
        saidaReal = get(robotData{exp}, 'OutputData');% X, Y, Angulo
        saidaReal = saidaReal(findMiddle(nData{exp}):nData{exp}, :);
        saidaModelo = get(auxComp, 'OutputData');% X, Y, Angulo
        tempo = findMiddle(nData{exp}) :1: nData{exp};
        tempo = tempo' .*0.032;
                
        if n==1
            subplot(3,1,1);
            title(modelName + " - " + p + " Step Prediction");
            hold on; grid on;
            plot(tempo, saidaReal(:,1));
            legendaX{1} = "Dados de validação";
            legend(legendaX);

            subplot(3,1,2);
            hold on; grid on;
            plot(tempo, saidaReal(:,2));
            legendaY{1} = "Dados de validação";
            legend(legendaY);

            subplot(3,1,3);
            hold on; grid on;
            plot(tempo, saidaReal(:,3));
            legendaA{1} = "Dados de validação";
            legend(legendaA);
        end
        
        subplot(3,1,1);
        plot(tempo,  saidaModelo(:,1));
        ylabel('Posição X [m]');
        legendaX{n+1} = "Ordem:" + (bestModels(1,n)+initialOrder-1) + ...
            " Exp:" + exp + " Fit:" + fit(1) + "% " + inputParam{exp}{3,1};
        legend(legendaX);
        
        subplot(3,1,2);
        plot(tempo,  saidaModelo(:,2));
        ylabel('Posição Y [m]');
        legendaY{n+1} = "Ordem:" + (bestModels(1,n)+initialOrder-1) + ...
            " Exp:" + exp + " Fit:" + fit(2) + "% " + inputParam{exp}{3,1};
        legend(legendaY);
        
        subplot(3,1,3);
        plot(tempo,  saidaModelo(:,3));
        ylabel('Ângulo [rad]');
        legendaA{n+1} = "Ordem:" + (bestModels(1,n)+initialOrder-1) + ...
            " Exp:" + exp + " Fit:" + fit(3) + "% " + inputParam{exp}{3,1};
        legend(legendaA);

    end
    
    saveas(gcf,baseAddress+"_"+p+".png");
end

end

