function plotSineData(t, vel, velMeas, figTitle, unit, axis)
    data_sz = round(length(t)/1);
    plot(t(1:data_sz), vel(1:data_sz), 'r--', ...
         t(1:data_sz), velMeas(1:data_sz), 'b');

    grid on; box on;

    title(figTitle,'FontSize', 16, 'Interpreter', 'Latex');
    xlabel('Time [s]','FontSize', 16, 'Interpreter', 'Latex');

%     ylabel("Velocity $[\frac{" + unit + "}{s}]; Position [" + unit + "]$",...
%            'FontSize', 16, 'Interpreter', 'Latex');

    legend("$\dot " + axis + " [" + unit + "/s] $", "$"+ axis + " [" + unit +"]$",...
           'FontSize', 14, 'Interpreter', 'Latex');
    ax = gca;
    ax.FontSize = 14;
end
