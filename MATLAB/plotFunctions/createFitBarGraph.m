function createFitBarGraph(arxPRBS, armaxPRBS, arxGBN, armaxGBN, baseAddress)
%createFitBarGraph Cria um gráfico de barras dos Fits de cada modelo

%% Pega o tamanho dos modelos
[prbsSize, arxOrderSize   ] = size(arxPRBS);
[   ~    , armaxOrderSize ] = size(armaxPRBS);


[gbnSize , ~ ] = size(arxGBN);
[   ~    , ~ ] = size(armaxGBN);

%% Gera os gráficos

% PRBS
fitModelosPRBS = arxPRBS{1, 1}.Report.Fit.FitPercent';
fitModelosGBN  =  arxGBN{1, 1}.Report.Fit.FitPercent';

boxFig = figure;
boxFig.WindowState = 'maximized';
pause(1);
for test = 1 :1: prbsSize
    for order = 1 :1: arxOrderSize
        if test+order ~= 2
            fitModelosPRBS  = [fitModelosPRBS;arxPRBS{test, order}.Report.Fit.FitPercent'];
        end
    end
end

createBar(fitModelosPRBS, prbsSize*arxOrderSize,...
    'Índice NRMSE para cada modelo do tipo ARX gerado com PRBS');

saveas(gcf,baseAddress+"ARXPRBS_bar.png");

auxFit = fitModelosPRBS;
fitModelosPRBS = armaxPRBS{1, 1}.Report.Fit.FitPercent';

boxFig = figure;
boxFig.WindowState = 'maximized';
pause(1);
for test = 1 :1: prbsSize
    for order = 1 :1: armaxOrderSize
        if test+order ~= 2
            fitModelosPRBS = [fitModelosPRBS; armaxPRBS{test, order}.Report.Fit.FitPercent'];
        end
    end
end

createBar(fitModelosPRBS, prbsSize*armaxOrderSize,...
    'Índice NRMSE para cada modelo do tipo ARMAX gerado com PRBS');
saveas(gcf,baseAddress+"ARMAXPRBS_bar.png");

auxFit = [auxFit , fitModelosPRBS];

% GBN
boxFig = figure;
boxFig.WindowState = 'maximized';
pause(1);
for test = 1 :1: gbnSize
    for order = 1 :1: arxOrderSize
        if test+order ~= 2
            fitModelosGBN = [fitModelosGBN; arxGBN{test, order}.Report.Fit.FitPercent'];
        end
    end
end

createBar(fitModelosGBN, gbnSize*arxOrderSize,...
    'Índice NRMSE para cada modelo do tipo ARX gerado com GBN');
saveas(gcf,baseAddress+"ARXGBN_bar.png");

auxFit = [auxFit, fitModelosGBN];

fitModelosGBN  =  armaxGBN{1, 1}.Report.Fit.FitPercent';

boxFig = figure;
boxFig.WindowState = 'maximized';
pause(1);
for test = 1 :1: gbnSize
    for order = 1 :1: armaxOrderSize
        if test+order ~= 2
            fitModelosGBN = [fitModelosGBN; armaxGBN{test, order}.Report.Fit.FitPercent'];
        end
    end
end

createBar(fitModelosGBN, gbnSize*armaxOrderSize,...
    'Índice NRMSE para cada modelo do tipo ARMAX gerado com GBN');
saveas(gcf,baseAddress+"ARMAXGBN_bar.png");

%% Gera boxplot dos fits

figure;
auxFit = [auxFit,fitModelosGBN];
boxplot(auxFit, 'Colors','mkg'); hold on; grid on;
title('Box Plot de cada tipo de modelo para o eixos $X,Y$ e $\theta$', 'Interpreter', 'Latex', 'FontSize', 12);
xlabel('Modelo $(X,Y,\theta)$', 'Interpreter', 'Latex', 'FontSize', 12);
ylabel('NRMSE [%]');
yticks(85 :1: 100);
xticks(1 :1: 12);
xticklabels({' ','ARX PRBS',' ',' ','ARMAX PRBS',' ',' ','ARX GBN',' ',' ','ARMAX GBN',' '});
plot(NaN,1,'m',NaN,1,'k',NaN,1,'g',NaN,1,'r+'); %// dummy plot for legend
legend('X','Y','$\theta$','Fora do intervalo', 'Interpreter', 'Latex','Location','southeast', 'FontSize', 10);
saveas(gcf,baseAddress+"BoxPlotFit.png");
end

