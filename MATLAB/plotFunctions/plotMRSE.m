function plotMRSE(mrse,modelName)
figure;
bar(mrse);
legend('Experimento 1', 'Experimento 2', 'Experimento 3',...
    'Location', 'northwest');
xticks(1 :1: 4);
xticklabels({'1 Passo', '5 Passos', '10 Passos', '20 Passos'});
xlabel('Limite de predição');
ylabel('MRSE [%]');
title(strcat('MRSE para o modelo ', modelName), 'Interpreter', 'Latex');
grid on;
end

