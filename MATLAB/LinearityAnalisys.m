%% Faz a analise de linearidade do robô
%% Incício
clear; close all;

%% Adiciona as funções auxiliares
addpath('importFunctions/');
addpath('plotFunctions/');

%% Lê os dados do teste de linearidade
Endereco = uigetdir(pwd,"Select the folder containing the experiment data");
[TempoX , VelX, VelXMed, PosXX,   ~  ] = importfileXY(Endereco+"/Data_X.csv", [2, Inf]);
[TempoY , VelY, VelYMed, ~    , PosYY] = importfileXY(Endereco+"/Data_Y.csv", [2, Inf]);
[TempoW , VelW, VelWMed, Angle]        =  importfileW(Endereco+"/Data_W.csv", [2, Inf]);

%% Ajusta a escala da Velocity enviada
% VelX = VelX * 2.5/100;
% VelY = VelY * 2.5/100;
% VelW = VelW * 6.5/100;

PosXX = detrend(PosXX);
PosYY = detrend(PosYY);
Angle = detrend(Angle);

%% Gera os gráficos
fh = figure();

subplot(3,1,1);
plotSineData(TempoX, VelX, PosXX, '$X_f$ Axis', "m", "x");

subplot(3,1,2);
plotSineData(TempoY, VelY, PosYY, '$Y_f$ Axis', "m", "y");

subplot(3,1,3);
plotSineData(TempoW, VelW, Angle, '$\theta$ Axis', "rad", "\theta");

% fh.WindowState = 'maximized';
set(gcf, 'Position',  [100, 100, 900, 700]);
pause(1);
exportgraphics(gcf, "sine_data.pdf");

%% Gera os gráficos da Transformada de Fourier do sinal
plotDFT(TempoX, VelX, PosXX, "dft_x.pdf", "m");
plotDFT(TempoY, VelY, PosYY, "dft_y.pdf", "m");
plotDFT(TempoW, VelW, Angle, "dft_w.pdf", "rad");
