/*
 * ControlePosicao_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.5
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Fri Oct 21 15:11:45 2022
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_types_h_
#define RTW_HEADER_ControlePosicao_types_h_

/* Model Code Variants */

/* Forward declaration for rtModel */
typedef struct tag_RTM_ControlePosicao_T RT_MODEL_ControlePosicao_T;

#endif                                 /* RTW_HEADER_ControlePosicao_types_h_ */
