/*
 * ControlePosicao.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.5
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Fri Oct 21 15:11:45 2022
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#include "ControlePosicao.h"
#include "ControlePosicao_private.h"

/*
 * Output and update for action system:
 *    '<S4>/If Action Subsystem'
 *    '<S4>/If Action Subsystem1'
 *    '<S5>/If Action Subsystem'
 *    '<S5>/If Action Subsystem1'
 *    '<S6>/If Action Subsystem'
 *    '<S6>/If Action Subsystem1'
 */
void ControlePosicaoModelClass::ControlePosic_IfActionSubsystem(real_T rtu_In1,
  real_T *rty_Out1)
{
  /* Inport: '<S157>/In1' */
  *rty_Out1 = rtu_In1;
}

/* Model step function */
void ControlePosicaoModelClass::step()
{
  real_T denAccum;
  real_T denAccum_0;
  real_T denAccum_1;
  real_T rtb_FilterCoefficient;
  real_T rtb_FilterCoefficient_f;
  real_T rtb_FilterCoefficient_l;
  real_T rtb_IntegralGain;
  real_T rtb_Integrator_d;
  real_T rtb_Integrator_p;
  real_T rtb_Merge;
  real_T rtb_Merge_h;
  real_T rtb_Merge_j;
  real_T rtb_SignPreIntegrator;
  boolean_T rtb_Equal1;
  boolean_T rtb_Equal1_lz;
  boolean_T rtb_NotEqual;
  boolean_T rtb_NotEqual_o;
  boolean_T rtb_NotEqual_p2;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Inport: '<Root>/PosX'
   *  Inport: '<Root>/ReAlimX'
   */
  rtb_Integrator_p = ControlePosicao_U.PosX - ControlePosicao_U.ReAlimX;

  /* If: '<S4>/If' incorporates:
   *  Constant: '<S4>/Constant'
   *  Constant: '<S4>/Constant1'
   */
  if (rtb_Integrator_p <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem' incorporates:
     *  ActionPort: '<S157>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S158>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem1' */
  }

  /* End of If: '<S4>/If' */

  /* DiscreteIntegrator: '<S90>/Integrator' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState <= 0))
  {
    ControlePosicao_DW.Integrator_DSTATE = 0.0;
  }

  /* DiscreteIntegrator: '<S85>/Filter' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Filter_PrevResetState <= 0)) {
    ControlePosicao_DW.Filter_DSTATE = 0.0;
  }

  /* Gain: '<S93>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S85>/Filter'
   *  Gain: '<S84>/Derivative Gain'
   *  Sum: '<S85>/SumD'
   */
  rtb_FilterCoefficient = (-0.000192560345238337 * rtb_Integrator_p -
    ControlePosicao_DW.Filter_DSTATE) * 95.3886868834244;

  /* Sum: '<S99>/Sum' incorporates:
   *  DiscreteIntegrator: '<S90>/Integrator'
   *  Gain: '<S95>/Proportional Gain'
   */
  rtb_Integrator_d = (2.62007979455078 * rtb_Integrator_p +
                      ControlePosicao_DW.Integrator_DSTATE) +
    rtb_FilterCoefficient;

  /* Saturate: '<S97>/Saturation' incorporates:
   *  DeadZone: '<S83>/DeadZone'
   */
  if (rtb_Integrator_d > 3.0) {
    /* Saturate: '<S97>/Saturation' */
    ControlePosicao_Y.SinalControleX = 3.0;
    rtb_Integrator_d -= 3.0;
  } else {
    if (rtb_Integrator_d < -3.0) {
      /* Saturate: '<S97>/Saturation' */
      ControlePosicao_Y.SinalControleX = -3.0;
    } else {
      /* Saturate: '<S97>/Saturation' */
      ControlePosicao_Y.SinalControleX = rtb_Integrator_d;
    }

    if (rtb_Integrator_d >= -3.0) {
      rtb_Integrator_d = 0.0;
    } else {
      rtb_Integrator_d -= -3.0;
    }
  }

  /* End of Saturate: '<S97>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  denAccum = (ControlePosicao_Y.SinalControleX - -1.93124601468313 *
              ControlePosicao_DW.TFPosicaoX_states[0]) - 0.931246013659393 *
    ControlePosicao_DW.TFPosicaoX_states[1];

  /* Outport: '<Root>/PosRoboX' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoX'
   */
  ControlePosicao_Y.PosRoboX = 0.00135660948379848 * denAccum;

  /* Outport: '<Root>/ErroX' */
  ControlePosicao_Y.ErroX = rtb_Integrator_p;

  /* RelationalOperator: '<S81>/NotEqual' */
  rtb_NotEqual = (0.0 != rtb_Integrator_d);

  /* Signum: '<S81>/SignPreSat' */
  if (rtb_Integrator_d < 0.0) {
    rtb_Integrator_d = -1.0;
  } else {
    if (rtb_Integrator_d > 0.0) {
      rtb_Integrator_d = 1.0;
    }
  }

  /* End of Signum: '<S81>/SignPreSat' */

  /* Gain: '<S87>/Integral Gain' */
  rtb_Integrator_p *= 0.0672287632955098;

  /* Signum: '<S81>/SignPreIntegrator' */
  if (rtb_Integrator_p < 0.0) {
    rtb_IntegralGain = -1.0;
  } else if (rtb_Integrator_p > 0.0) {
    rtb_IntegralGain = 1.0;
  } else {
    rtb_IntegralGain = rtb_Integrator_p;
  }

  /* End of Signum: '<S81>/SignPreIntegrator' */

  /* RelationalOperator: '<S81>/Equal1' incorporates:
   *  DataTypeConversion: '<S81>/DataTypeConv1'
   *  DataTypeConversion: '<S81>/DataTypeConv2'
   */
  rtb_Equal1 = (static_cast<int8_T>(rtb_Integrator_d) == static_cast<int8_T>
                (rtb_IntegralGain));

  /* Sum: '<Root>/Sum3' incorporates:
   *  Inport: '<Root>/PosY'
   *  Inport: '<Root>/ReAlimY'
   */
  rtb_Integrator_d = ControlePosicao_U.PosY - ControlePosicao_U.ReAlimY;

  /* If: '<S5>/If' incorporates:
   *  Constant: '<S5>/Constant'
   *  Constant: '<S5>/Constant1'
   */
  if (rtb_Integrator_d <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem' incorporates:
     *  ActionPort: '<S159>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S160>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem1' */
  }

  /* End of If: '<S5>/If' */

  /* DiscreteIntegrator: '<S140>/Integrator' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState_p <=
       0)) {
    ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  }

  /* DiscreteIntegrator: '<S135>/Filter' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Filter_PrevResetState_o <= 0))
  {
    ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  }

  /* Gain: '<S143>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S135>/Filter'
   *  Gain: '<S134>/Derivative Gain'
   *  Sum: '<S135>/SumD'
   */
  rtb_FilterCoefficient_l = (-0.554531619218569 * rtb_Integrator_d -
    ControlePosicao_DW.Filter_DSTATE_b) * 6.4692563260662;

  /* Sum: '<S149>/Sum' incorporates:
   *  DiscreteIntegrator: '<S140>/Integrator'
   *  Gain: '<S145>/Proportional Gain'
   */
  rtb_IntegralGain = (3.77096562037958 * rtb_Integrator_d +
                      ControlePosicao_DW.Integrator_DSTATE_i) +
    rtb_FilterCoefficient_l;

  /* Saturate: '<S147>/Saturation' incorporates:
   *  DeadZone: '<S133>/DeadZone'
   */
  if (rtb_IntegralGain > 3.0) {
    /* Saturate: '<S147>/Saturation' */
    ControlePosicao_Y.SinalControleY = 3.0;
    rtb_IntegralGain -= 3.0;
  } else {
    if (rtb_IntegralGain < -3.0) {
      /* Saturate: '<S147>/Saturation' */
      ControlePosicao_Y.SinalControleY = -3.0;
    } else {
      /* Saturate: '<S147>/Saturation' */
      ControlePosicao_Y.SinalControleY = rtb_IntegralGain;
    }

    if (rtb_IntegralGain >= -3.0) {
      rtb_IntegralGain = 0.0;
    } else {
      rtb_IntegralGain -= -3.0;
    }
  }

  /* End of Saturate: '<S147>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  denAccum_0 = (ControlePosicao_Y.SinalControleY - -0.0210590086589133 *
                ControlePosicao_DW.TFPosicaoY_states[0]) - -0.978941020820896 *
    ControlePosicao_DW.TFPosicaoY_states[1];

  /* Outport: '<Root>/PosRoboY' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoY'
   */
  ControlePosicao_Y.PosRoboY = 0.0210090298064152 * denAccum_0;

  /* Outport: '<Root>/ErroY' */
  ControlePosicao_Y.ErroY = rtb_Integrator_d;

  /* RelationalOperator: '<S131>/NotEqual' */
  rtb_NotEqual_o = (0.0 != rtb_IntegralGain);

  /* Signum: '<S131>/SignPreSat' */
  if (rtb_IntegralGain < 0.0) {
    rtb_IntegralGain = -1.0;
  } else {
    if (rtb_IntegralGain > 0.0) {
      rtb_IntegralGain = 1.0;
    }
  }

  /* End of Signum: '<S131>/SignPreSat' */

  /* Gain: '<S137>/Integral Gain' */
  rtb_Integrator_d *= 0.0831491183285383;

  /* Signum: '<S131>/SignPreIntegrator' */
  if (rtb_Integrator_d < 0.0) {
    rtb_Merge_h = -1.0;
  } else if (rtb_Integrator_d > 0.0) {
    rtb_Merge_h = 1.0;
  } else {
    rtb_Merge_h = rtb_Integrator_d;
  }

  /* End of Signum: '<S131>/SignPreIntegrator' */

  /* RelationalOperator: '<S131>/Equal1' incorporates:
   *  DataTypeConversion: '<S131>/DataTypeConv1'
   *  DataTypeConversion: '<S131>/DataTypeConv2'
   */
  rtb_Equal1_lz = (static_cast<int8_T>(rtb_IntegralGain) == static_cast<int8_T>
                   (rtb_Merge_h));

  /* Sum: '<Root>/Sum1' incorporates:
   *  Inport: '<Root>/PosAngular'
   *  Inport: '<Root>/ReAlimAngular'
   */
  rtb_IntegralGain = ControlePosicao_U.PosAngular -
    ControlePosicao_U.ReAlimAngular;

  /* If: '<S6>/If' incorporates:
   *  Constant: '<S6>/Constant'
   *  Constant: '<S6>/Constant1'
   */
  if (rtb_IntegralGain < 0.005) {
    /* Outputs for IfAction SubSystem: '<S6>/If Action Subsystem' incorporates:
     *  ActionPort: '<S161>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge_h);

    /* End of Outputs for SubSystem: '<S6>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S6>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S162>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge_h);

    /* End of Outputs for SubSystem: '<S6>/If Action Subsystem1' */
  }

  /* End of If: '<S6>/If' */

  /* DiscreteIntegrator: '<S40>/Integrator' */
  if ((rtb_Merge_h > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState_m <=
       0)) {
    ControlePosicao_DW.Integrator_DSTATE_b = 0.0;
  }

  /* DiscreteIntegrator: '<S35>/Filter' */
  if ((rtb_Merge_h > 0.0) && (ControlePosicao_DW.Filter_PrevResetState_p <= 0))
  {
    ControlePosicao_DW.Filter_DSTATE_d = 0.0;
  }

  /* Gain: '<S43>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S35>/Filter'
   *  Gain: '<S34>/Derivative Gain'
   *  Sum: '<S35>/SumD'
   */
  rtb_FilterCoefficient_f = (0.459450972448674 * rtb_IntegralGain -
    ControlePosicao_DW.Filter_DSTATE_d) * 12.6040790927334;

  /* Sum: '<S49>/Sum' incorporates:
   *  DiscreteIntegrator: '<S40>/Integrator'
   *  Gain: '<S45>/Proportional Gain'
   */
  rtb_SignPreIntegrator = (1.30726619518589 * rtb_IntegralGain +
    ControlePosicao_DW.Integrator_DSTATE_b) + rtb_FilterCoefficient_f;

  /* Saturate: '<S47>/Saturation' incorporates:
   *  DeadZone: '<S33>/DeadZone'
   */
  if (rtb_SignPreIntegrator > 8.0) {
    /* Saturate: '<S47>/Saturation' */
    ControlePosicao_Y.SinalControleW = 8.0;
    rtb_SignPreIntegrator -= 8.0;
  } else {
    if (rtb_SignPreIntegrator < -8.0) {
      /* Saturate: '<S47>/Saturation' */
      ControlePosicao_Y.SinalControleW = -8.0;
    } else {
      /* Saturate: '<S47>/Saturation' */
      ControlePosicao_Y.SinalControleW = rtb_SignPreIntegrator;
    }

    if (rtb_SignPreIntegrator >= -8.0) {
      rtb_SignPreIntegrator = 0.0;
    } else {
      rtb_SignPreIntegrator -= -8.0;
    }
  }

  /* End of Saturate: '<S47>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  denAccum_1 = (ControlePosicao_Y.SinalControleW - -1.95084041195921 *
                ControlePosicao_DW.TFPosicaoAngular_states[0]) -
    0.950870806628501 * ControlePosicao_DW.TFPosicaoAngular_states[1];

  /* Outport: '<Root>/PosRoboAngular' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoAngular'
   */
  ControlePosicao_Y.PosRoboAngular = 0.006140383035771 * denAccum_1;

  /* Outport: '<Root>/ErroW' */
  ControlePosicao_Y.ErroW = rtb_IntegralGain;

  /* RelationalOperator: '<S31>/NotEqual' */
  rtb_NotEqual_p2 = (0.0 != rtb_SignPreIntegrator);

  /* Signum: '<S31>/SignPreSat' */
  if (rtb_SignPreIntegrator < 0.0) {
    rtb_SignPreIntegrator = -1.0;
  } else {
    if (rtb_SignPreIntegrator > 0.0) {
      rtb_SignPreIntegrator = 1.0;
    }
  }

  /* End of Signum: '<S31>/SignPreSat' */

  /* Gain: '<S37>/Integral Gain' */
  rtb_IntegralGain *= 0.128919474636713;

  /* Switch: '<S81>/Switch' incorporates:
   *  Constant: '<S81>/Constant1'
   *  Logic: '<S81>/AND3'
   */
  if (rtb_NotEqual && rtb_Equal1) {
    rtb_Integrator_p = 0.0;
  }

  /* End of Switch: '<S81>/Switch' */

  /* Update for DiscreteIntegrator: '<S90>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE += 0.015 * rtb_Integrator_p;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S90>/Integrator' */

  /* Update for DiscreteIntegrator: '<S85>/Filter' */
  ControlePosicao_DW.Filter_DSTATE += 0.015 * rtb_FilterCoefficient;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S85>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[1] =
    ControlePosicao_DW.TFPosicaoX_states[0];
  ControlePosicao_DW.TFPosicaoX_states[0] = denAccum;

  /* Switch: '<S131>/Switch' incorporates:
   *  Constant: '<S131>/Constant1'
   *  Logic: '<S131>/AND3'
   */
  if (rtb_NotEqual_o && rtb_Equal1_lz) {
    rtb_Integrator_d = 0.0;
  }

  /* End of Switch: '<S131>/Switch' */

  /* Update for DiscreteIntegrator: '<S140>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_i += 0.015 * rtb_Integrator_d;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState_p = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S140>/Integrator' */

  /* Update for DiscreteIntegrator: '<S135>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b += 0.015 * rtb_FilterCoefficient_l;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState_o = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S135>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[1] =
    ControlePosicao_DW.TFPosicaoY_states[0];
  ControlePosicao_DW.TFPosicaoY_states[0] = denAccum_0;

  /* Signum: '<S31>/SignPreIntegrator' */
  if (rtb_IntegralGain < 0.0) {
    rtb_Integrator_p = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    rtb_Integrator_p = 1.0;
  } else {
    rtb_Integrator_p = rtb_IntegralGain;
  }

  /* End of Signum: '<S31>/SignPreIntegrator' */

  /* Switch: '<S31>/Switch' incorporates:
   *  Constant: '<S31>/Constant1'
   *  DataTypeConversion: '<S31>/DataTypeConv1'
   *  DataTypeConversion: '<S31>/DataTypeConv2'
   *  Logic: '<S31>/AND3'
   *  RelationalOperator: '<S31>/Equal1'
   */
  if (rtb_NotEqual_p2 && (static_cast<int8_T>(rtb_SignPreIntegrator) ==
                          static_cast<int8_T>(rtb_Integrator_p))) {
    rtb_IntegralGain = 0.0;
  }

  /* End of Switch: '<S31>/Switch' */

  /* Update for DiscreteIntegrator: '<S40>/Integrator' incorporates:
   *  DiscreteIntegrator: '<S35>/Filter'
   */
  ControlePosicao_DW.Integrator_DSTATE_b += 0.015 * rtb_IntegralGain;
  if (rtb_Merge_h > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_m = 1;
    ControlePosicao_DW.Filter_PrevResetState_p = 1;
  } else {
    if (rtb_Merge_h < 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_m = -1;
    } else if (rtb_Merge_h == 0.0) {
      ControlePosicao_DW.Integrator_PrevResetState_m = 0;
    } else {
      ControlePosicao_DW.Integrator_PrevResetState_m = 2;
    }

    if (rtb_Merge_h < 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_p = -1;
    } else if (rtb_Merge_h == 0.0) {
      ControlePosicao_DW.Filter_PrevResetState_p = 0;
    } else {
      ControlePosicao_DW.Filter_PrevResetState_p = 2;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S40>/Integrator' */

  /* Update for DiscreteIntegrator: '<S35>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_d += 0.015 * rtb_FilterCoefficient_f;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  ControlePosicao_DW.TFPosicaoAngular_states[1] =
    ControlePosicao_DW.TFPosicaoAngular_states[0];
  ControlePosicao_DW.TFPosicaoAngular_states[0] = denAccum_1;
}

/* Model initialize function */
void ControlePosicaoModelClass::initialize()
{
  /* InitializeConditions for DiscreteIntegrator: '<S90>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S85>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S140>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState_p = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S135>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState_o = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S40>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState_m = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S35>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState_p = 2;
}

/* Model terminate function */
void ControlePosicaoModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControlePosicaoModelClass::ControlePosicaoModelClass() :
  ControlePosicao_DW(),
  ControlePosicao_U(),
  ControlePosicao_Y(),
  ControlePosicao_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControlePosicaoModelClass::~ControlePosicaoModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControlePosicao_T * ControlePosicaoModelClass::getRTM()
{
  return (&ControlePosicao_M);
}
