function [mrseVal] = MRSE(valPredX, valRealX, valPredY, valRealY, valPredAng, valRealAng)
%MRSE Calcula o valor MRSE (Mean Relative Squared Error) para a amostra fornecida
%   valPred é o valor predito
%   valReal é o valor real

% Calcula o erro
erroX   = valRealX - valPredX;
erroY   = valRealY - valPredY;
erroAng = valRealAng - valPredAng;

% Calcula o MRSE 
mrseVal = (1/3) * ( sqrt(sum(erroX.^2  ) / sum(valRealX.^2  )) + ...
                    sqrt(sum(erroY.^2  ) / sum(valRealY.^2  )) + ...
                    sqrt(sum(erroAng.^2) / sum(valRealAng.^2)) ) * 100;
end