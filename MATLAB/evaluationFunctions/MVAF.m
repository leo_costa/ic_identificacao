function [mvafVal] = MVAF(valPredX, valRealX, valPredY, valRealY, valPredAng, valRealAng)
%MVAF Calcula o valor MVAF (Mean Variance Accounted For) para a amostra fornecida
%   Detailed explanation goes here

% Calcula o erro
erroX   = valRealX - valPredX;
erroY   = valRealY - valPredY;
erroAng = valRealAng - valPredAng;

% Calcula o MVAF 
mvafVal = (1/3) * ( (1 - (var(erroX  ) / var(valRealX  ))) + ...
                    (1 - (var(erroY  ) / var(valRealY  ))) + ...
                    (1 - (var(erroAng) / var(valRealAng))) ) * 100;
end

