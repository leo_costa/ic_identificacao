function fitConv = convertFit(fit, rows, columns)
fitConv = zeros(rows, columns);
j=1;
    for i=1:3:12
        fitConv(i:i+2,:) = cell2mat(fit(:,j));
        j = j+1;
    end
    
% fitConv = ["X","Y","$\theta$" ; fitConv];
end

