close all; clc; %clear;
%% Adiciona as funções auxiliares
addpath('importFunctions/');
addpath('evaluationFunctions/');
addpath('plotFunctions/');
addpath('auxFunctions/');

%% Definição dos experimentos
Enderecos = cell(3,1);
endBase = uigetdir(pwd,"Select the folder containing the experiment data");
Enderecos{1} = {endBase + "/PRBS_0/"};
Enderecos{2} = {endBase + "/PRBS_1/"};
Enderecos{3} = {endBase + "/GBN_70/"};

%% Leitura dos experimentos
expData = cell(3,1);
expLength = zeros(3,1);

for i=1:3
    [expData{i}, expLength(i)] = getIDData(Enderecos{i});
%         figure;
%         plot(expData{i});
end
    
%% Cria os modelos
OptArx = arxOptions;
OptArx.Focus = 'prediction';
OptArmax = armaxOptions;
OptArmax.Focus = 'prediction';

NA = ones(3,3);
NB = ones(3,3);
NC = ones(3,1);
NK = ones(3,3);

ordensARMAX = [3,2,2,5];
ordensARX = [6,5,6];

expForIdent = 3;

armaxModel = armax(expData{expForIdent}, ...
                [ordensARMAX(1)*NA ordensARMAX(2)*NB ...
                 ordensARMAX(3)*NC ordensARMAX(4)*NK], OptArmax);
             
armaxModel.Name = strcat("Armax(", string(ordensARMAX(1)) , ",",...
                                   string(ordensARMAX(2)) , ",",...
                                   string(ordensARMAX(3)) ,") with nK = ",...
                                   string(ordensARMAX(4)) );
                               
arxModel = arx(expData{expForIdent}, ...
                [ordensARX(1)*NA ordensARX(2)*NB ...
                 ordensARX(3)*NK], OptArx);
             
arxModel.Name = strcat("Arx(", string(ordensARX(1)) , ",",...
                               string(ordensARX(2)) , ") with nK = ",...
                               string(ordensARX(3)) );
%% Compara os modelos
for i=1:3
    figure('Name',strcat("Experimento ", string(i)),'NumberTitle','off');                      
    compare(expData{i}, arxModel, armaxModel, 20);
    xlim([0 expLength(i)*expData{i}.Ts])
    grid on;  
end
%% Calculo do MRSE
valMRSEarx    = zeros(3,4);
valMRSEarmax  = zeros(3,4);
valNMRSEarx   = cell(3,4);
valNMRSEarmax = cell(3,4);

nPred = [1,5,10,20];

    for p=1:4
        for i=1:3
            [saidaModelo, modeloNRMSE, ~] = compare(expData{i}, ...
                                          arxModel, armaxModel, nPred(p));

            saidaAuxArx = get(saidaModelo{1}, 'OutputData');
            saidaAuxArmax = get(saidaModelo{2}, 'OutputData');

            saidaReal = get(expData{i}, 'OutputData');

            valMRSEarx(i,p) = MRSE(saidaAuxArx(:,1), saidaReal(:,1), ...
                                   saidaAuxArx(:,2), saidaReal(:,2), ...
                                   saidaAuxArx(:,3), saidaReal(:,3));
                               
            valNMRSEarx{i,p} = modeloNRMSE{1}';

            valMRSEarmax(i,p) = MRSE(saidaAuxArmax(:,1), saidaReal(:,1),...
                                     saidaAuxArmax(:,2), saidaReal(:,2),...
                                     saidaAuxArmax(:,3), saidaReal(:,3));
                                 
            valNMRSEarmax{i,p} = modeloNRMSE{2}';
        end
    end
% disp(valMRSEarx);
% disp(valMRSEarmax);

%% Cria os arquivos de fit
fitARX = convertFit(valNMRSEarx, 3*length(nPred), 3);
fitARMAX = convertFit(valNMRSEarmax, 3*length(nPred), 3);

writematrix(fitARX,"fitARX" + string(expForIdent) + "_.csv",...
    'Delimiter', ',');
writematrix(fitARMAX,"fitARMAX" + string(expForIdent) + "_.csv",...
    'Delimiter', ',');

writematrix(valMRSEarx,"mrseARX" + string(expForIdent) + "_.csv",...
    'Delimiter', ',');
writematrix(valMRSEarmax,"mrseARMAX" + string(expForIdent) + "_.csv",...
    'Delimiter', ',');

arxName = " $ARX(" + join(string(ordensARX(1:end-1)), ",") + ...
    ")$ com $n_k=" + string(ordensARX(end)) + "$";
armaxName =  " $ARMAX(" + join(string(ordensARMAX(1:end-1)), ",") + ...
    ")$ com $n_k=" + string(ordensARMAX(end)) + "$";
plotBoxPlot(cell2mat(valNMRSEarx), arxName);
plotBoxPlot(cell2mat(valNMRSEarmax), armaxName);

plotMRSE(valMRSEarx', arxName);
plotMRSE(valMRSEarmax', armaxName);
%% Verificação de correlação
    for i=1:3
        inpData_1 = get(expData{i}, 'InputData');
        sprintf("Correlação entre as entradas X,Y,Theta Exp %d",i)
        disp(corrcoef(inpData_1));
        for j=i:3
            if i ~= j
               inpData_2 = get(expData{j}, 'InputData');
               sprintf("Correlação entre as entradas X,Y,Theta dos experimentos %d com %d",i,j)
               disp(corrcoef(inpData_1(1:min(expLength) ), inpData_2(1:min(expLength) ) ) );
            end
        end
    end