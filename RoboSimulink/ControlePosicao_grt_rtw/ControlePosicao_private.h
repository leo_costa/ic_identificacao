/*
 * ControlePosicao_private.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.4
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Wed May 11 17:01:01 2022
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_private_h_
#define RTW_HEADER_ControlePosicao_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#endif                               /* RTW_HEADER_ControlePosicao_private_h_ */
