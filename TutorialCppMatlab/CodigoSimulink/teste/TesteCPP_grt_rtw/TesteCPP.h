/*
 * TesteCPP.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "TesteCPP".
 *
 * Model version              : 1.14
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Wed Oct  9 17:28:03 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TesteCPP_h_
#define RTW_HEADER_TesteCPP_h_
#include <string.h>
#include <stddef.h>
#ifndef TesteCPP_COMMON_INCLUDES_
# define TesteCPP_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* TesteCPP_COMMON_INCLUDES_ */

#include "TesteCPP_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T TransferFcn2_states[2];       /* '<Root>/Transfer Fcn2' */
  real_T Integrator_DSTATE;            /* '<S76>/Integrator' */
  real_T Filter_DSTATE;                /* '<S71>/Filter' */
  real_T TransferFcn3_states[2];       /* '<Root>/Transfer Fcn3' */
  real_T Integrator_DSTATE_c;          /* '<S32>/Integrator' */
  real_T Filter_DSTATE_i;              /* '<S27>/Filter' */
} DW_TesteCPP_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T PosX;                         /* '<Root>/In1' */
  real_T PosY;                         /* '<Root>/In2' */
} ExtU_TesteCPP_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Out1;                         /* '<Root>/Out1' */
  real_T Out2;                         /* '<Root>/Out2' */
  real_T Out3;                         /* '<Root>/Out3' */
  real_T Out4;                         /* '<Root>/Out4' */
} ExtY_TesteCPP_T;

/* Real-time Model Data Structure */
struct tag_RTM_TesteCPP_T {
  const char_T *errorStatus;
};

/* Class declaration for model TesteCPP */
class TesteCPPModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_TesteCPP_T TesteCPP_U;

  /* External outputs */
  ExtY_TesteCPP_T TesteCPP_Y;

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  TesteCPPModelClass();

  /* Destructor */
  ~TesteCPPModelClass();

  /* Real-Time Model get method */
  RT_MODEL_TesteCPP_T * getRTM();

  /* private data and function members */
 private:
  /* Block states */
  DW_TesteCPP_T TesteCPP_DW;

  /* Real-Time Model */
  RT_MODEL_TesteCPP_T TesteCPP_M;
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'TesteCPP'
 * '<S1>'   : 'TesteCPP/Discrete PID Controller'
 * '<S2>'   : 'TesteCPP/Discrete PID Controller1'
 * '<S3>'   : 'TesteCPP/Discrete PID Controller/Anti-windup'
 * '<S4>'   : 'TesteCPP/Discrete PID Controller/D Gain'
 * '<S5>'   : 'TesteCPP/Discrete PID Controller/Filter'
 * '<S6>'   : 'TesteCPP/Discrete PID Controller/Filter ICs'
 * '<S7>'   : 'TesteCPP/Discrete PID Controller/I Gain'
 * '<S8>'   : 'TesteCPP/Discrete PID Controller/Ideal P Gain'
 * '<S9>'   : 'TesteCPP/Discrete PID Controller/Ideal P Gain Fdbk'
 * '<S10>'  : 'TesteCPP/Discrete PID Controller/Integrator'
 * '<S11>'  : 'TesteCPP/Discrete PID Controller/Integrator ICs'
 * '<S12>'  : 'TesteCPP/Discrete PID Controller/N Copy'
 * '<S13>'  : 'TesteCPP/Discrete PID Controller/N Gain'
 * '<S14>'  : 'TesteCPP/Discrete PID Controller/P Copy'
 * '<S15>'  : 'TesteCPP/Discrete PID Controller/Parallel P Gain'
 * '<S16>'  : 'TesteCPP/Discrete PID Controller/Reset Signal'
 * '<S17>'  : 'TesteCPP/Discrete PID Controller/Saturation'
 * '<S18>'  : 'TesteCPP/Discrete PID Controller/Saturation Fdbk'
 * '<S19>'  : 'TesteCPP/Discrete PID Controller/Sum'
 * '<S20>'  : 'TesteCPP/Discrete PID Controller/Sum Fdbk'
 * '<S21>'  : 'TesteCPP/Discrete PID Controller/Tracking Mode'
 * '<S22>'  : 'TesteCPP/Discrete PID Controller/Tracking Mode Sum'
 * '<S23>'  : 'TesteCPP/Discrete PID Controller/postSat Signal'
 * '<S24>'  : 'TesteCPP/Discrete PID Controller/preSat Signal'
 * '<S25>'  : 'TesteCPP/Discrete PID Controller/Anti-windup/Passthrough'
 * '<S26>'  : 'TesteCPP/Discrete PID Controller/D Gain/Internal Parameters'
 * '<S27>'  : 'TesteCPP/Discrete PID Controller/Filter/Disc. Forward Euler Filter'
 * '<S28>'  : 'TesteCPP/Discrete PID Controller/Filter ICs/Internal IC - Filter'
 * '<S29>'  : 'TesteCPP/Discrete PID Controller/I Gain/Internal Parameters'
 * '<S30>'  : 'TesteCPP/Discrete PID Controller/Ideal P Gain/Passthrough'
 * '<S31>'  : 'TesteCPP/Discrete PID Controller/Ideal P Gain Fdbk/Disabled'
 * '<S32>'  : 'TesteCPP/Discrete PID Controller/Integrator/Discrete'
 * '<S33>'  : 'TesteCPP/Discrete PID Controller/Integrator ICs/Internal IC'
 * '<S34>'  : 'TesteCPP/Discrete PID Controller/N Copy/Disabled'
 * '<S35>'  : 'TesteCPP/Discrete PID Controller/N Gain/Internal Parameters'
 * '<S36>'  : 'TesteCPP/Discrete PID Controller/P Copy/Disabled'
 * '<S37>'  : 'TesteCPP/Discrete PID Controller/Parallel P Gain/Internal Parameters'
 * '<S38>'  : 'TesteCPP/Discrete PID Controller/Reset Signal/Disabled'
 * '<S39>'  : 'TesteCPP/Discrete PID Controller/Saturation/Enabled'
 * '<S40>'  : 'TesteCPP/Discrete PID Controller/Saturation Fdbk/Disabled'
 * '<S41>'  : 'TesteCPP/Discrete PID Controller/Sum/Sum_PID'
 * '<S42>'  : 'TesteCPP/Discrete PID Controller/Sum Fdbk/Disabled'
 * '<S43>'  : 'TesteCPP/Discrete PID Controller/Tracking Mode/Disabled'
 * '<S44>'  : 'TesteCPP/Discrete PID Controller/Tracking Mode Sum/Passthrough'
 * '<S45>'  : 'TesteCPP/Discrete PID Controller/postSat Signal/Forward_Path'
 * '<S46>'  : 'TesteCPP/Discrete PID Controller/preSat Signal/Forward_Path'
 * '<S47>'  : 'TesteCPP/Discrete PID Controller1/Anti-windup'
 * '<S48>'  : 'TesteCPP/Discrete PID Controller1/D Gain'
 * '<S49>'  : 'TesteCPP/Discrete PID Controller1/Filter'
 * '<S50>'  : 'TesteCPP/Discrete PID Controller1/Filter ICs'
 * '<S51>'  : 'TesteCPP/Discrete PID Controller1/I Gain'
 * '<S52>'  : 'TesteCPP/Discrete PID Controller1/Ideal P Gain'
 * '<S53>'  : 'TesteCPP/Discrete PID Controller1/Ideal P Gain Fdbk'
 * '<S54>'  : 'TesteCPP/Discrete PID Controller1/Integrator'
 * '<S55>'  : 'TesteCPP/Discrete PID Controller1/Integrator ICs'
 * '<S56>'  : 'TesteCPP/Discrete PID Controller1/N Copy'
 * '<S57>'  : 'TesteCPP/Discrete PID Controller1/N Gain'
 * '<S58>'  : 'TesteCPP/Discrete PID Controller1/P Copy'
 * '<S59>'  : 'TesteCPP/Discrete PID Controller1/Parallel P Gain'
 * '<S60>'  : 'TesteCPP/Discrete PID Controller1/Reset Signal'
 * '<S61>'  : 'TesteCPP/Discrete PID Controller1/Saturation'
 * '<S62>'  : 'TesteCPP/Discrete PID Controller1/Saturation Fdbk'
 * '<S63>'  : 'TesteCPP/Discrete PID Controller1/Sum'
 * '<S64>'  : 'TesteCPP/Discrete PID Controller1/Sum Fdbk'
 * '<S65>'  : 'TesteCPP/Discrete PID Controller1/Tracking Mode'
 * '<S66>'  : 'TesteCPP/Discrete PID Controller1/Tracking Mode Sum'
 * '<S67>'  : 'TesteCPP/Discrete PID Controller1/postSat Signal'
 * '<S68>'  : 'TesteCPP/Discrete PID Controller1/preSat Signal'
 * '<S69>'  : 'TesteCPP/Discrete PID Controller1/Anti-windup/Passthrough'
 * '<S70>'  : 'TesteCPP/Discrete PID Controller1/D Gain/Internal Parameters'
 * '<S71>'  : 'TesteCPP/Discrete PID Controller1/Filter/Disc. Forward Euler Filter'
 * '<S72>'  : 'TesteCPP/Discrete PID Controller1/Filter ICs/Internal IC - Filter'
 * '<S73>'  : 'TesteCPP/Discrete PID Controller1/I Gain/Internal Parameters'
 * '<S74>'  : 'TesteCPP/Discrete PID Controller1/Ideal P Gain/Passthrough'
 * '<S75>'  : 'TesteCPP/Discrete PID Controller1/Ideal P Gain Fdbk/Disabled'
 * '<S76>'  : 'TesteCPP/Discrete PID Controller1/Integrator/Discrete'
 * '<S77>'  : 'TesteCPP/Discrete PID Controller1/Integrator ICs/Internal IC'
 * '<S78>'  : 'TesteCPP/Discrete PID Controller1/N Copy/Disabled'
 * '<S79>'  : 'TesteCPP/Discrete PID Controller1/N Gain/Internal Parameters'
 * '<S80>'  : 'TesteCPP/Discrete PID Controller1/P Copy/Disabled'
 * '<S81>'  : 'TesteCPP/Discrete PID Controller1/Parallel P Gain/Internal Parameters'
 * '<S82>'  : 'TesteCPP/Discrete PID Controller1/Reset Signal/Disabled'
 * '<S83>'  : 'TesteCPP/Discrete PID Controller1/Saturation/Enabled'
 * '<S84>'  : 'TesteCPP/Discrete PID Controller1/Saturation Fdbk/Disabled'
 * '<S85>'  : 'TesteCPP/Discrete PID Controller1/Sum/Sum_PID'
 * '<S86>'  : 'TesteCPP/Discrete PID Controller1/Sum Fdbk/Disabled'
 * '<S87>'  : 'TesteCPP/Discrete PID Controller1/Tracking Mode/Disabled'
 * '<S88>'  : 'TesteCPP/Discrete PID Controller1/Tracking Mode Sum/Passthrough'
 * '<S89>'  : 'TesteCPP/Discrete PID Controller1/postSat Signal/Forward_Path'
 * '<S90>'  : 'TesteCPP/Discrete PID Controller1/preSat Signal/Forward_Path'
 */
#endif                                 /* RTW_HEADER_TesteCPP_h_ */
