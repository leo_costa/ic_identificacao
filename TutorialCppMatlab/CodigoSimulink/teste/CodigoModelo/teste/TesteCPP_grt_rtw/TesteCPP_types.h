/*
 * TesteCPP_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "TesteCPP".
 *
 * Model version              : 1.14
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Wed Oct  9 17:28:03 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TesteCPP_types_h_
#define RTW_HEADER_TesteCPP_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_TesteCPP_T RT_MODEL_TesteCPP_T;

#endif                                 /* RTW_HEADER_TesteCPP_types_h_ */
