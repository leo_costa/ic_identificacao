/*
 * TesteCPP.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "TesteCPP".
 *
 * Model version              : 1.14
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C++ source code generated on : Wed Oct  9 17:28:03 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "TesteCPP.h"
#include "TesteCPP_private.h"

/* Model step function */
void TesteCPPModelClass::step()
{
  real_T denAccum;
  real_T rtb_Integrator_g;
  real_T rtb_FilterCoefficient;
  real_T rtb_Sum3;
  real_T rtb_FilterCoefficient_m;

  /* DiscreteTransferFcn: '<Root>/Transfer Fcn2' */
  TesteCPP_Y.Out1 = 0.001982781401951 * TesteCPP_DW.TransferFcn2_states[0] +
    0.00187712456709 * TesteCPP_DW.TransferFcn2_states[1];

  /* Sum: '<Root>/Sum2' incorporates:
   *  DiscreteTransferFcn: '<Root>/Transfer Fcn2'
   *  Inport: '<Root>/In1'
   */
  rtb_Integrator_g = TesteCPP_U.PosX - TesteCPP_Y.Out1;

  /* Gain: '<S79>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S71>/Filter'
   *  Gain: '<S70>/Derivative Gain'
   *  Sum: '<S71>/SumD'
   */
  rtb_FilterCoefficient = (-0.069877764365876 * rtb_Integrator_g -
    TesteCPP_DW.Filter_DSTATE) * 1.1217876456889631;

  /* Sum: '<S85>/Sum' incorporates:
   *  DiscreteIntegrator: '<S76>/Integrator'
   *  Gain: '<S81>/Proportional Gain'
   */
  rtb_Sum3 = (0.780465533692083 * rtb_Integrator_g +
              TesteCPP_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

  /* Saturate: '<S83>/Saturation' */
  if (rtb_Sum3 > 4.0) {
    /* Outport: '<Root>/Out3' */
    TesteCPP_Y.Out3 = 4.0;
  } else if (rtb_Sum3 < -4.0) {
    /* Outport: '<Root>/Out3' */
    TesteCPP_Y.Out3 = -4.0;
  } else {
    /* Outport: '<Root>/Out3' */
    TesteCPP_Y.Out3 = rtb_Sum3;
  }

  /* End of Saturate: '<S83>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/Transfer Fcn3' */
  TesteCPP_Y.Out2 = 0.001283843872055 * TesteCPP_DW.TransferFcn3_states[0] +
    0.001239015400059 * TesteCPP_DW.TransferFcn3_states[1];

  /* Sum: '<Root>/Sum3' incorporates:
   *  DiscreteTransferFcn: '<Root>/Transfer Fcn3'
   *  Inport: '<Root>/In2'
   */
  rtb_Sum3 = TesteCPP_U.PosY - TesteCPP_Y.Out2;

  /* Gain: '<S35>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S27>/Filter'
   *  Gain: '<S26>/Derivative Gain'
   *  Sum: '<S27>/SumD'
   */
  rtb_FilterCoefficient_m = (0.0 * rtb_Sum3 - TesteCPP_DW.Filter_DSTATE_i) *
    29.976181125676057;

  /* Sum: '<S41>/Sum' incorporates:
   *  DiscreteIntegrator: '<S32>/Integrator'
   *  Gain: '<S37>/Proportional Gain'
   */
  TesteCPP_Y.Out4 = (0.85714791965314 * rtb_Sum3 +
                     TesteCPP_DW.Integrator_DSTATE_c) + rtb_FilterCoefficient_m;

  /* Saturate: '<S39>/Saturation' */
  if (TesteCPP_Y.Out4 > 4.0) {
    /* Sum: '<S41>/Sum' incorporates:
     *  Outport: '<Root>/Out4'
     */
    TesteCPP_Y.Out4 = 4.0;
  } else {
    if (TesteCPP_Y.Out4 < -4.0) {
      /* Sum: '<S41>/Sum' incorporates:
       *  Outport: '<Root>/Out4'
       */
      TesteCPP_Y.Out4 = -4.0;
    }
  }

  /* End of Saturate: '<S39>/Saturation' */

  /* Update for DiscreteTransferFcn: '<Root>/Transfer Fcn2' incorporates:
   *  Outport: '<Root>/Out3'
   */
  denAccum = (TesteCPP_Y.Out3 - -1.8445744672074189 *
              TesteCPP_DW.TransferFcn2_states[0]) - 0.848497619373556 *
    TesteCPP_DW.TransferFcn2_states[1];
  TesteCPP_DW.TransferFcn2_states[1] = TesteCPP_DW.TransferFcn2_states[0];
  TesteCPP_DW.TransferFcn2_states[0] = denAccum;

  /* Update for DiscreteIntegrator: '<S76>/Integrator' incorporates:
   *  Gain: '<S73>/Integral Gain'
   */
  TesteCPP_DW.Integrator_DSTATE += 1.14715318178071 * rtb_Integrator_g * 0.032;

  /* Update for DiscreteIntegrator: '<S71>/Filter' */
  TesteCPP_DW.Filter_DSTATE += 0.032 * rtb_FilterCoefficient;

  /* Update for DiscreteTransferFcn: '<Root>/Transfer Fcn3' incorporates:
   *  Outport: '<Root>/Out4'
   */
  denAccum = (TesteCPP_Y.Out4 - -1.896277602858504 *
              TesteCPP_DW.TransferFcn3_states[0]) - 0.898863582166294 *
    TesteCPP_DW.TransferFcn3_states[1];
  TesteCPP_DW.TransferFcn3_states[1] = TesteCPP_DW.TransferFcn3_states[0];
  TesteCPP_DW.TransferFcn3_states[0] = denAccum;

  /* Update for DiscreteIntegrator: '<S32>/Integrator' incorporates:
   *  Gain: '<S29>/Integral Gain'
   */
  TesteCPP_DW.Integrator_DSTATE_c += 1.11330426884818 * rtb_Sum3 * 0.032;

  /* Update for DiscreteIntegrator: '<S27>/Filter' */
  TesteCPP_DW.Filter_DSTATE_i += 0.032 * rtb_FilterCoefficient_m;
}

/* Model initialize function */
void TesteCPPModelClass::initialize()
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus((&TesteCPP_M), (NULL));

  /* states (dwork) */
  (void) memset((void *)&TesteCPP_DW, 0,
                sizeof(DW_TesteCPP_T));

  /* external inputs */
  (void)memset(&TesteCPP_U, 0, sizeof(ExtU_TesteCPP_T));

  /* external outputs */
  (void) memset((void *)&TesteCPP_Y, 0,
                sizeof(ExtY_TesteCPP_T));

  /* InitializeConditions for DiscreteIntegrator: '<S76>/Integrator' */
  TesteCPP_DW.Integrator_DSTATE = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S71>/Filter' */
  TesteCPP_DW.Filter_DSTATE = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/Transfer Fcn2' */
  TesteCPP_DW.TransferFcn2_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/Transfer Fcn3' */
  TesteCPP_DW.TransferFcn3_states[0] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/Transfer Fcn2' */
  TesteCPP_DW.TransferFcn2_states[1] = 0.0;

  /* InitializeConditions for DiscreteTransferFcn: '<Root>/Transfer Fcn3' */
  TesteCPP_DW.TransferFcn3_states[1] = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S32>/Integrator' */
  TesteCPP_DW.Integrator_DSTATE_c = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S27>/Filter' */
  TesteCPP_DW.Filter_DSTATE_i = 0.0;
}

/* Model terminate function */
void TesteCPPModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
TesteCPPModelClass::TesteCPPModelClass()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
TesteCPPModelClass::~TesteCPPModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_TesteCPP_T * TesteCPPModelClass::getRTM()
{
  return (&TesteCPP_M);
}
