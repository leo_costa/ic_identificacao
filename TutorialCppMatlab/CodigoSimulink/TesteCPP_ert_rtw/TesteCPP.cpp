//
// Sponsored License - for use in support of a program or activity
// sponsored by MathWorks.  Not for government, commercial or other
// non-sponsored organizational use.
//
// File: TesteCPP.cpp
//
// Code generated for Simulink model 'TesteCPP'.
//
// Model version                  : 1.21
// Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
// C/C++ source code generated on : Fri Oct 11 18:43:17 2019
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Linux 64)
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#include "TesteCPP.h"

// Model step function
void TesteCPPModelClass::step()
{
  real_T rtb_Integrator_g;
  real_T rtb_FilterCoefficient;
  real_T rtb_Sum3;
  real_T rtb_FilterCoefficient_m;
  real_T u0;

  // DiscreteTransferFcn: '<Root>/Transfer Fcn2'
  rtb_Integrator_g = 0.001982781401951 * rtDW.TransferFcn2_states[0] +
    0.00187712456709 * rtDW.TransferFcn2_states[1];

  // Outport: '<Root>/Out1'
  rtY.Out1 = rtb_Integrator_g;

  // Sum: '<Root>/Sum2' incorporates:
  //   Inport: '<Root>/In1'

  rtb_Integrator_g = rtU.PosX - rtb_Integrator_g;

  // Gain: '<S79>/Filter Coefficient' incorporates:
  //   DiscreteIntegrator: '<S71>/Filter'
  //   Gain: '<S70>/Derivative Gain'
  //   Sum: '<S71>/SumD'

  rtb_FilterCoefficient = (-0.069877764365876 * rtb_Integrator_g -
    rtDW.Filter_DSTATE) * 1.1217876456889631;

  // Sum: '<S85>/Sum' incorporates:
  //   DiscreteIntegrator: '<S76>/Integrator'
  //   Gain: '<S81>/Proportional Gain'

  rtb_Sum3 = (0.780465533692083 * rtb_Integrator_g + rtDW.Integrator_DSTATE) +
    rtb_FilterCoefficient;

  // Saturate: '<S83>/Saturation'
  if (rtb_Sum3 > 4.0) {
    // Outport: '<Root>/Out3'
    rtY.Out3 = 4.0;
  } else if (rtb_Sum3 < -4.0) {
    // Outport: '<Root>/Out3'
    rtY.Out3 = -4.0;
  } else {
    // Outport: '<Root>/Out3'
    rtY.Out3 = rtb_Sum3;
  }

  // End of Saturate: '<S83>/Saturation'

  // DiscreteTransferFcn: '<Root>/Transfer Fcn3'
  rtb_Sum3 = 0.001283843872055 * rtDW.TransferFcn3_states[0] + 0.001239015400059
    * rtDW.TransferFcn3_states[1];

  // Outport: '<Root>/Out2'
  rtY.Out2 = rtb_Sum3;

  // Sum: '<Root>/Sum3' incorporates:
  //   Inport: '<Root>/In2'

  rtb_Sum3 = rtU.PosY - rtb_Sum3;

  // Gain: '<S35>/Filter Coefficient' incorporates:
  //   DiscreteIntegrator: '<S27>/Filter'
  //   Gain: '<S26>/Derivative Gain'
  //   Sum: '<S27>/SumD'

  rtb_FilterCoefficient_m = (0.0 * rtb_Sum3 - rtDW.Filter_DSTATE_i) *
    29.976181125676057;

  // Sum: '<S41>/Sum' incorporates:
  //   DiscreteIntegrator: '<S32>/Integrator'
  //   Gain: '<S37>/Proportional Gain'

  u0 = (0.85714791965314 * rtb_Sum3 + rtDW.Integrator_DSTATE_c) +
    rtb_FilterCoefficient_m;

  // Saturate: '<S39>/Saturation'
  if (u0 > 4.0) {
    // Outport: '<Root>/Out4'
    rtY.Out4 = 4.0;
  } else if (u0 < -4.0) {
    // Outport: '<Root>/Out4'
    rtY.Out4 = -4.0;
  } else {
    // Outport: '<Root>/Out4'
    rtY.Out4 = u0;
  }

  // End of Saturate: '<S39>/Saturation'

  // Update for DiscreteTransferFcn: '<Root>/Transfer Fcn2' incorporates:
  //   Outport: '<Root>/Out3'

  u0 = (rtY.Out3 - -1.8445744672074189 * rtDW.TransferFcn2_states[0]) -
    0.848497619373556 * rtDW.TransferFcn2_states[1];
  rtDW.TransferFcn2_states[1] = rtDW.TransferFcn2_states[0];
  rtDW.TransferFcn2_states[0] = u0;

  // Update for DiscreteIntegrator: '<S76>/Integrator' incorporates:
  //   Gain: '<S73>/Integral Gain'

  rtDW.Integrator_DSTATE += 1.14715318178071 * rtb_Integrator_g * 0.032;

  // Update for DiscreteIntegrator: '<S71>/Filter'
  rtDW.Filter_DSTATE += 0.032 * rtb_FilterCoefficient;

  // Update for DiscreteTransferFcn: '<Root>/Transfer Fcn3' incorporates:
  //   Outport: '<Root>/Out4'

  u0 = (rtY.Out4 - -1.896277602858504 * rtDW.TransferFcn3_states[0]) -
    0.898863582166294 * rtDW.TransferFcn3_states[1];
  rtDW.TransferFcn3_states[1] = rtDW.TransferFcn3_states[0];
  rtDW.TransferFcn3_states[0] = u0;

  // Update for DiscreteIntegrator: '<S32>/Integrator' incorporates:
  //   Gain: '<S29>/Integral Gain'

  rtDW.Integrator_DSTATE_c += 1.11330426884818 * rtb_Sum3 * 0.032;

  // Update for DiscreteIntegrator: '<S27>/Filter'
  rtDW.Filter_DSTATE_i += 0.032 * rtb_FilterCoefficient_m;
}

// Model initialize function
void TesteCPPModelClass::initialize()
{
  // (no initialization code required)
}

// Constructor
TesteCPPModelClass::TesteCPPModelClass()
{
  // Currently there is no constructor body generated.
}

// Destructor
TesteCPPModelClass::~TesteCPPModelClass()
{
  // Currently there is no destructor body generated.
}

//
// File trailer for generated code.
//
// [EOF]
//
