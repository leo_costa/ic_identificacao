\documentclass[a4paper, 10pt, conference]{ieeeconf}

\IEEEoverridecommandlockouts      % This command is only needed if
                                  % you want to use the \thanks command
\overrideIEEEmargins              % Needed to meet printer requirements.

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{epsfig} % for postscript graphics files
\usepackage{mathptmx} % assumes new font selection scheme installed
\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{amssymb}  % assumes amsmath package installed
\usepackage{multicol,multirow,booktabs}
\usepackage{textcomp}

\newcommand{\matlab}{MATLAB\textsuperscript{\textregistered}}
\newcommand{\simulink}{Simulink\textsuperscript{\textregistered}}

\bibliographystyle{IEEEtran}

\title{\LARGE \bf

Using system identification to obtain the dynamic models of a mobile robot and
tunning a PID position controller

}

\author{
  % Revisão double blinded
  \authorblockN{XXXXXXXX XX XXXXX XXXXX$^1$ XXX XXXXXX XXXXXXXXXX$^2$}

  \authorblockN{XXXXXXXX XX XXXXX XXXXX$^1$ and XXXXXX XXXXXXXXXX$^2$}
  \authorblockA{XXXXXXXXXX XXXXXX XX XXX, XXX XXXXXXXX XX XXXXX -- XX, XXXXXX\\
  Email: XXXXXXXXXXXX@XXX.XXX.XX$^1$\\ XXXXXXX@XXX.XXX.XX$^2$ }
  \thanks{*XXXX XXXX XXX XXXXXXXXX XX XXXXXX XXXXXXXXXXXXX XXX.}% Revisão double blinded

%   \authorblockN{Leonardo da Silva Costa$^1$ and Flavio Tonidandel$^2$}
%   \authorblockA{University Center of FEI, São Bernardo do Campo -- SP, Brazil\\
%   Email: unieleocosta@fei.edu.br$^1$\\ flaviot@fei.edu.br$^2$ }
%   \thanks{*This work was supported by Centro Universitario FEI.}% Revisão double blinded

}

% Figura -> caption embaixo, Tabela -> caption em cima
\begin{document}

\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

A system identification approach is proposed to solve the problem of obtaining
the dynamic models of the robots in the Small Size League (SSL), due to its
capacity to capture all effects/parameters that influence the system, such as
the friction coefficients. First, an investigation about the system linearity
and time invariance is made, after that, a short overview about the design of
the PRBS and GBN input signals is shown. Secondly, the ARX/ARMAX models were
presented and identified using the parametrized PRBS/GBN signals. The models
were compared regarding their capacity to represent the system and to predict
the system's future states. The Transfer Function models also were identified,
via a step response experiment, and, were used to tune PID position controllers
for the robot. In conclusion, the performance of both ARX/ARMAX models was
similar, but, due to its reduced polynomial order, the ARMAX is a better choice
over ARX.  Finally, given the last in-person competition, the estimated
transfer functions proved to be a reasonable way to design the PID controllers,
providing a good performance for the robots.

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

The Small Size League (SSL) is one of the most dynamic leagues of the RoboCup
Soccer competition, due to it having a very fast paced game play, given the
high speed of the robots and the team's size, which goes from 6 to 11 robots.
In order to keep up with the speed of the game, the motion control of the
robots must be really well tunned, specifically the position controller, given
that the robots must be extremely fast and precise, for example, a goalie which
has to intercept a quick pass and shoot to goal play.

However, tunning any controller for this purpose, like a Proportional Integral
Derivative (PID), is not an easy task, since it requires a rather precise model
of the system to achieve the desired performance. Beyond that, there are many
parameters which change with time and from robot to robot.  For example, the
carpet friction coefficient, friction in the gears and mechanical wear in
general, which are insignificant in the timespan of a competition, but, can be
more significant as the years add up; and, the case where different robots must
be used together, e.g. older versions along with new ones, this has a huge
impact, specially due to the robots having different mass, maximum speed and
acceleration. Taking all of this into account increases both the complexity of
the robot model, and the time needed to estimate the parameters for each robot.

% Therefore, making the task of having well tunned
% controllers for each robot extremely time consuming, which is a great problem,
% since teams only have a couple of days to prepare everything for the
% competition.

% A few solutions to this problem are: robust controllers, which are capable of
% maintaining the plant dynamics even though the plant model changes, adaptive
% controllers, which are capable of self adapting the control effort to make the
% plant have desired dynamics, or even fuzzy controllers. The problem with these
% approaches is that they become rather complex when considering the SSL robot,
% which is a Multiple Input Multiple Output (MIMO) system with three inputs
% ($\dot x,\,\dot y,\,\dot \theta$) and three outputs ($x, y, \theta$). And, they
% still require a model of the plant, either to design, or to validate the
% controller.

Therefore, in order to quickly obtain the models of the robots, considering all
dynamics that might influence the system, this paper proposes the use of the
system identification technique.

This paper will show the steps needed to perform a proper system
identification, which includes the design of a input signal and choosing a
model type and its order. It will also compare both ARX (AutoRegressive with
eXtra inputs)\cite{tangirala2014principles} and ARMAX (AutoRegressive Moving
Average with eXtra inputs)\cite{tangirala2014principles} models, as well as
determine their best parameters for the model of an omnidirectional mobile
robot. At last, it will show how practical the system identification technique
can be when using it to tune a position PID controller for the robots.

\section{System Identification}

System identification is the technique that allows the user to estimate the
model of any system given its input and output data. It is commonly used when
there is a system which is hard to model by using the classic methods, e.g.
physics laws. According to \cite{ljung1998system}, through system
identification one can obtain the model of a system in many mathematical
formats. Transfer Functions (TF), State Space and polynomial models are the
most common.

\subsection{The Robot}

The system used in this paper is an omnidirectional robot, it is composed of
four swedish wheels at an angle $\phi = 33^\circ$ from the robot's horizontal
axis, as illustrated in Fig. \ref{fig:ssl-robot}, this setup allows the robot
to move in both $x$ and $y$ directions and spin around its center at the same
time.

There is a global coordinate system, represented by $X_f,\,Y_f$ and $\theta_f$,
used to define the position of the robot in the field, $\vec P_r = \left[ x_f,
y_f, \theta \right]$, and a local coordinate system, represented by $X_r$ and
$Y_r$, which is used as a reference for the robot to drive himself. In order to
get to a certain position, the robot receives a velocity command for each axis
of its local coordinate system ($\vec v = \left[ \dot x_r,\,\dot y_r,\, \dot
\theta \right]$).

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.4\linewidth]{Images/Plant}
  \caption{SSL omnidirectional robot}\label{fig:ssl-robot}
\end{figure}

Prior to the system identification, it is necessary to know if the system is
linear and time invariant (LTI). This is important due to the fact that, not
only the input signal design and identification algorithms are different for
nonlinear time variant systems, but the whole process can be much more
complicated \cite{gondhalekar2009strategies}. In \cite{hosseini2011comparison}
it is possible to see many techniques that can be used to experimentally
determine if a system is linear.

The technique that was used is the harmonic analysis, which consists of
applying a sine wave $y(t) = A\sin(\omega t)$ to the system's input, where, $A$
is the amplitude and $\omega$ is the angular frequency. Then, if the system is
LTI and there is no noise, the output should have no more harmonics than the
ones present in the input signal \cite{hosseini2011comparison}. This can be
confirmed by analyzing the Discrete Fourier Transform (DFT)
\cite{jenkins1999fourier} of the experiment data.

The input signals used in this experiment can be seen on Equations
\eqref{eq:sine_xy} and \eqref{eq:sine_w}, all units are in SI and the sampling
period is $15\;ms$.

\begin{align}
  V_{i}(t) &= 0.75\sin(2t),\; i=x_f,\,y_f  \label{eq:sine_xy}\\
  V_{\theta}(t) &= 2.6\sin(2t)  \label{eq:sine_w}
\end{align}

The result can be seen on Fig. \ref{fig:sine_data}, the red dashed curve is the
input data, i.e. robot velocity, and the blue curve is the output data, i.e.
robot position. The only pre-processing that was done was the detrending of the
robot position.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{Images/sine_data.pdf}
  \caption{Harmonic analysis data for each axis.}
  \label{fig:sine_data}
\end{figure}

By analyzing Fig. \ref{fig:sine_data} it is possible to see that there is a
phase shift of around $90^\circ$, which is expected, since, due to its inertia,
the robot doesn't respond instantaneously when it receives a command.

The DFT of the results for each axis can be seen on Fig. \ref{fig:dft_x},
\ref{fig:dft_y} and \ref{fig:dft_w}, respectively. Each plot shows the
magnitude and phase of the DFT, hence, it is possible to see that there are no
extra components in the output, except for those caused by the noise, however,
they have a magnitude that is insignificant. Beyond that, there is an inversion
in the phase graph, most likely caused by the phase shift seen on Fig.
\ref{fig:sine_data}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Images/dft_x.pdf}
  \caption{DFT of the $x$ axis data.}
  \label{fig:dft_x}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Images/dft_y.pdf}
  \caption{DFT of the $y$ axis data.}
  \label{fig:dft_y}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Images/dft_w.pdf}
  \caption{DFT of the $\theta$ axis data.}
  \label{fig:dft_w}
\end{figure}

Therefore, it is feasible to assume that the system is linear, even though the
noise might create a few extra components, their influence in the system output
is little.

With respect to the time invariance of the system, if the output isn't
influenced by the absolute time, it can be considered time-invariant
\cite{ljung1987system}. Therefore, since the models are supposed to be correct
for around one week, which is the duration of one competition, and it can be
updated within a few minutes, it is reasonable to assume that they don't change
with the absolute time. In conclusion, the system can be considered as LTI.

\section{Plant Excitation}

The most common family of input signals used in system identification are the
pseudo random signals, like the Pseudo Random Binary Signal (PRBS) and
Generalized Binary Noise (GBN), due to their ability of exciting all the
dynamics in the frequency range of the plant \cite{aguirre2004introduccao}. In
this paper, both signals will be used to identify the ARX/ARMAX models in order
to see if any of those is more suited to this application.

\subsection{Pseudo Random Binary Signal - PRBS}

The main advantage of the PRBS signal is that its correlation function is
really close to the white noise's correlation function. This is important
because it gives the signal a practically uniform distribution of the power
spectral density, allowing it to excite all dynamics present in the system's
frequency range \cite{soderstrom1989}.

The PRBS signal can be easily generated using a digital circuit (Fig.
\ref{fig:prbs-gen}). Note that, the bits used in the \textbf{XOR} operation
might change with $n$ \cite{aguirre2004introduccao}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.55\linewidth]{Images/PRBS_gen}
  \caption{PRBS Generator for $n = 6$ \cite{aguirre2004introduccao}}
  \label{fig:prbs-gen}
\end{figure}

The PRBS signal is defined by its duration ($N$), which can be calculated using
Equation \eqref{eq:prbs_dur}, and switching time ($T_b$), i.e. the smallest
period where the signal can change, which can be determined using the heuristic
shown in Equation \eqref{eq:prbs_tb} \cite{aguirre2004introduccao}.

\begin{align}
  N =& 2^n - 1 \label{eq:prbs_dur} \\
  \frac{\tau_{min}}{10} \leq T_{b}& \leq \frac{\tau_{min}}{3} \label{eq:prbs_tb}
\end{align}

Where, $n$ is the size of the shift register and $\tau_{min}$ is the smallest
time constant of the system. Using \cite{ogata2002modern} as reference, the
time constants of the system were measured using a unit step response and the
results can be seen in Equation \eqref{eq:t_b_eq}.

\begin{equation}
  \left.\begin{aligned}
    \tau_{x_f} = 1.01\;s \\
    \tau_{y_f} = 1.11\;s \\
    \tau_{\theta} = 0.76\;s
  \end{aligned}\right\} \Rightarrow 0.076 \leq T_b \leq 0.254
  \label{eq:t_b_eq}
\end{equation}

\subsection{Generalized Binary Noise - GBN}

The GBN \cite{tulleken1990generalized} has some similar properties to the PRBS
and it also has two parameters: the switching probability $p$, and the minimum
switch period $T^*$. They determine the probability that the signal changes in
the current time step, and the minimum time that the signal must maintain its
current value, respectively \cite{orenstein2013procedimento}. Regarding the
parametrization of the signal, \cite{tulleken1990generalized} has some
guidelines, which depends on the type/order of the system.

% According to \cite{tulleken1990generalized}, binary noise signals are
% excellent for system identification due to its similarity to a white noise.
% For instance, just like the PRBS, its energy is almost evenly distributed
% along the frequency range, allowing a lower signal amplitude to be used. This
% is important because, in some cases, it isn't allowed, or might be dangerous,
% to excite the system in its dominant frequencies during operation
% \cite{tulleken1990generalized}.

% these can be
% seen on Table \ref{tab:gbn_guide}.

% \multicolumn{2}{c}{texto}
% \multirow{2}{*}{texto}

% \begin{table}[!htb]
%   \centering
%   \caption{GBN design guidelines \cite{tulleken1990generalized}}
%   \label{tab:gbn_guide}
%   \begin{tabular}{clcc}
%     \toprule
%     \multicolumn{2}{c}{System Type} & $T^*/\tau$ & $p$ \\
%     \toprule
%     \multicolumn{2}{c}{First order} & $\frac{1}{32}$ & 0.94 \\
%     \midrule
%     \multirow{2}{7em}{Second order, osc. damped} & min. phase & $1/25$ & $0.8$ \\
%     \cline{2-4}
%     {} & non-min. phase & $1/15$ & $0.8$ \\
%     \midrule
%     \multirow{2}{7em}{Second order, over-damped} & min. phase &
%     \multirow{2}{*}{$\frac{1}{20} \text{ to } \frac{1}{10}$} &
%     \multirow{2}{*}{$0.9-0.95$} \\
%
%     {} & non-min. phase & {} & {} \\
%     \bottomrule
%   \end{tabular}
% \end{table}

% In \cite{tulleken1990generalized}, there is a fourth column in that table,
% however, according to the author, that column is just a factor that needs to be
% applied to the switching probability in case the selected sampling time is
% different from $T^*$. Even though the sampling time throughout this whole paper
% is $15\;ms$, the software that generates the input signals guarantees that each
% signal will only be update according to its switching time, i.e. $T^*$ for GBN
% and $T_b$ for PRBS.

\section{Model Types}

After properly selecting and designing the input signal, the model type must be
chosen. The model type is just how the model is represented mathematically. As
previously stated, there are many types that can be used during the system
identification. In this paper, three will be covered: Transfer Function,
which is useful when dealing with single input single output (SISO) linear
systems, especially in classic control theory \cite{ogata2002modern}, and,
ARX/ARMAX models, which are from a family of polynomial models that are
commonly used in the design of Model Predictive Controllers
\cite{ghaffari2013model}.

\subsection{Transfer Function}

When considering LIT SISO systems, the TF ($H(s)$) is one of the most commonly
used \cite{aguirre2004introduccao}. It is defined as the Laplace transform of
the impulse response of the system under no initial conditions, and can be
represented as the relation between the Laplace transform of the output
($Y(s)$) and input ($X(s)$), as seen on Equation \eqref{eq:tf_def}
\cite{ogata2002modern}.

\begin{equation}
  H(s) = \frac{Y(s)}{X(s)}
  \label{eq:tf_def}
\end{equation}

As stated earlier, the TF is a versatile type of model, because, for first and
second order systems, it can be obtained by a simple step response experiment.
Moreover, it allows a quick design of a PID controller when using the \matlab
PID Tunner.

One might question the use of the TF for the system considered in this paper,
due to the fact it is a Multiple Input Multiple Output (MIMO) system (3
inputs/3 outputs), however, since it is a omnidirectional and holonomic system,
its inputs and outputs can be considered separately.

\subsection{ARX Model}

Both ARX and ARMAX models are based on a more general representation for
discrete-time systems which can be seen on Equation \eqref{eq:arx_armax_g}
\cite{aguirre2004introduccao}. In this representation, the $B(q),\,F(q)$ and
$A(q)$ polynomials are used to model the system's input ($u(k)$), and
$C(q),\,D(q)$ and $A(q)$ polynomials are used to model the system's noise by
using a white noise function, $v(k)$.

\begin{equation}
y(k)=\frac{B(q)}{F(q)A(q)}u(k)+\frac{C(q)}{D(q)A(q)}v(k)
\label{eq:arx_armax_g}
\end{equation}

What differs between each type of polynomial model is which polynomials are
used. For instance, the ARX model only uses $B(q)$ and $A(q)$, as seen in
Equation \eqref{eq:arx_model}.

\begin{equation}
y(k)=\frac{B(q)}{A(q)}u(k)+\frac{1}{A(q)}v(k)
\label{eq:arx_model}
\end{equation}

From now onwards, the ARX models will be referenced using the ARMA notation,
which is ARX($n_a,n_b$) with delay $n_k$, this notation specifies the order of
the $A(q)$ ($n_a$) and $B(q)$ ($n_b$) polynomials, as well as the delay,
specified in number of samples ($n_k$) \cite{tangirala2014principles}.

\subsection{ARMAX Model}

Similarly to the ARX model, the ARMAX model uses the $A(q),\,B(q)$ and $C(q)$
polynomials and it can be seen on Equation \eqref{eq:armax_model}. The
difference between them is that the ARMAX includes a Moving Average term in the
ARX model \cite{tangirala2014principles}.

\begin{equation}
y(k)=\frac{B(q)}{A(q)}u(k)+\frac{C(q)}{A(q)}v(k)
\label{eq:armax_model}
\end{equation}

The ARMAX model will also be referenced using the ARMA notation, which is
ARMAX($n_a,n_b,n_c$) with delay $n_k$. Where, $n_c$ is the order of the $C(q)$
polynomial \cite{tangirala2014principles}.

It is important to note that, since the ARX/ARMAX models will be identified
considering a single MIMO system, the models will not be a single set
polynomials, but a matrix of polynomials. Hence, it is possible to specify the
order of each polynomial in the matrix, but, this is not the case. For the
sake of simplicity the order of all polynomials in the $A(q)$ matrix, for
example, will be the same.

\section{Related Work}

In the SSL the system identification technique isn't commonly used, however,
there are many studies using it for omnidirectional robots in other
applications. For example, \cite{amudhan2019design} uses system identification
to estimate the system dynamics in order to design a Linear Quadratic Tracking
(LQT) controller for an omnidirectional robot. The author compares the LQT
against a PI controller, and obtains some interesting results. There is only
one thing that differs between the approach the author has taken and what is
done in this paper, which is the system's variables used.
\cite{amudhan2019design} considered as input the wheel speed and the output as
the speed measured by the wheel's encoder, while in this paper, the input is
the robot velocity in the global frame, and the output is the robot position in
the global frame.

Although the work seen on \cite{xin2020omnidirectional} has some considerable
differences from what is being developed in this paper, it has an interesting
comparison between the dynamic model of the system, designed using classic
physics laws, and a Nonlinear ARX neural network (NARX) model. The comparison
shows that, in the case where the robot is moving and rotating at the same
time, the dynamic model isn't capable of accurately representing that behavior.
Therefore, the author solves this problem by using system identification to
obtain a NARX model, this way, improving the precision of the simulated robot
trajectory. Some analysis regarding the model stability are also presented in
\cite{xin2020omnidirectional}.

A great example of how a grey box identification works can be seen on
\cite{giro2021dynamic}, the author thoroughly explains how the system
identification technique was used to estimate the parameters of an
omnidirectional robot's dynamic model, for instance, its mass, moment of
inertia and center of mass. The dynamic model was manually designed using
Lagrange's equations and then, several experiments were made in order to
estimate the model's parameters for a unloaded and loaded robot. Even though
the application has a bigger focus on a load-carrying robot, this work has some
insightful contributions.

Some similar works have been done in the Very Small Size League (VSSL)
\cite{verySmallPereira,verySmallMendes}. Although the type of robot might be
different, i.e. in the VSSL a differential drive non-holonomic robot is used, a
lot of information can be used from these works. For example, in
\cite{verySmallPereira}, three ARX($2,2$) with delay $8$ are used to model the
robot, these models were obtained using a PRBS signal, hence, this work
provides a starting point for the possible values that might generate a good
model for the SSL robot.

\section{Methodology}

First of all, a few experiments were made in order to determine the best
parameters for the PRBS and GBN signals. After that, these signals were sent
simultaneously to the robot's inputs and the input-output data was measured.
The collected data was then used to identify the ARX/ARMAX models with its
polynomial orders ranging from 1 to 8, and delay ranging from 4 to 7. The
identification process was done using the \matlab System Identification
Toolbox.

The ARX/ARMAX models were compared using two metrics: Mean Relative Squared
Error (MRSE) and normalized Root Mean Squared Error (nMRSE), seen on Equations
\eqref{eq:MRSE} and \eqref{eq:nMRSE}, respectively.

\begin{equation}
MRSE = \frac{1}{k}\sum_{i=1}^{k} \sqrt{\frac{\sum_{j=1}^{N} \hat{e}_{ij}^{2}}{\sum_{j=1}^{N} y_{ij}^{2}}}100\%
\label{eq:MRSE}
\end{equation}

\begin{equation}
nRMSE = \Bigl(1 - \frac{\sqrt{\sum_{j=1}^{N} \hat{e}_{ij}^{2}}}{\sqrt{\sum_{j=1}^{N} (y_{j}-\bar{y})^{2}}}\Bigr)100\%
\label{eq:nMRSE}
\end{equation}

Where, $k$ is the number of outputs in the system, $N$ is the number of
samples, $\hat e$ is the prediction error at sample $j$, $y$ is the output at
sample $j$ and $\bar y$ is the mean value of the output.

The MRSE was used for the ARX/ARMAX models, since it gives a single value for
the whole model, and the nMRSE was used in the TF models, since they only have
one output. Besides, the nMRSE also indicates if the model is worse than an
average line (nMRSE $\approx 0\%$).

% A good model will have
% a MRSE closer to $0\%$ and a nMRSE closer to $100\%$, and, a nMRSE close to
% $0\%$ means that an average line is better than the model. One advantage of the
% MRSE is that it will generate a single value for the whole model, while the
% nMRSE will generate a value for each output.

The models were tested using the cross validation method, where, three
experiments were made, one was used to identify the model and the other two
were used for validation. The models were compared regarding their capacity to
represent the system at the current time, and to predict the system's future
states, thus, the 5, 10 and 20 step predictions were used. The parameters for
each experiment can be seen in Table \ref{tab:exp_params}.

% \begin{table}[!htb]
%   \centering
%   \caption{Experiment parameters}\label{tab:exp_params}
%   \begin{tabular}{ccccccccccc}
%     \toprule
%     \multirow{2}{*}{Signal} & \multirow{2}{*}{Axis} & \multicolumn{3}{c}{Amplitude} & \multicolumn{3}{c}{Probabilities} & \multicolumn{3}{c}{$T_b$ or $T^*$ $[ms]$} \\[2pt]
%     \cline{3-11}
%     {} & {} & $\#1$ & $\#2$ & $\#3$ & $\#1$ & $\#2$ & $\#3$ & $\#1$ & $\#2$ & $\#3$ \\
%     \toprule
%     \multirow{3}{*}{PRBS} & $x_f\;[m/s]$      & $0.25$ & $0.50$ & --- & --- & --- & --- & 250 & 250 & --- \\
%     {}                    & $y_f\;[m/s]$      & $0.25$ & $0.50$ & --- & --- & --- & --- & 250 & 250 & --- \\
%     {}                    & $\theta\;[rad/s]$ & $1.85$ & $1.85$ & --- & --- & --- & --- & 250 & 250 & --- \\
%     \midrule
%     \multirow{3}{*}{GBN}  & $x_f\;[m/s]$      & --- & --- & $0.30$ & --- & --- & $0.35$ & --- & --- & --- \\
%     {}                    & $y_f\;[m/s]$      & --- & --- & $0.30$ & --- & --- & $0.35$ & --- & --- & --- \\
%     {}                    & $\theta\;[rad/s]$ & --- & --- & $1.25$ & --- & --- & $0.80$ & --- & --- & --- \\
%     \bottomrule
%   \end{tabular}
% \end{table}

\begin{table}[!htb]
  \centering
  \caption{Experiment parameters}\label{tab:exp_params}
  \begin{tabular}{ccccc}
    \toprule
    \multirow{2}{*}{Exp.} & \multirow{2}{*}{Signal} & Amplitude & Probabilities & \multirow{2}{*}{$T_b \text{ or } T^*$ $[ms]$} \\
    \cline{3-4}
    {} & {} & $x_f,\,y_f,\,\theta$ & $p_{x_f},\,p_{y_f},\,p_\theta$  & \\
    \toprule
    $1$ & PRBS & $ 0.25,\,0.25,\,1.85 $ & $ -, -, - $ & 250 \\
    $2$ & PRBS & $ 0.50,\,0.50,\,1.85 $ & $ -, -, - $ & 250 \\
    $3$ & GBN  & $ 0.30,\,0.30,\,1.25 $ & $ 0.35,\, 0.35,\, 0.8 $ & 70 \\
    \bottomrule
  \end{tabular}
\end{table}

% \begin{itemize}
%     \item Exp. \#1 - Signal PRBS - $T_b = 250\;ms$
%       \begin{itemize}
%         \item Axis amplitudes:
%           \begin{itemize}
%             \item $x_f = 0.25\;m/s$, $y_f = 0.25\;m/s$, $\theta = 1.85\;rad/s$
%           \end{itemize}
%       \end{itemize}
%     \item Exp. \#2 - Signal PRBS - $T_b = 250\;ms$
%       \begin{itemize}
%         \item Axis amplitudes:
%           \begin{itemize}
%             \item $x_f = 0.5\;m/s$, $y_f = 0.5\;m/s$, $\theta = 1.85\;rad/s$
%           \end{itemize}
%       \end{itemize}
%     \item Exp. \#3 - Signal GBN - $T^* = 70\;ms$
%       \begin{itemize}
%         \item Axis amplitudes and probabilities:
%           \begin{itemize}
%             \item $x_f = 0.3\;m/s,\quad p_{x_f} = 0.35$
%             \item $y_f = 0.3\;m/s,\quad p_{y_f} = 0.35$
%             \item $\theta = 1.25\;rad/s,\quad p_\theta = 0.8$
%           \end{itemize}
%       \end{itemize}
% \end{itemize}

At last, the TF models were also identified, through the unit step response in
each axis, and used to tune a PID position controller for each axis of the
robot. The controller tunning was made using \matlab Auto Tunner.

For all the collected data the sampling time was $15\;ms$. And the PRBS signal
used has $n = 8$.


\section{Results}

\subsection{Choosing the Right Model Order}

A complete analysis was made in order to choose which model order to use. In
this paper, a small part of it will be shown. To summarize the process, all of
the proposed models were identified using the best parametrization of the PRBS
and GBN signals, then, they were compared and the best ones are:

\begin{itemize}
  \item ARX(6,5) with delay $6$
  \item ARMAX(3,2,2) with delay $5$
\end{itemize}

\subsection{Comparing the Best ARX and ARMAX Models}

The comparison between the best ARX and ARMAX models can be seen on Fig.
\ref{fig:mrse_arx} and \ref{fig:mrse_armax}, respectively. Each color
represents one experiment and the experiment used to identify the model is
written in the arrow above the bars, e.g. the first three bars/models (left to
right) were identified using experiment 1 and so on.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Images/MRSE_MRSE_ARX_}
  \caption{MRSE for the ARX model}
  \label{fig:mrse_arx}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Images/MRSE_MRSE_ARMAX_}
  \caption{MRSE for the ARMAX model}
  \label{fig:mrse_armax}
\end{figure}

By analyzing Fig. \ref{fig:mrse_arx} and \ref{fig:mrse_armax} it is possible to
see that there is a little to no difference between the models, there are
almost no cases where there is a difference greater than $1\%$ in the MRSE.
Therefore, both models are perfectly good to represent the system.  One
tiebreaker that could be used is the model order, i.e. the ARX model has
polynomials with an order up to $6$, while the ARMAX model has polynomials with
an order up to $3$, thus, the ARMAX model would be a better choice, since it
demands less parameters to represent the system.

\subsection{Transfer Function Models}

% To obtain the TF models for each axis of the robot, a unit step response
% experiment was made for each axis individually. The obtained models can be seen
% on Equations \eqref{eq:tf_x}, \eqref{eq:tf_y} and \eqref{eq:tf_w}. The nMRSE
% for them is $97.63\%,\,98.68\%$ and $98.96\%$ for the $x_f,\,y_f$ and $\theta$
% axis, respectively.

The obtained TF models for each axis of the robot can be seen in Equations
\eqref{eq:tf_x}, \eqref{eq:tf_y} and \eqref{eq:tf_w}. The nMRSE for them is
$97.63\%,\,98.68\%$ and $98.96\%$ for the $x_f,\,y_f$ and $\theta$ axis,
respectively.

\begin{align}
  \frac{P_x(z)}{V_x(z)} &= \frac{0.0003494}{ z^{2}-1.97 z+0.97} \label{eq:tf_x}\\
  \frac{P_y(z)}{V_y(z)} &= \frac{0.0006378}{z^{2} - 1.94 z+0.9399} \label{eq:tf_y}\\
  \frac{P_\theta(z)}{V_\theta(z)} &= \frac{0.001623}{ z^{2} - 1.901 z+0.9009} \label{eq:tf_w}
\end{align}

In order to identify these models, the number of poles ($n_p$) and zeros
($n_z$) had to be specified. In this case they were $n_p = 2$ and $n_z = 0$,
these values values were determined by trial and error, and, they were the
least amount of poles/zeros that resulted in a good fit of the model (nMRSE
greater than $95\%$).

It is important to note that, when performing the identification of the models,
the output data must always start from zero, since the TF model doesn't have
initial conditions, and, given a step signal of the form:

\begin{equation*}
  u(t) = \begin{cases}
    0,\; t < t_s \\
    1,\; t \geq t_s
  \end{cases}
\end{equation*}

There should be a small period before the step begins ($t_s$), in this case,
all step commands had $0.4 \leq t_s \leq 0.6\;s$.

% \begin{equation}
%   \begin{bmatrix}
%     P_x(z)/V_x(z) \\ P_y(z)/V_y(z) \\ P_\theta(z)/V_\theta(z)
%   \end{bmatrix} =
%   \begin{bmatrix}
%     \frac{0.0003494}{ z^{2}-1.97 z+0.97} \\[6pt]
%     \frac{0.0006378}{z^{2} - 1.94 z+0.9399} \\[6pt]
%    \frac{0.001623}{ z^{2} - 1.901 z+0.9009}
%   \end{bmatrix}
% \label{eq:tfs}
% \end{equation}

\subsection{Design of the PID}

In order to design the PID controller for each axis, a \simulink diagram was
built with the control loop for each axis, one example of these control loops
can be seen on Fig.~\ref{fig:control_x}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{Images/PIDTunBlockDiag}
  \caption{Control loop for the $x$ axis}
  \label{fig:control_x}
\end{figure}

The controllers were tunned in order to have the least amount of overshoot and
settling time as possible. What limits this is how much control effort is
necessary, when considering the team's SSL robot, its maximum speed is around
$2.5\;m/s$ (linear) and $6.25\;rad/s$ (rotation). Therefore, the models were
tunned accordingly to that. The performance parameters of the controllers can
be seen on Table \ref{tab:systemParam}.

\begin{table}[!htb]
  \centering
  \caption{Tunned system performance parameters}
  \label{tab:systemParam}
  \begin{tabular}{cccc}
    \toprule
    Axis & Rise time [s] & Settling time [s] & Overshoot [\%] \\
    \toprule
    $x_f$ & 1.05 & 3.48 & 9.8 \\
    $y_f$ & 0.795 & 2.19 & 3.84 \\
    $\theta$ & 0.165 & 0.525 & 7.03 \\
    \bottomrule
  \end{tabular}
\end{table}

Finally, the controllers were deployed into the team's software and they were
tested in the actual robot. The step response can be seen on Fig.
\ref{fig:resultController} and the measured performance parameters can be seen
on Table \ref{tab:measSystemParam}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\linewidth]{Images/resultController.png}
  \caption{Step response of the robot}
  \label{fig:resultController}
\end{figure}

\begin{table}[!htb]
  \centering
  \caption{Measured system performance parameters}
  \label{tab:measSystemParam}
  \begin{tabular}{cccc}
    \toprule
    Axis & Rise time [s] & Settling time [s] & Overshoot [\%] \\
    \toprule
    $x_f$ & 1.785 & 3.299 & 0 \\
    $y_f$ & 1.275 & 3.210 & 0 \\
    $\theta$ & 0.150 & 0.735 & 9.287 \\
    \bottomrule
  \end{tabular}
\end{table}

There are some noticeable differences between the tunned values and what was
actually measured, but they are still acceptable. One possible reason for the
deviation between these values, especially the $x_f$ rise time and $y_f$
settling time, is that, since the robot doesn't have a proper controller for
the wheels, there is no limit to its acceleration, resulting in a small wheel
slippage.

\section{Conclusions}

From the results shown previously, it is possible to confirm that the ARX/ARMAX
models were successfully identified, providing good predictions of the system's
states, i.e. an MRSE lower than $3.5\%$ for 20 step prediction, which is around
$0.3s$ in the future. Moreover, even though the performance of both models is
similar, the ARMAX model is preferred over ARX due to its reduced order of
polynomials.

Regarding the identification and use of the TF models, some interesting results
have been achieved, specially due to it allowing the design of the PID
controllers, which improved dramatically the performance of the robot's,
allowing the team to climb from third place to first place in the last
in-person competition.

Now, the next steps for this project is to utilize the ARMAX model in the
design of even more sophisticated control systems, pushing the performance of
the robots even further.

% \addtolength{\textheight}{-0cm}  % This command serves to balance the column lengths
%                                   % on the last page of the document manually. It shortens
%                                   % the textheight of the last page by a suitable amount.
%                                   % This command does not take effect until the next page
%                                   % so it should come on the page before the last. Make
%                                   % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliography{IEEEabrv,References}

\end{document}
