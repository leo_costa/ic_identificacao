#!/usr/bin/python3
import matplotlib.pyplot as plt
import pandas as pd
import sys
import re
from statistics import mean

def anotatePlot(x1, y1, x2, y2, text, offsetText):
    # Annotate arrow
    plt.annotate('',
                xy=(x1-0.3, y1),
                xytext=(x2+0.3, y2),
                xycoords='data',
                arrowprops=dict(arrowstyle='<->', connectionstyle='arc3', color='blue', lw=2)
                )

    # Annotate Text
    plt.annotate(text, # text to display
                xy=(mean([x1,x2])-1, y1+offsetText),
                rotation=0,
                va='center',
                ha='left',
                )

def anotatePlotAX(x1, y1, x2, y2, text, offsetText,ax):
    # Annotate arrow
    ax.annotate('',
                xy=(x1-0.3, y1),
                xytext=(x2+0.3, y2),
                xycoords='data',
                arrowprops=dict(arrowstyle='<->', connectionstyle='arc3', color='blue', lw=2)
                )

    # Annotate Text
    ax.annotate(text, # text to display
                xy=(mean([x1,x2]), y1+offsetText),
                rotation=0,
                va='center',
                ha='center',
                )

def generateBarCharts(offsetText, BAR_LIM, plotName, fitARXName, fitARMAXName, legendLoc):
    # "/home/leonardo/Python/ICFInal/fitARX_2.csv"
    # "/home/leonardo/Python/ICFInal/fitARMAX_2.csv"
    offsetText = float(offsetText)

    df_arx = pd.read_csv(fitARXName)
    df_armax = pd.read_csv(fitARMAXName)

    NA = [2,3,4,5,6,7,8]
    NB = [1,2,3,4,5,6,7]
    NC = [1,2,3,4,5,6,7]
    NK = [4,5,6,7,6,5,4]

    modelNames = ["ARX({0},{1}) ARMAX({0},{1},{2}) $n_k$={3}".format(i,j,k,z) for i,j,k,z in zip(NA,NB,NC,NK)]*4

    df_both = pd.DataFrame({'$X_{ARX}$':list(df_arx.X), '$Y_{ARX}$':list(df_arx.Y), '$\\theta_{ARX}$':list(df_arx.iloc[:,2]),
                            '$X_{ARMAX}$':list(df_armax.X), '$Y_{ARMAX}$':list(df_armax.Y), '$\\theta_{ARMAX}$':list(df_armax.iloc[:,2])},
                            index=list(modelNames))

    # Remove valores acima de 100%
    df_both[abs(df_both.iloc[:,:]) > 100] = 0

    print(df_both.describe(), end="\n\n")

    # get the maximum Y values for each bar group
    Y = [max(list(df_both.iloc[i:i+7,0:6].max() ) )+0.4 for i in range(0,22,7)]

    plt.rcParams.update({'font.size': 16})
    df_both.plot(kind='bar', width=0.75, figsize=(18,10))
    plt.xticks(rotation=40, ha='right', rotation_mode='anchor')

    plt.ylim(BAR_LIM)
    plt.grid(b=True)
    plt.ylabel("nRMSE [%]", rotation=0, ha='right', rotation_mode='anchor')

    for i,j,k in zip(range(0,22,7), [1,5,10,20], range(0,4,1)):
        anotatePlot(*(i, Y[k]), *(i+6, Y[k]), '{0} Step Prediction'.format(j), offsetText)

    plt.subplots_adjust(left=0.12, bottom=0.25, right=0.99, top=0.98)
    plt.legend(loc=legendLoc)
    plt.savefig('/home/leonardo/Python/ICFInal/' + plotName + '_all.png')

    # plt.show(block=False)
    Y = [list(df_both.iloc[i:i+7,0:6].max() + 0.3) for i in range(0,22,7)]
    Yaux = []
    for listMax in Y:
        listMax = [max([listMax[i],listMax[i+3]]) for i in range(0,3,1)]
        Yaux.append(listMax)
    Y = Yaux
    del Yaux

    plt.rcParams.update({'font.size': 14})
    fig = plt.figure()
    plots = [fig.add_subplot(1,3,i+1) for i in range(0,3,1)]

    for i in range(0,3,1):
        df_both.iloc[:,[0+i,3+i]].plot(kind='area', stacked=False, ax=plots[i], figsize=(18,7))
        plots[i].set_xticks(range(0,28,1))
        plots[i].set_xticklabels(["#{0}".format(i) for i in range(0,28,1)], rotation=90)
        plots[i].set_ylim(BAR_LIM)
        plots[i].grid(True)
        plots[i].set_ylabel("NRMSE [%]")
        plots[i].set_xlabel("Models")
        plt.subplots_adjust(left=0.05, bottom=0.071, right=0.98, top=0.98)

        for z in range(0,3,1):
            for i,j,k in zip(range(0,22,7), [1,5,10,20], range(0,4,1)):
                anotatePlotAX(*(i, Y[k][z]-0.2), *(i+6, Y[k][z]-0.2), '{0} Step\nPrediction'.format(j), offsetText+0.15, plots[z])
    # plt.ylim(BAR_LIM)
    plt.savefig('/home/leonardo/Python/ICFInal/' + plotName + '.png')


    plt.show()

if __name__ == "__main__":
    legendLoc = re.sub('[-]', ' ', sys.argv[7])
    # (offsetText, BAR_LIM, plotName, fitARXName, fitARMAXName, legendLoc)
    # (0.2         90 100   GBN_Bom   fitARX.csv  fitARMAX.csv  upper-right
    generateBarCharts(sys.argv[1], (int(sys.argv[2]),int(sys.argv[3])), sys.argv[4], sys.argv[5], sys.argv[6], legendLoc)
