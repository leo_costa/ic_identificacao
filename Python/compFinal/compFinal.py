import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
import sys
import re
import numpy as np
from statistics import mean

def anotatePlot(x1, y1, x2, text, offsetText):
    # Annotate arrow
    plt.annotate('',
                xy=(x1-0.3, y1),
                xytext=(x2+0.3, y1),
                xycoords='data',
                arrowprops=dict(arrowstyle='<->', connectionstyle='arc3', color='black', lw=1)
                )

    # Annotate Text
    plt.annotate(text, # text to display
                xy=(mean([x1,x2]), y1+offsetText),
                rotation=0,
                va='center',
                ha='center',
                )

def createFitBarChart(dfFit, axisLabels, chartYLim, legendLoc, modelType, fitType):
    barWidth = 0.5
    plt.rcParams.update({'font.size': 14})
    colors = ['#41d8cf','#e1191e','#3c2cff']

    for axis in axisLabels:
        plt.figure(figsize=(8,6))
        for step in range(0, len(dfFit['StepPred'].values), 9):
            x = np.asanyarray(dfFit.loc[step:step+8, 'StepPred']) + np.array([n*barWidth for n in range(-4,5)])
            y = np.asanyarray(dfFit.loc[step:step+8, axis])

            plt.bar(x, y, barWidth-0.1, color=colors)
            xArrow = x
            yArrow = y
            expI = ["E.#{0}".format(n) for n in [3,2,1]]
            while xArrow.size > 0:
                anotatePlot(xArrow[0], max(yArrow[0:3])+0.1, xArrow[2], expI.pop(), 0.25)
                xArrow = xArrow[3:]
                yArrow = yArrow[3:]

#          plt.title( "$" + axis + "$")

        plt.ylim(chartYLim)
        plt.ylabel(fitType + " $[\%]$")
        yTick = 1

        if fitType == "MRSE":
            yTick = 0.5
        plt.yticks(list(np.arange(*chartYLim, yTick)))

        plt.xlabel("Prediction limit [Steps]")
        plt.xticks(list(range(0,16,5)), [n for n in [1,5,10,20]])


        if axis == 'Theta':
            axis = '\\theta'

        plt.grid(True)
        patches = [mpatches.Patch(color=cor, label="Exp #{0}".format(n)) for cor,n in zip(colors,[1,2,3]) ]
        plt.legend(handles=patches, loc=legendLoc)
        plt.subplots_adjust(left=0.10, bottom=0.09, right=0.995, top=0.95)
        plt.savefig(fitType + "_" + axis + "_" + modelType + "_.png")
        # plt.show()

def compFinalFit(modelType):
    if modelType == "ARX":
        fitFiles = ["fitARX{0}_.csv".format(n) for n in range(1,4)]
    elif modelType == "ARMAX":
        fitFiles = ["fitARMAX{0}_.csv".format(n) for n in range(1,4)]

    dfFitFiles = [pd.read_csv(filename, header=None) for filename in fitFiles]

    expNames = ["Exp #{0}".format(n) for n in range(1,4)]*4
    expIdent = [[exp for n in range(0,12)] for exp in range(1,4)]
    steps = [0 for n in range(0,3)] + [5 for n in range(0,3)] + [10 for n in range(0,3)] + [15 for n in range(0,3)]

    dfFit = pd.DataFrame()

    for n in range(0,3):
        dfFitFiles[n].columns = ['X', 'Y', 'Theta']
        dfFitFiles[n].insert(0,'ExpName',expNames)
        dfFitFiles[n]['ExpIdent'] = expIdent[n]
        dfFitFiles[n]['StepPred'] = steps
        dfFit = dfFit.append(dfFitFiles[n])

    dfFit.sort_values(['StepPred','ExpIdent'], inplace=True)
    dfFit.reset_index(inplace=True, drop=True)
    # print(dfFit.head(36))

    createFitBarChart(dfFit, ['X', 'Y', 'Theta'], (88, 101), 'lower left', modelType, "nMRSE")

def compFinalMRSE(modelType):
    if modelType == "ARX":
        fitFiles = ["mrseARX{0}_.csv".format(n) for n in range(1,4)]
    elif modelType == "ARMAX":
        fitFiles = ["mrseARMAX{0}_.csv".format(n) for n in range(1,4)]

    dfFitFiles = [pd.read_csv(filename, header=None) for filename in fitFiles]
    dfFit = pd.DataFrame()
    dfFit["MRSE"] = []

    expNames = []
    for n in range(1,4):
        for exp in [n,n,n,n]:
            expNames.append(int(exp))

    for n in range(0,3):
        data = []
        for valList in dfFitFiles[n].iloc[:,:].values:
            for val in valList:
                data.append(val)
        dfAux = pd.DataFrame({"MRSE":data,
                              "ExpName":expNames,
                              "ExpIdent":[n+1 for j in range(0,12)],
                              "StepPred":list(range(0,16,5))*3
                             }
                            )
        dfFit = dfFit.append(dfAux, sort=False)

    dfFit.sort_values(['StepPred','ExpIdent'], inplace=True)
    dfFit.reset_index(inplace=True, drop=True)
#      print(dfFit.head(36))
#      print(dfFit.describe(), end="\n\n")

    createFitBarChart(dfFit, ['MRSE'], (0, 5), 'upper left', modelType, "MRSE")

if __name__ == "__main__":
    if len(sys.argv) == 3:
        if sys.argv[1] == "nMRSE":
            compFinalFit(sys.argv[2])
        elif sys.argv[1] == "MRSE":
            compFinalMRSE(sys.argv[2])
    else:
        print("Please provide the following parameters:")
        print("Parameters: fitType = [nMRSE MRSE] modelType = [ARX ARMAX]")
