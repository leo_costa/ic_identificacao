
# Repositório do projeto de iniciação científica sobre identificação de sistemas para robôs móveis

Neste repositório será possível encontrar:

* Os relatórios feitos durante o projeto;
* Os códigos em MATLAB para realizar a identificação dos modelos ARX, ARMAX e
  Função de Transferência;
* Todos os dados obtidos durante o projeto;
* O artigo e pôster feitos para o SICFEI;
* Um tutorial sobre como gerar o código em C++ de um modelo no Simulink;
* O aplicativo em MATLAB para a identificação dos modelos;
* Alguns scripts em Python utilizados para gerar os gráficos utilizados no
  relatório final;
* O código em C++ de um controlador PID para a posição dos robôs no campo;
* O modelo em Simulink do controlador PID de posição de posição dos robôs.

Durante o projeto também foi desenvolvido um software para a aquisição dos
dados do robô para gerar os modelos, esse software pode ser encontrado neste
[repositório](https://gitlab.com/leo_costa/systemidentificationsoftware)

Também existe um [repositório
separado](https://gitlab.com/robofei/ssl/ssl_pid_positioncontroller) contendo
somente o modelo em Simulink do controlador PID, os dados e os scripts
necessários para identificar a FT do robô.
