function matrixTXT = createARXMatrixTXT(arxModelMatrix)
%createARXMatrixTXT Creates a string that contains the ARX Matrix

modelOrder = length(arxModelMatrix{1,1});
numberStringPattern = "";

for i=1 : modelOrder
    numberStringPattern = strcat(numberStringPattern, "%d ");
end
numberStringPattern = strcat(numberStringPattern, ", ", ...
                             numberStringPattern, ", ", ...
                             numberStringPattern);
                         
numberStringPattern = strcat(numberStringPattern, ":\n");

matrixTXT = sprintf(numberStringPattern + ...
                    numberStringPattern , ...
          arxModelMatrix{1,1}, arxModelMatrix{1,2}, arxModelMatrix{1,3},...
          arxModelMatrix{2,1}, arxModelMatrix{2,2}, arxModelMatrix{2,3});
      
% Matrix must end with ';'
numberStringPattern = regexprep(numberStringPattern, ':',';');
matrixTXT = sprintf(matrixTXT + ...
                    numberStringPattern, ...
          arxModelMatrix{3,1}, arxModelMatrix{3,2}, arxModelMatrix{3,3});
end

