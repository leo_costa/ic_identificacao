function saveTFtoTXT(tfModelX,tfModelY,tfModelW,fileAddress)
%saveTFtoTXT Saves all Transfer Function models to a text file.

txtTFX = createTFtxt(tfModelX, "X");
txtTFY = createTFtxt(tfModelY, "Y");
txtTFW = createTFtxt(tfModelW, "W");
               
fileAddress = strcat(fileAddress, 'TFModels.tf');
[fileID, errorMsg]= fopen(fileAddress, 'w');

if ~isempty(errorMsg)
    msgbox(errorMsg,'Error','error');
else
    fprintf(fileID, txtTFX);
    fprintf(fileID, txtTFY);
    fprintf(fileID, txtTFW);
    
    fclose(fileID);
end

end

