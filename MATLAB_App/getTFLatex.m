function latexString = getTFLatex(tf,inputName, outputName)
%getTFLatex Converts a Transfer Function Model to a interpretable latex
%string

% Find the division/fraction characters that MatLab prints
divisionChars = regexp(evalc('tf'), '-[-]+', 'match');
TFtxt = split(evalc('tf'), divisionChars{1});
% Remove line breaks and white spaces
TFtxt = regexprep(TFtxt,'[\n\r]+',' ');
TFtxt = TFtxt(~ismissing(TFtxt));
% Get the numerator and denominator coefficients
num = TFtxt{1};
den = TFtxt{2};
% Removes 'Sample', the text after and white spaces
den = split(den, ' ');
den = den(1:find(contains(den,'Sample'))-1);
% den = strtrim(den);
% den = den(~ismissing(den));
% Removes the beggining of the text, e.g. 'tf = '
num = split(num, ' ');
num = num((find(contains(num,'='))+1):end);
% Removes a possible line break and white spaces
% num = strtrim(num);
% num = num(~ismissing(num));
% Joins the coefficients together
num = join(num);
den = join(den);

latexString = "$\frac{" + outputName + "}{"+ inputName +"} = \frac{"...
               + num{1} + "}{" + den{1} + "}$";

end

