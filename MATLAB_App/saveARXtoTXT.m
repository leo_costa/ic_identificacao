function saveARXtoTXT(arxModel,fileAddress)
%getARXLatex Saves a ARX Model to a text file.

ASize = size(arxModel.A);
BSize = size(arxModel.B);

headerA = sprintf('Matrix A=%dx%d\n', ASize(1,1), ASize(1,2));
matrixA = createARXMatrixTXT(arxModel.A);

headerB = sprintf('Matrix B=%dx%d\n', BSize(1,1), BSize(1,2));
matrixB = createARXMatrixTXT(arxModel.B);

fileAddress = strcat(fileAddress, 'ARXModel.arx');
[fileID, errorMsg]= fopen(fileAddress, 'w');

if ~isempty(errorMsg)
    msgbox(errorMsg,'Error','error');
else
    fprintf(fileID, headerA);
    fprintf(fileID, matrixA);
    fprintf(fileID, headerB);
    fprintf(fileID, matrixB);
    
    fclose(fileID);
end

end

