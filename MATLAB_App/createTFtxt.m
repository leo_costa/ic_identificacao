function tfTXT = createTFtxt(tfModel, axis)
%createTFtxt Creates a string that contains a transfer function model

nPoles = length(tfModel.Denominator);
nZeros = length(tfModel.Numerator);

header = sprintf("%s Axis: Denominator Coefficients = %d |" + ... 
                           " Numerator Coefficients = %d \n", ...
                   axis, nPoles, nZeros);

numberStringPatternZeros = "Numerator = [ ";
numberStringPatternPoles = "Denominator = [ ";

for i=1 : nZeros
    numberStringPatternZeros = strcat(numberStringPatternZeros, "%d ");
end
numberStringPatternZeros = strcat(numberStringPatternZeros, "] \n");

for i=1 : nPoles
    numberStringPatternPoles = strcat(numberStringPatternPoles, "%d ");
end   
numberStringPatternPoles = strcat(numberStringPatternPoles, "] \n \n");

tfTXT = sprintf(header + ...
                numberStringPatternZeros + ...
                numberStringPatternPoles,  ...
                tfModel.Numerator, ...
                tfModel.Denominator); 
end

