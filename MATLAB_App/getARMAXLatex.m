function [latexStringA, latexStringB, latexStringC]= getARMAXLatex(armax)
%getARXLatex Converts a ARMAX Model to a interpretable latex
%string

% Definition of the delimiter to separate the string
delimiter = ["Model for output " + '"' + armax.OutputName{1} + '"' + ...
                ": A(z)y_1(t) = - A_i(z)y_i(t) + B(z)u(t) + C(z)e_1(t)";...
             "Model for output " + '"' + armax.OutputName{2} + '"' + ... 
                ": A(z)y_2(t) = - A_i(z)y_i(t) + B(z)u(t) + C(z)e_2(t)";...
             "Model for output " + '"' + armax.OutputName{3} + '"' + ... 
                ": A(z)y_3(t) = - A_i(z)y_i(t) + B(z)u(t) + C(z)e_3(t)";...
             "Name:";...
             "model:";...
             "Sample time:"];
ARMAXtxt = split(evalc('armax'),delimiter);
% Removes line break
ARMAXtxt = regexprep(ARMAXtxt,'[\n\r]+',' ');
% Get only the lines that have the polynomials
ARMAXtxt = ARMAXtxt(contains(ARMAXtxt,'A(z)'));

% Gets the polynomials separated as follows:
% A1
% A2
% A3
% B1
% B2
% B3
% C
matrixXtxt = getMultiLineMatrix(ARMAXtxt{1});
matrixYtxt = getMultiLineMatrix(ARMAXtxt{2});
matrixWtxt = getMultiLineMatrix(ARMAXtxt{3});

% Create A*u(k) = 
latexStringA = "$\left(\begin{array}{ccc} " + ...
              matrixXtxt{1} + " & " + matrixXtxt{2} + " & " + matrixXtxt{3} + ...
              " \\ " + ...
              matrixYtxt{2} + " & " + matrixYtxt{1} + " & " + matrixYtxt{3} + ...
              " \\ " + ...
              matrixWtxt{2} + " & " + matrixWtxt{3} + " & " + matrixWtxt{1} + ...
              " \end{array}\right) \times " + ...
              "\left(\begin{array}{c} x(k)\\ y(k)\\ \theta(k) \end{array}\right) = $";

% Create B*y(k)
latexStringB = "$\left(\begin{array}{ccc} " + ...
               matrixXtxt{4} + " & " + matrixXtxt{5} + " & " + matrixXtxt{6} + ...
               " \\ " + ...
               matrixYtxt{4} + " & " + matrixYtxt{5} + " & " + matrixYtxt{6} + ...
               " \\ " + ...
               matrixWtxt{4} + " & " + matrixWtxt{5} + " & " + matrixWtxt{6} + ...
               " \end{array}\right) \times " + ...
               "\left(\begin{array}{c} u_{x}(k)\\ u_{y}(k)\\ u_{\theta}(k) \end{array}\right) + $";

           
% Create C
latexStringC = "$\left(\begin{array}{ccc} " + ...
               matrixXtxt{7} + " \\ " + ...
               matrixYtxt{7} + " \\ " + ...
               matrixWtxt{7} + " \end{array}\right) \times " + ...
               "\left(\begin{array}{c} e_{x}(k)\\ e_{y}(k)\\ e_{\theta}(k) \end{array}\right)$";
% Checks if the generated string is smaller than 1200 characters (Max
% ammount that MatLab can process)

if strlength(latexStringA) > 1200 || strlength(latexStringB) > 1200 || strlength(latexStringC) > 1200
    latexStringA = "Model too large! stringAsize = " + strlength(latexStringA); 
    latexStringB = "stringBsize = " + strlength(latexStringB);
    latexStringC = "stringCsize = " + strlength(latexStringB);
end

end

