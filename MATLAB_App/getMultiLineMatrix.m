function multiLineMatrix = getMultiLineMatrix(singleLineMatrix)
%getMultiLineMatrix Separates the ARX Polynomial Matrix into several lines 

multiLineMatrix = split(singleLineMatrix, "  ");
% Deletes line breaks and white spaces
multiLineMatrix = regexprep(multiLineMatrix,'[\n\r]+',' ');
multiLineMatrix = multiLineMatrix(~ismissing(multiLineMatrix));

% If the polynomial got split into more than one line, rejoins it together
for i=1 : length(multiLineMatrix)
    if not(contains(multiLineMatrix{i},"(z)")) && i > 1
        multiLineMatrix{i-1} = strcat(multiLineMatrix{i-1},multiLineMatrix{i}) ;
        multiLineMatrix{i} = '';
    end

end

% Removes empty cells
multiLineMatrix = multiLineMatrix(~ismissing(multiLineMatrix));

% Joins any z^ that may have gotten separated from its value
multiLineMatrix =  regexprep(multiLineMatrix,'[^(]z[^]\s','z^');

% Adds curly braces for the expoents
multiLineMatrix =  regexprep(multiLineMatrix,'z.([+-]\d*)','z^{$1}');

% Changes z to q
multiLineMatrix =  regexprep(multiLineMatrix,'z','q');

% Remove everything before the equal sign
multiLineMatrix = split(multiLineMatrix, '=');
multiLineMatrix = multiLineMatrix(:,2);
end

